<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery-ui-1.11.4/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery-ui-1.11.4/jquery-ui.structure.min.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery-ui-1.11.4/jquery-ui.theme.min.css">

<script type="text/javascript" src="${ctx}/static/jquery/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-ui-1.11.4/jquery-ui.min.js"></script>

<link type="text/css" rel="stylesheet" href="${ctx}/static/jtable.2.4.0/themes/metro/blue/jtable.min.css" />
<script type="text/javascript" src="${ctx}/static/jtable.2.4.0/jquery.jtable.min.js" ></script>

<title>index</title>
</head>
<body>
<h1>index</h1>
<a href="${pageContext.request.contextPath}/index">进入商城</a>
<a href="${pageContext.request.contextPath}/nanda/index">南京大学</a>
<a href="${pageContext.request.contextPath}/wuda/index">武汉大学</a>
<br/><br/>
<%-- 作用是：起始页可以字定义，如新品促销、新品发布等页面 --%>
<%-- <% response.sendRedirect(request.getContextPath()+"/index"); %> --%>

<div id="PersonTableContainer"></div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#PersonTableContainer').jtable({
            title: 'Table of people',
            actions: {
                listAction: '/GettingStarted/PersonList',
                createAction: '/GettingStarted/CreatePerson',
                updateAction: '/GettingStarted/UpdatePerson',
                deleteAction: '/GettingStarted/DeletePerson'
            },
            fields: {
                PersonId: {
                    key: true,
                    list: false
                },
                Name: {
                    title: 'Author Name',
                    width: '40%'
                },
                Age: {
                    title: 'Age',
                    width: '20%'
                },
                RecordDate: {
                    title: 'Record date',
                    width: '30%',
                    type: 'date',
                    create: false,
                    edit: false
                }
            }
        });
    });
</script>

</body>
</html>