<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div class="navi_main">	
<div class="col_1_of_1">
<div class="navi_menu">
    <div class="navi_menu_heading">
    	<a class="catalog_link" href="http://mall.360.com/allproduct" target="_blank">全部分类导航<em></em></a>
    </div>
    <ul>
	    <li><a href="${ctx}/index">首页</a></li>
	    <li><a href="javascript:;">路由器</a></li>
	    <li><a href="javascript:;">儿童卫士3</a></li>
	    <li><a href="javascript:;">行车记录仪</a></li>
	    <li><a href="javascript:;">超级充电器<i class="tip-green">New</i></a></li>    
    </ul>
</div>
</div>
</div>