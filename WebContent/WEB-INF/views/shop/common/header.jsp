<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div class="header_bar">
<div class="col_1_of_1">
	<div class="bar_left">
		<a href="http://mall.360.com/#" onclick="return false">收藏360商城</a>
	</div>
	<div class="bar_right">
	<div class="header_bar_nav">
		<ul>
		    <c:choose>
			  <c:when test="${not empty sessionScope.SHOP_HTTP_SESSION}">		  	
			  	<li class="i_dropdown" id="i_dropdown">
		            <a href="#">${sessionScope.SHOP_HTTP_SESSION.getAccount().getCustomerName()}<em></em></a>
		            <ul class="i_sub_menu" id="i_sub_menu">
		                <li><a href="#">个人中心</a></li>
		                <li><a href="#">我的试用</a></li>
		                <li><a href="#">试用积分</a></li>                
		                <li><a href="#">收货地址</a></li>
		                <li><a href="#">账户设置</a></li>
		                <li><a href="javascript:;" onclick="logout()">退出登录</a></li>
		            </ul>
		    	</li>
			  </c:when>
			  <c:otherwise>
			  	<li><a href="javascript:;" onclick="loginShow()">登录</a></li>
				<li><a href="javascript:;" onclick="registerShow()">注册</a></li>
			  </c:otherwise> 
			</c:choose>
		    <li><a href="javascript:;">我的订单</a></li>
			<li><a href="javascript:;">手机360商城</a></li>
			<li><a href="javascript:;">企业采购</a></li>
			<li><a href="javascript:;">帮助中心</a></li>        
		</ul>
	</div>
	</div>
</div>
</div>