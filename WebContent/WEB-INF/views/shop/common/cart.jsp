<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div class="header_cart" id="i_cart">			
	<a href="${ctx}/mycart" ><i></i>我的购物车<span class="cart_size">0</span><em></em></a> 
	<div class="cart_submenu" id="i_cart_popup">
	<c:choose>
	  <c:when test="${not empty sessionScope.SHOP_HTTP_SESSION}">	
	  	<c:choose>
		  <c:when test="${sessionScope.SHOP_HTTP_SESSION.getShoppingCart().getNumberOfItems() > 0}">
		    <div class="ungrid_row">
			    <div class="ungrid_col cart_col" style="width:55px"><img alt="" src="${ctx}/static/shop/t0107616523bf23adbe.png" /></div>
			    <div class="ungrid_col cart_col">360超级充电器</div>
			    <div class="ungrid_col cart_col" style="width:50px">299.00</div>
			    <div class="ungrid_col cart_col" style="width:40px"><a href="#"><span class="cart_item_remove"></span></a></div>
			</div>
			<div class="ungrid_row">
			    <div class="ungrid_col cart_col" style="width:55px"><img alt="" src="${ctx}/static/shop/t0107616523bf23adbe.png" /></div>
			    <div class="ungrid_col cart_col">360超级充电器</div>
			    <div class="ungrid_col cart_col" style="width:50px">299.00</div>
			    <div class="ungrid_col cart_col" style="width:40px"><a href="#"><span class="cart_item_remove"></span></a></div>
			</div>
			<div class="ungrid_row" style="height: 42px; background-color: #F6F9FC;padding-top: 5px;">
		        <div class="ungrid_col cart_col">共2件商品</div>
		        <div class="ungrid_col cart_col">总计:￥798.00</div>		
		        <div class="ungrid_col cart_col" style="text-align: right;"><a href="${ctx}/i/mycart/viewCart" target="_blank" class="u-btn">去购物车</a></div>			        		        		        
		    </div>					    
		  </c:when>
		  <c:otherwise>
		       	你的购物车还没有商品，赶紧去选购吧!
		       	<h1>${sessionScope.SHOP_HTTP_SESSION.getAccount().getCustomerName()}</h1>
	  			<h1>${sessionScope.SHOP_HTTP_SESSION.getShoppingCart().getNumberOfItems()}</h1>
		  </c:otherwise> 
		</c:choose>												        	
	  </c:when>
	  <c:otherwise>				  	
	  		请登录后查看你的购物车。分布式对JPS的sessionScope有影响没有？
	  		<h1>${sessionScope.SHOP_HTTP_SESSION.getAccount().getCustomerName()}</h1>
	  		<h1>${sessionScope.SHOP_HTTP_SESSION.getShoppingCart().getNumberOfItems()}</h1>			  		  	
	  </c:otherwise> 
	</c:choose>
	</div>					
</div>