<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="display: block;	width: 100%;">
<div class="footer_top">
	<div class="col_1_of_1">
		<div class="section group">
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item" style="border-right: solid 1px #444;"><i class="icon1"></i>7天无理由退货</div>
		    </div>
		
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item" style="border-right: solid 1px #444;"><i class="icon2"></i>15天免费换货</div>		        
		    </div>
		
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item"><i class="icon3"></i>满99元包邮<span style="font-size:12px">(特殊商品除外)</span></div>
		    </div>		    
		</div>
	</div>
</div>

<div class="footer_middle">
	<div class="col_1_of_1">
		<div class="section group">
		    <div class="col span_2_of_10">		        
				<span class="header">新手入门</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">新用户注册</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">用户登录</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">找回密码</a></li>
				</ul>
		    </div>
		
		    <div class="col span_2_of_10">
		        <span class="header">购物指南</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">购买流程</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">支付方式</a></li>
					<li><a href="#" rel="nofollow">配送说明</a></li>					
				</ul>		        
		    </div>
		
		    <div class="col span_2_of_10">
		        <span class="header">售后服务</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">服务条款</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">七日退货</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">十五日换货</a></li>
				</ul>
		    </div>	
		    		    
		    <div class="col span_2_of_10">
		        <span class="header">免费试用</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">试用流程</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">查看申请结果</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">提交报告</a></li>
				</ul>
		    </div>
		    
		    <div class="col span_2_of_10">
		        <span class="header">联系我们</span>
				<ul> 
					<li class="call-number">400-6822-360 </li>
					<li class="call-time">周一到周日9:00-18:00</li>
				</ul>
		    </div>	    
		</div>
	</div>
</div>
<div class="footer_copyright">360商城©2013-2015 深圳奇虎健安智能科技有限公司版权所有｜粤ICP备14094849号-1</div>
</div>