<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div id="loginbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">用户登录</span>        
        <a href="javascript:loginClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">  
    	<form id="loginForm">    
        <div class="popup_box_content">               	 
    		 <div style="width: 420px;">
	    		 <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="telephone">电话号码:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="telephone" name="telephone" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_loginName_error_div"></div>			        			        
			    </div>			    
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="password">密码:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="password" name="password" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_password_error_div"></div>		        		        
			    </div>
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col">
			        	<div id="login_loginName_msg_div" style="color: red; display: inline;">${loginNameErrorMessage}</div>
			        	<div id="login_password_msg_div" style="color: red; display: inline;">${passwordErrorMessage}</div>							
			        </div>
			        		        		        
			    </div>
			    
    		</div>        	 
        </div>        
        <div class="popup_box_footer">					
			<input type="button" class="popup_box_btn popup_box_btn_gray" value="登 录" onclick="loginPost()" />							
		</div>
		</form>		        
    </div>
</div>

<div id="registerbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">新用户注册</span>        
        <a href="javascript:registerClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">        
        <div class="popup_box_content" id="registerContainer">
    		        	 
        </div>               
    </div>
</div><!-- end of registerbox -->