<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div class="flexslider" id="flexslider_wrapper">
<ul class="slides" id="slides_wrapper">
	<li id="slide-6" style="background: url(${ctx}/static/banner/2.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-3" style="background: url(${ctx}/static/banner/3.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-4" style="background: url(${ctx}/static/banner/4.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-5" style="background: url(${ctx}/static/banner/5.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-10" style="background: url(${ctx}/static/banner/10.png) 50% 0 no-repeat;"></li>
	<li id="slide-11" style="background: url(${ctx}/static/banner/11.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-14" style="background: url(${ctx}/static/banner/12.jpg) 50% 0 no-repeat;"></li>
</ul>
</div>