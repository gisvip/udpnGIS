<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div class="col_1_of_1">
	<h2 class="mod-titles">明星单品 <i>Fashionate Ones</i></h2>
	<div class="produ_row_content">
		<div class="produ_row_ul_content_0 float_left">
			<a href="http://pop.jumei.com/dress_sport?from=all_null_index_floor_cloth_chanel&amp;lo=3222&amp;mat=21618" target="_blank">
				<img src="${ctx}/static/shop/pl.jpg">
			</a>
		</div>
		<div class="float_right">
			<ul class="produ_row_ul_content_1">
				<li>
					<a	href="http://pop.jumei.com/promotion/40146.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42608"	target="_blank"> 
						<img width="490" height="240" src="${ctx}/static/shop/p1.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_2">
				<li>
					<a	href="http://pop.jumei.com/promotion/40078.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42609"	target="_blank"> 
						<img width="240" height="240" src="${ctx}/static/shop/p2.jpg">

				</a></li>
			</ul>
			<ul class="produ_row_ul_content_3">
				<li>
					<a	href="http://pop.jumei.com/promotion/41034.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42610"	target="_blank"> 
						<img width="240" height="240" src="${ctx}/static/shop/p3.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_4">
				<li>
					<a	href="http://pop.jumei.com/promotion/39944.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42607"	target="_blank"> 
						<img width="490" height="240" src="${ctx}/static/shop/p4.jpg">
					</a>
				</li>
			</ul>
		</div>
	</div>	
</div>
<!-- end of product list 1 -->
<div class="col_1_of_1">
	<h2 class="mod-titles">新品首发 <i>New Arrivals</i></h2>
	<div class="produ_row_content">
		<div class="produ_row_ul_content_0 float_right2">
			<a href="http://pop.jumei.com/dress_sport?from=all_null_index_floor_cloth_chanel&amp;lo=3222&amp;mat=21618" target="_blank">
				<img src="${ctx}/static/shop/pl.jpg">
			</a>
		</div>
		<div class="float_left2">
			<ul class="produ_row_ul_content_1">
				<li>
					<a	href="http://pop.jumei.com/promotion/40146.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42608"	target="_blank"> 
						<img width="490" height="240" src="${ctx}/static/shop/p1.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_2">
				<li>
					<a	href="http://pop.jumei.com/promotion/40078.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42609"	target="_blank"> 
						<img width="240" height="240" src="${ctx}/static/shop/p2.jpg">

				</a></li>
			</ul>
			<ul class="produ_row_ul_content_3">
				<li>
					<a	href="http://pop.jumei.com/promotion/41034.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42610"	target="_blank"> 
						<img width="240" height="240" src="${ctx}/static/shop/p3.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_4">
				<li>
					<a	href="http://pop.jumei.com/promotion/39944.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42607"	target="_blank"> 
						<img width="490" height="240" src="${ctx}/static/shop/p4.jpg">
					</a>
				</li>
			</ul>
		</div>
	</div>		
</div>
<!-- end of product list 2 -->
<div class="col_1_of_1">
<h2 class="mod-titles">社区动态 <i>Community News</i></h2>
	<div class="section group">        
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/eyelet-curtains/"><img src="${ctx}/static/shop/h-portal1.jpg" alt="Eyelet Curtains"></a>       	    	
		</div>
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/contemporary-curtains/"><img src="${ctx}/static/shop/h-portal2.jpg" alt="Pencil Pleat Curtains"></a>                
		</div>
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/duvet-covers/"><img src="${ctx}/static/shop/h-portal3.jpg" alt="Duvets Sets"></a>                
		</div>
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/venetian-blinds/"><img src="${ctx}/static/shop/h-portal4.jpg" alt="Venetian Blinds"></a>
		</div>    
	</div>
</div>
<!-- end of product list 3 -->
<div class="col_1_of_1">	
    <div class="section group">
        <div class="col span_2_of_6">
        	<a href="http://www.linen4less.co.uk/eyelet-curtains/"><img src="${ctx}/static/shop/h-portal1.jpg" alt="Eyelet Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
        	<a href="http://www.linen4less.co.uk/contemporary-curtains/"><img src="${ctx}/static/shop/h-portal2.jpg" alt="Pencil Pleat Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
        	<a href="http://www.linen4less.co.uk/duvet-covers/"><img src="${ctx}/static/shop/h-portal3.jpg" alt="Duvets Sets"></a>               
		</div>       
	</div>
</div>
<!-- end of product list 4 -->
<div class="col_1_of_1">
	<div class="section group">        
    	<div class="col span_2_of_6">
           	<a href="http://www.linen4less.co.uk/eyelet-curtains/"><img src="${ctx}/static/shop/web1.jpg" alt="Eyelet Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
           	<a href="http://www.linen4less.co.uk/contemporary-curtains/"><img src="${ctx}/static/shop/web3.jpg" alt="Pencil Pleat Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
           	<a href="http://www.linen4less.co.uk/duvet-covers/"><img src="${ctx}/static/shop/web2.jpg" alt="Duvets Sets"></a>               
		</div>            
   	</div>
</div>
<!-- end of product list 5 -->
<div class="col_1_of_1">
<h2 class="mod-titles">免费试用 <i>Get-to-Use Freely</i></h2>
	<div class="section group">
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/teachers/"><img src="${ctx}/static/shop/h-portal-parents.jpg" alt="Teachers">		        
	        </a>
	    </div>
	
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/parents/"><img src="${ctx}/static/shop/h-portal-teachers.jpg" alt="Parents">		        
	        </a>
	    </div>
	
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/tutors/"><img src="${ctx}/static/shop/h-portal-tutors.jpg" alt="Tutors">		        
	        </a>
	    </div>
	    
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/teachers/"><img src="${ctx}/static/shop/h-portal-parents.jpg" alt="Teachers">
	        </a>
	    </div>
	</div>
</div>
<!-- end of product list 6 -->