<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<div class="catalog_wrapper">
<div class="col_1_of_1">
<div class="flyout_menu">
<ul class="flyout_menu_ul">
   <li class="flyout_menu_li">
   		<div class="flyout_menu_icon"><img src="${ctx}/static/shop/shouji.png"></div>
   		<div class="flyout_menu_title">手机通讯</div>
   		<div class="flyout_menu_links"> 
   			<a href="http://mall.360.com/preorder/dashen" target="_blank">大神F1</a> 
   			<a href="http://mall.360.com/preorder/dashenf2" target="_blank">大神F2</a>
   		</div>
   		<ul class="flyout_menu_sub_ul">
   			<li><a href="javascript:;" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/t01df2802eb44374fa8.png"></span>大神Note3</a></li>
   			<li><a href="javascript:;" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/t01c2d63285b213563b.png"></span>大神X7</a></li>
   			
	    	<li><a href="javascript:;" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/f12g.png"></span>大神F1 2G内存版</a></li>
	    	
	    	<li><a href="http://mall.360.com/shop/item?item_id=5581291558d4a6c40d000017" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/f1yidong.png"></span>大神F2 全高清</a></li>
	    	<li><a href="http://mall.360.com/preorder/dashenf2" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/f2yidong.png"></span>大神F2 移动版</a></li>
	    	<li><a href="http://mall.360.com/preorder/dashen" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/f1plus.png"></span>大神F2 全网通</a></li>
	    	
	    	<li><a href="javascript:;" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/t0101d26b877bfa71b0.png"></span>大神1S</a></li>
	    	<li><a href="javascript:;" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/t01abded9c7c151e8d0.png"></span>QIKU移动电源</a></li>
	    	<li><a href="${ctx}/item?itemId=512" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/chaojichongdian.png"> </span>超级充电器</a></li>
	    </ul>
   </li>
   <li class="flyout_menu_li">
   		<div class="flyout_menu_icon"><img src="${ctx}/static/shop/shoubiao.png"></div>
		<div class="flyout_menu_title">智能穿戴</div>
		<div class="flyout_menu_links"> 
			<a href="http://baby.mall.360.com/detail.3.html" target="_blank">儿童卫士智能手表</a>
		</div>
	    <ul class="flyout_menu_sub_ul">
	    	<li><a href="http://baby.mall.360.com/detail.2.html" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/ertongbiao2.png"></span>儿童卫士智能手表2</a></li>
	        <li><a href="http://baby.mall.360.com/detail.3.html" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/ertongbiao3.png"></span>儿童卫士智能手表3</a></li>
	        <li><a href="http://mall.360.com/shop/item?item_id=548ac33658d4a69b09000015" target="_blank"><span class="list_img"><img src="${ctx}/static/shop/fangdiu.png"></span>防丢贴片</a></li>
	    </ul>
   </li>		   
   <li class="flyout_menu_li">
   		<div class="flyout_menu_icon"><img src="${ctx}/static/shop/shexiangji.png"></div>
		<div class="flyout_menu_title">智能家居</div>
		<div class="flyout_menu_links"> 
			<a href="http://baby.mall.360.com/detail.3.html" target="_blank">只能摄像机</a>
		</div>
	    <ul class="flyout_menu_sub_ul" style="top: -198px;">
	    	<li><a href="http://jia.mall.360.com/" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t0184159d26a6cd8770.png"> </span> 智能摄像机 </a></li>
			<li><a href="http://luyou.mall.360.com/" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t019beeb99e3c8aa342.png"> </span> 安全路由P1 </a></li>
			<li><a href="http://mall.360.com/shop/item?item_id=54a36b5558d4a62825000025" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t01ef736e95398e82ec.png"> </span> 随身wifi2 </a></li>
			<li><a href="http://wifi.mall.360.com/" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t01205a05f8076d5e31.png"> </span> 随身wifi3 </a></li>
			<li><a href="http://mall.360.com/shop/item?item_id=548d49af58d4a6c20900000a" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t01044aa2632c2d3390.png"> </span> 随身wifi U盘版 </a></li>
			<li><a href="http://air.mall.360.com/" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t01da6b06c00ea6ea3a.png"> </span> 空气卫士 </a></li>
			<li><a href="http://mall.360.com/shop/item?item_id=548abf2b58d4a6ca09000006" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t010a546a5fac925e5a.png"> </span> 微插座 </a></li>
	    	
	    </ul>
   </li>		   
   <li class="flyout_menu_li">
   		<div class="flyout_menu_icon"><img src="${ctx}/static/shop/t01d192608bb999fda5.png"></div>
		<div class="flyout_menu_title">智能车品</div>
		<div class="flyout_menu_links"> 
			<a href="http://baby.mall.360.com/detail.3.html" target="_blank">行车记录仪</a>
		</div>
	    <ul class="flyout_menu_sub_ul">
	    	<li><a href="http://mall.360.com/shop/item?item_id=548ac9dc58d4a60c11000001" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t018ffbf71fd2feca90.png"> </span>行车记录仪 </a></li>
			<li><a href="http://mall.360.com/shop/item?item_id=5582b15058d4a6590d00001a" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t01348d21baee2a87bf.png"> </span>多听车听宝 </a></li>
	    </ul>
   </li>
   <li class="flyout_menu_li">
   		<div class="flyout_menu_icon"><img src="${ctx}/static/shop/t01c7800a6e6eb884c4.png"></div>
		<div class="flyout_menu_title">娱乐游戏 </div>
		<div class="flyout_menu_links"> 
			<a href="http://baby.mall.360.com/detail.3.html" target="_blank">小智音响</a>
			<a href="http://mall.360.com/preorder/dashenf2" target="_blank">魔调耳机</a>
		</div>
	    <ul class="flyout_menu_sub_ul" style="top:-70px">
	    	<li><a href="http://mall.360.com/shop/item?item_id=54bcbb7d58d4a6a264000059" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t01d80fe2c51f1b6552.png"> </span>小智音箱 </a></li>
			<li><a href="http://mall.360.com/shop/item?item_id=54aa887558d4a62825000040" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t019d6f4da0b896754e.png"> </span>大酋长游戏机 </a></li>
			<li><a href="http://mall.360.com/shop/item?item_id=5566d46f58d4a60e12000062" target="_blank"> <span class="list_img"> <img src="${ctx}/static/shop/t016613fab1186f785d.png"> </span>魔调耳机 </a></li>
	    </ul>
   </li>
</ul>
</div>
</div>
</div>