<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>360商城</title>
<link type="image/x-icon" rel="shortcut icon" href="${ctx}/static/images/favicon.png">
<link rel="stylesheet" href="${ctx}/static/grid/col.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/2cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/3cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/4cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/5cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/6cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/7cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/8cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/9cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/10cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/11cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/12cols.css" media="all">

<link type="text/css" rel="stylesheet" href="${ctx}/static/flexSlider/flexslider.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/backtotop/css/style.css">

<style type="text/css">
html, body, div, span,
h1, h2, h3, h4, h5, h6, p, em, img, i,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
}

ul,li {
    list-style: none;
}

a {
    margin: 0;
    padding: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
}
		
.col_1_of_1 {
	width: 1240px;
	margin: 0 auto;
}

/* ------------------------------------------------------------------------------------------- */

.ungrid_row { width: 100%; display: table; table-layout: fixed; border-top: solid 1px #fafafa;}
.ungrid_col { display: table-cell; vertical-align: middle; }

/* ------------------------------------------------------------------------------------------- */

。popup_box input[type=text], input[type="password"]{
	font-size:13px;     
	height: 25px;	
	width:180px;
	padding: 3px;
	margin: 0px;  		  
    outline:none;
	width:180px;
	border:1px solid #D9D9D9;
	border-top-color:#c0c0c0;
	line-height:17px;
	font-size:14px;
	color:#777;
}


.u-btn{display:inline-block;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;padding:0 12px;height:28px;line-height:28px;border:1px solid #2d88bf;font-size:12px;letter-spacing:1px;word-spacing:normal;text-align:center;vertical-align:middle;cursor:pointer;background:#54aede;}
.u-btn,.u-btn:hover{color:#fff;text-decoration:none;}
.u-btn:hover,.u-btn:focus{background:#399dd8;}
.u-btn::-moz-focus-inner{padding:0;margin:0;border:0;}

/* ------------------------------------------------------------------------------------------- */
body{
font-family: "Microsoft YaHei", Tahoma, Verdana;
}

.header_bar {
	width: 100%;
	height: 40px;
	line-height: 40px;
	
	background-color: #fafafa;
	border-bottom: solid 1px #dfdfdf;
	
	font-family: "Microsoft YaHei", "Microsoft JhengHei", Tahoma, Verdana, SimHei;
	font-size: 12px;
}

.header_bar .bar_left{
	display: inline-block; float: left; 
}

.header_bar .bar_right{
	display: inline-block; float: right; 
}

.header_bar a {
    text-decoration: none;
    color: #666;
}

.header_bar a:hover,
.header_bar a:focus,
.header_bar a:active {
    color: #74b72c;
}


.header_bar_nav ul li {
	position:relative;
    display:inline-block;
}

.header_bar_nav li ul {    
    position:absolute;
    left:0;
    top:40px; 
    
    width:120px;
    border: solid 1px #dfdfdf;
    text-align:center;
    
    background-color: #fff;
    z-index: 99;
}


.header_bar_nav li ul.i_sub_menu {
    display:none;
}

.header_bar_nav li li {
    position:relative;
    margin:0;
    display:block;
    border-bottom:solid 1px #fafafa;
}


.header_bar_nav a {
    line-height:40px;
    padding:0 12px;
    margin:0 12px;
}

.header_bar_nav li.i_dropdown > a em {
border-style: solid;
border-width: 6px 5px 0;
border-color: #74b72c transparent;
display: inline-block;
height: 0;
margin-left: 5px;
overflow: hidden;
width: 0;
}

/* ------------------------------------------------------------------------------------------- */

.popup_col { height: 42px; line-height: 42px; }

.popup_box {
    min-width: 350px;
    width: auto;
    border-radius: 2px;
    display: none;
  	box-shadow: 1px 1px 10px rgba(0,0,0,.3) ;
}

.popup_box_head {
    position: relative;
    cursor:move;
	border-top-left-radius: 2px;
	border-top-right-radius: 2px;
  	height: 35px;
  	line-height: 35px;
	background-color: #49a7df; 
}

.popup_box_head .popup_box_head_title {
    color: #fff;
	font: 14px "Microsoft YaHei", Verdana, SimHei, "Microsoft JhengHei", Tahoma;
	padding-left: 10px;
}

.popup_box_head .popup_box_head_close {
    float: right;    
    margin-right: 10px;
	margin-top: 4px;  
    display: block;height: 22px;width: 22px;
    background: url(${ctx}/static/images/win_close.png) no-repeat -22px 0px;   
}

.popup_box_head .popup_box_head_close:hover {    
    background: url(${ctx}/static/images/win_close.png) no-repeat -22px -22px;        
}

.popup_box_head .ui-box-head-text-360 {
    margin-left: 10px;
    color: #808080;
    float: left;
}

.popup_box_container {
    background: #fff;
    border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;   
}

.popup_box_content {
	text-align:center;
	padding: 0px;
	margin: 0px;
	min-height: 200px;
	max-height: 300px;
	width: auto;
	height: auto;
	display:block;
	font: 12px "Microsoft YaHei", Verdana, SimHei, "Microsoft JhengHei", Tahoma;
	overflow-y:auto;
	overflow-x:hidden;
}

div.popup_box_content span {
    padding-top:90px;
    vertical-align: middle;
    color: #00335E;
}

.popup_box_footer {     
    text-align: center;    
    border-top-style: solid;
    border-top-color: #e4e5e9;
    border-top-width: 1px;	
	height: 42px;
	background-color: #F6F9FC;
	border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;
	padding-top: 5px;
}

.popup_box_btn_gray:hover {
color: #fff;
}
.popup_box_btn_gray:hover {
background: #3083eb;
border-color: #1f74df;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3083eb',endColorstr='#3083eb',GradientType=0);
}
.popup_box_btn_gray {
background-image: -webkit-linear-gradient(bottom,#f5f9fc,#fff);
background-image: -moz-linear-gradient(bottom,#f5f9fc,#fff);
background-image: -o-linear-gradient(bottom,#f5f9fc,#fff);
background-image: linear-gradient(to top,#f5f9fc,#fff);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff',endColorstr='#f5f9fc',GradientType=0);
}
.popup_box_btn {
-khtml-user-select: none;
background: #fff;
border-radius: 2px;
border-width: 1px;
border-style: solid;
border-color: #bbcbdf;
color: #536083;
cursor: pointer;
display: inline-block;
font-size: 12px;
font-family: arial;
min-width: 47px;
overflow: hidden;
padding: 10px 14px;
text-align: center;
-moz-user-select: none;
-webkit-user-select: none;
-ms-user-select: none;
user-select: none;
}

.b-iframe{
    overflow: hidden; border: none; padding: 0; margin: 0; width:600px; height:280px;
}

.popup_box label.error {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
  padding-left: 16px;
  vertical-align:middle;
  font-size: 11px;
  color: #EA5200;
  margin-left: 2px;
  width: auto;
  display: inline;
}

.popup_box label.checked {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
}


/* ------------------------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------------------------- */
.prodIntro {
  width: 1000px;  
  margin: 0px auto;
  overflow: hidden;
  background-color: #fff;
  color: #666;
  font: 12px "Microsoft YaHei",Verdana,SimHei,"Microsoft JhengHei",Tahoma;
}

.prodIntro .sPic {
  float: left;
  width: 540px;    
  overflow: hidden;
}

.prodIntro .sInfo {
  float: right;
  width: 460px;
}
/* ------------------------------------------------------------------------------------------- */

.flexslider { 
position: relative; height: auto;  padding: 0px; width: 100%;   width: 520px;
margin: 0px; overflow: hidden; background: url(${ctx}/static/flexSlider/images/loading.gif) 50% no-repeat;
border: 0; outline: 0;
}


.slides { position: relative; list-style: none outside none;padding: 0; margin: 0; }

.flexslider .slides img {
	height: 500px;
    width: 520px;
}


.flex-control-thumbs li {
    height: 72px;
    width: 72px;
    overflow: hidden;    
}

.flex-control-thumbs img {    
    width: 68px; 
    height: 68px;
    padding: 1px;
    opacity: 0.8;
    border: 1px #e8e8e8 solid;      
}

.flex-control-thumbs .flex-active {
	border: 2px #7c7c7c solid;
	opacity: 1;
    padding: 0;
} 

/* ------------------------------------------------------------------------------------------- */
.prodIntro .sInfo .tr {
    margin-top: 25px;
    overflow: hidden;
    padding-bottom: 8px;
}

.prodIntro .sInfo .hr {
   border-bottom: 1px #e6e9ed solid;
}

.prodIntro .sInfo .tr strong {
    color: #000;
    display: block;
    font-size: 30px;
    font-weight: 400
}

.prodIntro .sInfo .tr .txt {
    clear: both;
    overflow: hidden;
    padding-bottom: 22px;
    position: relative
}

.prodIntro .sInfo .tr p.slogan {
    color: #000;
    font-size: 14px;
    line-height: 24px;
    padding: 20px 0;
}
.prodIntro .sInfo .tr p.mianze {
    color: #666;
    font-size: 12px;
    font-family: simsun;
    line-height: 24px
}
.prodIntro .sInfo .tr .txt span {
    color: #000;
    float: left;
    font-size: 14px;
    margin-top: 10px;
    overflow: hidden;
    width: 55px
}

.prodIntro .sInfo .tr .txt .nowprice {
    color: #f33a35;
    display: inline-block;
    font-size: 36px;
    font-style: normal;
    margin: -6px 10px 0 0;
    vertical-align: middle
}
.prodIntro .sInfo .tr .txt .oldprice {
    color: #000;
    font-size: 14px;
    font-style: normal;
    text-decoration: line-through
}
.prodIntro .sInfo .tr .txt .nowprice em {
    font-size: 30px;
    font-style: normal
}

.prodIntro .sInfo .tr .txt a.lk,
.prodIntro .sInfo .tr .txt a.lk:link {
    color: #666;
    font-size: 12px;
    text-decoration: none;
    vertical-align: 1px
}
.prodIntro .sInfo .errTip {
    color: #ff1f01;
    display: none;
    font-size: 12px;
    left: 185px;
    position: absolute;
    top: 10px
}

.prodIntro .sInfo .tr .gcIpt {
    float: left;
    margin-top: 5px;
    margin-bottom: 5px;
    text-align: center
}

.prodIntro .sInfo .tr .gcIpt input {
    background: #fff;
    border: 1px #ccc solid;
    color: #000;
    font-family: arial;
    font-size: 16px;
    height: 30px;
    line-height: 30px;
    
    text-align: center;
    vertical-align: middle;
    width: 50px
}
.prodIntro .sInfo .tr .gcIpt a.increment {
    margin-left: 3px;
} 
.prodIntro .sInfo .tr .gcIpt input:focus {
    outline: none
}

.prodIntro .sInfo .tr .gcIpt a {
  background-color: #f6f6f6;
  border: 1px solid #ccc;
  display: inline-block;
  font-size: 40px;
  height: 32px;
  line-height: 24px;
  
  vertical-align: middle;
  width: 36px;
  
  color: #666;
  outline: none;
  text-decoration: none;
}


.prodIntro .sInfo .glist {
    color: #666;
    float: left;
    font-size: 14px;
    width: 249px;
    list-style: none;
}
.prodIntro .sInfo .glist li {
    _display: inline;
    float: left;
    margin-right: 10px;
    margin-bottom: 5px
}

.prodIntro .sInfo .glist li a,
.prodIntro .sInfo .glist li a:link {
    border: 1px #ddd solid;
    display: inline-block;
    padding: 2px
}
.prodIntro .sInfo .glist li i {
    display: inline-block;
    height: 26px;
    vertical-align: middle;
    width: 26px
}
.prodIntro .sInfo .glist li u {
    color: #333;
    display: none;
    font-size: 12px;
    text-decoration: none
}
.prodIntro .sInfo .glist li a:hover {
    border-color: #7c7c7c;
    text-decoration: none
}
.prodIntro .sInfo .glist li.cur u {
    display: block
}
.prodIntro .sInfo .glist li.cur a {
    border: 2px #7c7c7c solid;
    padding: 1px;
    position: relative
}
.prodIntro .sInfo .glist li.out a,
.prodIntro .sInfo .glist li.out a:link,
.prodIntro .sInfo .glist li.out a:hover {
    border-color: #ededed;
    color: #ddd
}


.prodIntro .sInfo .getCart,
.prodIntro .sInfo .sellOut {
    background: #f33e39;
    color: #fff;
    display: inline-block;
    font-size: 18px;
    font-weight: 700;
    height: 45px;
    line-height: 45px;
    text-align: center;
    width: 180px;
    text-decoration: none;
}
.prodIntro .sInfo .sellOut {
    background-color: #646464;
    cursor: default
}
.prodIntro .sInfo .nobdr {
    border: 0
}
.prodIntro .sInfo .getCart:hover {
    background-color: #dd2a25
}




.gInfoBar {
    background: rgba(242, 242, 242, 0.9);
    height: 60px;
    left: 0;
    position: fixed;
    top: -61px;
    width: 100%;
    filter: progid: DXImageTransform.Microsoft.gradient(startColorstr=#e5f2f2f2, endColorstr=#e5f2f2f2);
    zoom: 1
}

.gInfoBar .con {
    height: 40px;
    margin: 0 auto;
    overflow: hidden;
    padding: 10px;
    width: 940px
}
.gInfoBar .con .ginfo {
    _display: inline;
    float: right;
    margin-right: 10px;
    overflow: hidden;
    text-align: right;
    *zoom: 1
}
.gInfoBar .con .fl {
    float: left;
    height: 40px;
    line-height: 40px;
    padding: 0 25px
}
.gInfoBar .con .ginfo img {
    float: left;
    height: 40px;
    margin-right: 10px;
    width: 40px
}
.gInfoBar .con .ginfo .txt {
    float: left;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 190px
}
.gInfoBar .con .ginfo .txt span {
    display: block
}
.gInfoBar .con .ginfo .txt .name {
    color: #000;
    font-size: 14px;
    height: 20px;
    margin-bottom: 5px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 190px
}
.gInfoBar .con .ginfo .txt .price {
    color: #f33a35;
    font-size: 14px;
    font-family: arial;
    font-weight: 700
}
.gInfoBar .con .fl a,
.gInfoBar .con .fl a:link {
    color: #333;
    font-size: 16px;
    padding: 17px 5px;
    text-decoration: none;
}
.gInfoBar .con .fl a:hover {
    color: #000;
    font-weight: bold;
}
.gInfoBar .con .cur a,
.gInfoBar .con .cur a:link,
.gInfoBar .con .cur a:hover {
    color: #7abb30
}
.gInfoBar .con .sellOut,
.gInfoBar .con a.getCart,
.gInfoBar .con a.getCart:link,
.gInfoBar .con a.getCart:hover {
    background: #f33e39;
    color: #fff;
    float: right;
    font-size: 16px;
    font-weight: 700;
    height: 40px;
    line-height: 40px;
    text-align: center;
    width: 120px
}
.gInfoBar .con .sellOut {
    background: #646464;
    cursor: default
}



.gdetail {
    width: 1000px;  
  	margin: 0px auto;
    background-color: #fff;
    padding-top: 30px;
}

.gdetail .gTab {
    background: #fff;
    border-top: 1px #e6e9ed solid;
    border-bottom: 1px #e6e9ed solid;
    color: #333;
    font-size: 20px;
    height: 70px;
    line-height: 70px;
    
}

.gdetail .gTab a,
.gdetail .gTab a:link {
    color: #2c2727;
    margin: 0 30px;
    padding: 20px 3px;
    text-decoration: none
}
.gdetail .gTab a:hover,
.gdetail .gTab a.cur,
.gdetail .gTab a.cur:link,
.gdetail .gTab a.cur:hover {
    color: #000;
    /* font-weight: 600; */ 
    font-weight: bold;    
}



.gdetail  .col2 {
    border: 1px #ccc solid;
    border-bottom: 0;
    margin-top: -1px;
    width: 100%
}

.gdetail .col2 td {
    border-bottom: 1px #ccc solid;
    color: #666;
    font-size: 16px;
    height: 40px;
    line-height: 40px;
    text-align: left;
    text-indent: 16px
}
.gdetail  td.wd207 {
    border-right: 1px #ccc solid;
    color: #333;
    text-align: center!important;
    text-indent: 0;
    width: 207px
}



.gdetail .consult {
    font-size: 13px;
	text-align: left;
}

.gdetail .consult dt,
.gdetail .consult dd {
    color: #333;
    display: block;
    line-height: 30px;
    margin-bottom: 15px
}
.gdetail .consult dd {
    background-position: 0 -45px;
    color: #666;
    margin-bottom: 20px;
    padding: 0 0 15px 35px
}

.gdetail .consult dt label {
    display: inline-block;
    font-size: 18px;
    width: 35px;
}
.gdetail .consult dt i,
.gdetail .consult dd i {
    background: url(http://p0.qhimg.com/t0147bb66948b7f6357.png) left top no-repeat;
    display: inline-block;
    height: 30px;
    margin-right: 5px;
    vertical-align: middle;
    width: 35px
}
.gdetail .consult dd i {
    background-position: 0 -45px
}
.gdetail .consult dd.last {
    border-bottom: none
}
.gdetail .consult dd .consult_info,
.gdetail .consult dd .consult_editor {
    margin-top: 15px
}
.gdetail .consult dd .consult_editor {
    background: #f5f8f1;
    display: block;
    height: 137px;
    overflow: hidden;
    width: 645px
}
.gdetail .consult dd .consult_editor textarea {
    border: 1px #c9c9c9 solid;
    display: block;
    height: 70px;
    margin: 15px auto 5px;
    resize: none;
    width: 613px
}
.gdetail .consult dd .consult_info .user {
    color: #666;
    margin-right: 20px
}
.gdetail .consult dd .consult_info .time {
    color: #999
}
.gdetail .consult dd .consult_info .edit {
    background: #ff890f;
    border-bottom: 2px #c56e14 solid;
    border-radius: 3px;
    color: #fff;
    font-size: 12px;
    height: 16px;
    line-height: 16px;
    margin-left: 10px;
    padding: 0 5px;
    text-align: center
}
.gdetail .consult dd .consult_editor .reply {
    background: #82c92f;
    color: #fff;
    float: right;
    font-size: 12px;
    height: 30px;
    line-height: 30px;
    margin-right: 13px;
    text-align: center;
    text-decoration: none;
    width: 74px
}

</style>
<script type="text/javascript" src="${ctx}/static/jquery/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="${ctx}/static/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="${ctx}/static/flexSlider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="${ctx}/static/backtotop/js/backtotop.js"></script>

</head>
<body>
<c:if test="${not empty loginNameErrorMessage || not empty passwordErrorMessage}">			
<script type="text/javascript">		
$(document).ready(function(){	
	$('#loginbox').bPopup({
		autoClose: false,
		modal: false,	 	
	 	modalClose: false, 
	 	opacity: 0.6,
	    speed: 500,
	    transition: 'slideBack'        
	});
});
</script>
</c:if>

<c:choose>
  <c:when test="${not empty httpSession}">		  	
  	<input type="hidden" id="authenticated" value="true" />
  </c:when>
  <c:otherwise>
  	<input type="hidden" id="authenticated" value="false" />
  </c:otherwise> 
</c:choose>

<div class="header_bar">
<div class="col_1_of_1">
	<div class="bar_left">
		<a href="http://mall.360.com/#" onclick="return false">收藏360商城</a>
	</div>
	<div class="bar_right">
	<div class="header_bar_nav">
		<ul>
		    <c:choose>
			  <c:when test="${not empty httpSession}">		  	
			  	<li class="i_dropdown" id="i_dropdown">
		            <a href="#">${httpSession.customer.username}<em></em></a>
		            <ul class="i_sub_menu" id="i_sub_menu">
		                <li><a href="#">个人中心</a></li>
		                <li><a href="#">我的试用</a></li>
		                <li><a href="#">试用积分</a></li>                
		                <li><a href="#">收货地址</a></li>
		                <li><a href="#">账户设置</a></li>
		                <li><a href="javascript:;" onclick="logout()">退出登录</a></li>
		            </ul>
		    	</li>
			  </c:when>
			  <c:otherwise>
			  	<li><a href="javascript:;" onclick="loginShow()">登录</a></li>
				<li><a href="javascript:;" onclick="registerShow()">注册</a></li>
			  </c:otherwise> 
			</c:choose>
		    <li><a href="javascript:;">我的订单</a></li>
			<li><a href="javascript:;">手机360商城</a></li>
			<li><a href="javascript:;">企业采购</a></li>
			<li><a href="javascript:;">帮助中心</a></li>        
		</ul>
	</div>
	</div>
</div>
</div>
<!-- ******************************************************************************************** -->

<!-- ******************************************************************************************** -->

<!-- ******************************************************************************************** -->
<form id="mycartForm">
<div class="prodIntro">
	<div class="sPic">
		<div class="flexslider">
		  <ul class="slides">
		    <li data-thumb="${ctx}/static/item/chongdianqi/t01319d2efef7a9ccd9.jpg">
		      <img src="${ctx}/static/item/chongdianqi/t01319d2efef7a9ccd9.jpg" />
		    </li>
		    <li data-thumb="${ctx}/static/item/chongdianqi/t017ffff760a7081fda.jpg">
		      <img src="${ctx}/static/item/chongdianqi/t017ffff760a7081fda.jpg" />
		    </li>
		    <li data-thumb="${ctx}/static/item/chongdianqi/t01ef794ca7a4c17f42.jpg">
		      <img src="${ctx}/static/item/chongdianqi/t01ef794ca7a4c17f42.jpg" />
		    </li>		    
		    <li data-thumb="${ctx}/static/item/chongdianqi/t0178b49eff6c25d9df.jpg">
		      <img src="${ctx}/static/item/chongdianqi/t0178b49eff6c25d9df.jpg" />
		    </li>
		    <li data-thumb="${ctx}/static/item/chongdianqi/t01a6a0f958b30a7b6e.jpg">
		      <img src="${ctx}/static/item/chongdianqi/t01a6a0f958b30a7b6e.jpg" />
		    </li>
		  </ul>
		</div>
	</div>
	<div class="sInfo">
		<div class="tr hr"> 
			<strong>360超级充电器（玫瑰红）</strong> 
	        <p class="slogan">七夕价：7块7 18:00开抢，只限20个名额，抢完为止。别忘了需要先注册，登录后才能参与抢购哟！</p>
	    </div>
	    <div class="tr">
	        <div class="txt"> <span>价格：</span><strong class="nowprice"><em>￥</em>39</strong>  <i class="oldprice">市场价:￥39</i> 
	        </div>
	        <div class="txt"> <span>分类：</span> 
	            <ul class="glist" id="glist">
	                <li class="cur">
	                    <a href="${ctx}/static/item/chongdianqi//item_id55cc63c95efb1150798b4567.html" title="玫瑰红">
	                        <img src="${ctx}/static/item/chongdianqi//t012677154f3b7b03c6.jpg" alt="玫瑰红">
	                    </a>
	                </li>
	            </ul>
	        </div>
	        <div class="txt"> <span>数量：</span> 
	            <div class="gcIpt"> <a href="javascript:void(0);" class="decrement" data-num="-1">-</a>
	                <input type="text" name="itemCount" class="goodsCount" value="1"><a href="javascript:void(0);" class="increment" data-num="1">+</a>
	            </div>
	        </div>
	        <p class="cl mianze"></p>
	    </div>
	    <div class="tr">   
	    	<a href="javascript:void(0);" class="getCart" onclick="addToMyCart()">加入购物车</a>    
	    </div>
	</div>
</div>			 
</form>   
<!-- ******************************************************************************************** -->

<div class="gdetail">
	<div class="gTab"> 
		<a href="javascript:void(0);" class="cur">产品详情</a>  
		<a href="http://mall.360.com/rush/item?item_id=55cc63c95efb1150798b4567#gArg">规格参数</a>  
		<a href="http://mall.360.com/rush/item?item_id=55cc63c95efb1150798b4567#gConsult">常见问题</a> 
	</div>
	<div class="gCon">
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t010772d107fc9c72a4.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t01d0e147a6adf19905.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t01de09cb3f9c0c75f3.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t01227837f4de8bcc72.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t01111a2e235f23b625.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t012036556596c9374c.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t01b596f89a05fe2c82.jpg">
	    </p>
	    <p style="white-space: normal;">
	        <img src="${ctx}/static/item/chongdianqi//t01d8685ee257ed9223.jpg">
	    </p>
	    <p>
	        <br>
	    </p>
	</div>
	<!-- <div class="gTit">规格参数
    	<a style="display:block;margin-top:-120px;visibility:hidden"></a>
    </div> -->
	
	<table class="col2">
    <tbody>
        <tr>
            <td class="wd207">产品名称</td>
            <td>360超级充电器-桌面版</td>
        </tr>
        <tr>
            <td class="wd207">包装清单</td>
            <td>充电器x1、电源线x1、说明书x1</td>
        </tr>
        <tr>
            <td class="wd207">表面工艺</td>
            <td>底盖磨砂哑面，顶部高光面</td>
        </tr>
        <tr>
            <td class="wd207">整体尺寸</td>
            <td>81*58*27mm</td>
        </tr>
        <tr>
            <td class="wd207">整体颜色</td>
            <td>蓝色</td>
        </tr>
        <tr>
            <td class="wd207">LOGO显示方式</td>
            <td>丝印</td>
        </tr>
        <tr>
            <td class="wd207">插头外观工艺</td>
            <td>人体工程学</td>
        </tr>
        <tr>
            <td class="wd207">LED灯颜色</td>
            <td>绿色</td>
        </tr>
        <tr>
            <td class="wd207">USB端口颜色</td>
            <td>绿色（色号：7726C）</td>
        </tr>
        <tr>
            <td class="wd207">电源线材颜色</td>
            <td>白色 一体式</td>
        </tr>
        <tr>
            <td class="wd207">包装方式</td>
            <td>主体毛绒袋+线材单体</td>
        </tr>
        <tr>
            <td class="wd207">塑料材质</td>
            <td>PC阻燃材料94V0</td>
        </tr>
        <tr>
            <td class="wd207">PCBA板颜色</td>
            <td>黑色</td>
        </tr>
        <tr>
            <td class="wd207">PVBA散热片</td>
            <td>黄色绝缘胶带</td>
        </tr>
        <tr>
            <td class="wd207">PCBA方案</td>
            <td>同步整流+单口限流</td>
        </tr>
        <tr>
            <td class="wd207">电源线长度</td>
            <td>1.2米</td>
        </tr>
        <tr>
            <td class="wd207">电源线材质</td>
            <td>PVC</td>
        </tr>
        <tr>
            <td class="wd207">线芯规格</td>
            <td>3*0.75mm</td>
        </tr>
        <tr>
            <td class="wd207">插头材料</td>
            <td>阻燃材料</td>
        </tr>
        <tr>
            <td class="wd207">插头规格</td>
            <td>国标2孔</td>
        </tr>
        <tr>
            <td class="wd207">线材硬度</td>
            <td>65PVC</td>
        </tr>
        <tr>
            <td class="wd207">包装材质</td>
            <td>白铜400G/表面过哑胶</td>
        </tr>
        <tr>
            <td class="wd207">过载保护</td>
            <td>6.5AMAX</td>
        </tr>
        <tr>
            <td class="wd207">防雷保护</td>
            <td>1KV</td>
        </tr>
        <tr>
            <td class="wd207">防浪涌保护</td>
            <td>小于60A</td>
        </tr>
        <tr>
            <td class="wd207">防火级别</td>
            <td>94V0</td>
        </tr>
        <tr>
            <td class="wd207">漏电保护阀值</td>
            <td>小于0.25mA接触电流</td>
        </tr>
        <tr>
            <td class="wd207">额定功率</td>
            <td>25W</td>
        </tr>
        <tr>
            <td class="wd207">技术参数</td>
            <td>5V5A</td>
        </tr>
        <tr>
            <td class="wd207">USB充电接口</td>
            <td>4Port</td>
        </tr>
        <tr>
            <td class="wd207">USB单口输出</td>
            <td>5V2.4A</td>
        </tr>
        <tr>
            <td class="wd207">USB总功率</td>
            <td>5V5A 25W</td>
        </tr>
        <tr>
            <td class="wd207">USB识别芯片</td>
            <td>智能侦测IC （芯卓微）</td>
        </tr>
        <tr>
            <td class="wd207">USB充电效率</td>
            <td>85%</td>
        </tr>
        <tr>
            <td class="wd207">耐高温性能</td>
            <td>100度不变形</td>
        </tr>
    </tbody>
	</table>
    
    <dl class="consult"> 
    	<dt><label>1.</label><i></i>订单提交成功后还可以修改收货信息吗？ </dt>
	    <dd><i></i>订单付款之前，您可以进入“我的订单”，在订单详情页内修改收货信息。付款之后将不可修改收货信息。</dd>
	    <dt><label>2.</label><i></i>支付完成后还能取消订单吗？如何取消？ </dt>
	    <dd><i></i>支付完成后，配货之前可以取消订单，您可以进入“我的订单”，直接点击订单后面取消按钮。</dd>
	    <dt><label>3.</label><i></i>订单取消后还能恢复吗？ </dt>
	    <dd><i></i>订单一旦取消后将无法恢复，请您慎重操作。</dd>
	    <dt><label>4.</label><i></i>订单取消成功后退款如何返还？ </dt>
	    <dd><i></i>订单取消后，退款会按照你购买时的支付方式原路返回到您的银行卡/支付宝账户。</dd>
	    <dt><label>5.</label><i></i>对商品不满意可以申请退换货吗？如何操作？ </dt>
	    <dd><i></i>在确认收货后，7天之内可以申请退货，15之内可以进行换货，具体操作如下:
	        <br>退货：进入“我的订单”，在操作区域中点击申请退货，填写问题描述，提交服务单即可申请退货。
	        <br>换货：请拨打我们的客服热线，在客服的指导下完成换货。</dd>
	    <dt><label>6.</label><i></i>为什么我的订单总是无法提交成功？ </dt>
	    <dd><i></i>可能存在以下几种情况：
	        <br>（1）订单信息填写不完整
	        <br>（2）订单商品库存不足或者库存无货；
	        <br>（3）网络延时及以上各种情况都会在页面中弹出提示信息，可以通过修改订单信息（提示信息）或者稍后再试，即可成功提交订单。</dd>
	    <dt><label>7.</label><i></i>订单已提交成功，如何付款？ </dt>
	    <dd><i></i>您好，奇酷目前支持的支付方式分为以下几种，请在订单提交后2小时内付款完成：
	        <br>（1）网上银行
	        <br>（2）第三方支付：包括支付宝和快钱。</dd>
	    <dt><label>8.</label><i></i>订单已支付成功，什么时候可以发货？ </dt>
	    <dd><i></i>您好，订单提交成功后我们会尽快发货，详细进度您可进入“我的订单”实时查看。详见 <a href="http://mall.360.com/user/myorder" target="_blank">我的订单&gt;&gt;</a> 
	    </dd>
	    <dt><label>9.</label><i></i>订单发货后，还可以改送到其他地方吗？ </dt>
	    <dd><i></i>订单一旦提交成功，将无法修改。请在提交订单前仔细检查核对。</dd>
	    <dt><label>10.</label><i></i>我的地址比较偏僻，你们能送到吗？ </dt>
	    <dd><i></i>一般情况下，邮局可覆盖的范围我们均可以为您配送到。</dd>
	    <dt><label>11.</label><i></i>我订购的手机，忘记选择发票怎么办？ </dt>
	    <dd><i></i>手机类商品，为了保证您能充分享受生产厂家提供的售后服务（售后服务需根据发票确认您的购买日期），不管您是否选择开具发票，都将随单为您开具，发票内容默认为您订购的商品全明细，不支持修改发票内容。请认真填写发票抬头并核对，若您未填写发票抬头，默认抬头“个人“，订单付款前是可以自行修改发票内容。</dd>
	</dl>            
	
  	
</div>

<!-- ******************************************************************************************** -->

<div class="gInfoBar" id="gInfoBar" style="top: -61px;">
    <div class="con">
        <div class="ginfo">
            <img src="${ctx}/static/item/chongdianqi//t01319d2efef7a9ccd9.jpg">
            <div class="txt"><span class="name">【七夕七块七】360超级充电器（玫瑰红）</span><span class="price">￥7.7</span></div>
        </div>
        <div class="fl"><a href="http://mall.360.com/rush/item?item_id=55cc63c95efb1150798b4567#gInfo" class="gInfoH">产品详情</a>
        </div>
        <div class="fl"><a href="http://mall.360.com/rush/item?item_id=55cc63c95efb1150798b4567#gArg">规格参数</a>
        </div>
        <div class="fl"><a href="http://mall.360.com/rush/item?item_id=55cc63c95efb1150798b4567#gConsult">常见问题</a>
        </div> <a href="javascript:void(0);" class="presale">即将开启</a>  <a href="javascript:void(0);" class="getcart" style="display:none">立即抢购</a> 
    </div>
</div>
<!-- ******************************************************************************************** -->


<!-- ******************************************************************************************** -->
<a href="javascript:;" class="cd-top">Top</a>

<!-- ******************************************************************************************** -->
<div id="loginbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">用户登录</span>        
        <a href="javascript:loginClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">  
    	<form id="loginForm">    
    	<input type="hidden" id="itemId" name="itemId" value="${itemId}" />
        <div class="popup_box_content">               	 
    		 <div style="width: 420px;">
	    		 <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="loginName">用户名:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="loginName" name="loginName" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_loginName_error_div"></div>			        			        
			    </div>			    
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="password">密码:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="password" name="password" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_password_error_div"></div>		        		        
			    </div>
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col">
			        	<div id="login_loginName_msg_div" style="color: red; display: inline;">${loginNameErrorMessage}</div>
			        	<div id="login_password_msg_div" style="color: red; display: inline;">${passwordErrorMessage}</div>							
			        </div>
			        		        		        
			    </div>
			    
    		</div>        	 
        </div>        
        <div class="popup_box_footer">					
			<input type="button" class="popup_box_btn popup_box_btn_gray" value="登 录" onclick="loginPost()" />							
		</div>
		</form>		        
    </div>
</div>
<!-- ******************************************************************************************** -->

<script type="text/javascript">
$(document).ready(function(){
	$("#i_dropdown").hover(
		function(){
			$(this).children("#i_sub_menu").slideDown(200);
		},
	    function(){
	    	$(this).children("#i_sub_menu").slideUp(200);
		}
	);
	$(window).scroll(function(){
		$(document).scrollTop()-$(".gTab").offset().top>61&&$("#gInfoBar").css("top")=="-61px"?$("#gInfoBar").animate({top:"0"},500):$(document).scrollTop()-$(".gTab").offset().top<0&&$("#gInfoBar").css("top")=="0px"&&$("#gInfoBar").animate({top:"-61px"},500)
	});
	
	$("#glist").delegate("li","click",function(e){if($(this).attr("class")=="cur")return!1});
	$(".gInfoH").click(function(){$("#gInfoBar").animate({top:"-61px"},500)});
	
	$('.flexslider').flexslider({
		useCSS: true, 
		animation: "slide",
	    controlNav: "thumbnails"
	});
});

function loginShow(){
	$('#loginbox').bPopup({
	 	modal: false,
	 	opacity: 0.6,
	 	modalClose: false, 
        speed: 500,
        transition: 'slideDown'        
 	});
}

function loginClose(){		
	$("#login_loginName_msg_div").empty();
	$("#login_password_msg_div").empty(); 
	$("#login_loginName_error_div").empty();
	$("#login_password_error_div").empty();	
	
	closeEventFlag = true;
	$("#loginbox").bPopup().close(); 	
}

function loginPost(){		
	var form = $("#loginForm");
	form.attr("action","${ctx}/item/login");
	form.attr("method","post");
	form.submit();
            	
	return false;
}

function logout(){
	var form = $("#loginForm");
	form.attr("action","${ctx}/item/logout");
	form.attr("method","post");
	form.submit();
    return false; 
}

function addToMyCart(){
	var authenticated = document.getElementById("authenticated").value;
	var itemId = document.getElementById("itemId").value; 
	//alert(itemId);
	if(authenticated=="true"){
		var form = $("#mycartForm");
		form.attr("action","${ctx}/mycart/addItemToCart/"+itemId);
		form.attr("method","post");
		form.submit();		
	}else if(authenticated=="false"){
		loginShow();
	}	
}
</script>
</body>
</html>