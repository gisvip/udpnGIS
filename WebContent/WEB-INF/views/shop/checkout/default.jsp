<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>360商城</title>
<link type="image/x-icon" rel="shortcut icon" href="${ctx}/static/images/favicon.png">
<link rel="stylesheet" href="${ctx}/static/grid/col.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/2cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/3cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/4cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/5cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/6cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/7cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/8cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/9cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/10cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/11cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/12cols.css" media="all">

<link type="text/css" rel="stylesheet" href="${ctx}/static/flexSlider/flexslider.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/backtotop/css/style.css">

<style type="text/css">
html, body, div, span,
h1, h2, h3, h4, h5, h6, p, em, img, i,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
}

ul,li {
    list-style: none;
}

a {
    margin: 0;
    padding: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
}
		
.col_1_of_1 {
	width: 1240px;
	margin: 0 auto;
}

/* ------------------------------------------------------------------------------------------- */

.ungrid_row { width: 100%; display: table; table-layout: fixed; border-top: solid 1px #fafafa;}
.ungrid_col { display: table-cell; vertical-align: middle; }

/* ------------------------------------------------------------------------------------------- */

input[type=text], input[type="password"]{
	font-size:13px;     
	height: 25px;	
	width:180px;
	padding: 3px;
	margin: 0px;  		  
    outline:none;
	width:180px;
	border:1px solid #D9D9D9;
	border-top-color:#c0c0c0;
	line-height:17px;
	font-size:14px;
	color:#777;
}


.u-btn{display:inline-block;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;padding:0 12px;height:28px;line-height:28px;border:1px solid #2d88bf;font-size:12px;letter-spacing:1px;word-spacing:normal;text-align:center;vertical-align:middle;cursor:pointer;background:#54aede;}
.u-btn,.u-btn:hover{color:#fff;text-decoration:none;}
.u-btn:hover,.u-btn:focus{background:#399dd8;}
.u-btn::-moz-focus-inner{padding:0;margin:0;border:0;}

/* ------------------------------------------------------------------------------------------- */

.header_bar {
	width: 100%;
	height: 40px;
	line-height: 40px;
	
	background-color: #fafafa;
	border-bottom: solid 1px #dfdfdf;
	
	font-family: "Microsoft YaHei", "Microsoft JhengHei", Tahoma, Verdana, SimHei;
	font-size: 12px;	
}

.header_bar .bar_left{
	display: inline-block; float: left; 
}

.header_bar .bar_right{
	display: inline-block; float: right; 
}

.header_bar a {
    text-decoration: none;
    color: #666;
}

.header_bar a:hover,
.header_bar a:focus,
.header_bar a:active {
    color: #74b72c;
}


.header_bar_nav ul li {
	position:relative;
    display:inline-block;
}

.header_bar_nav li ul {    
    position:absolute;
    left:0;
    top:40px; 
    
    width:120px;
    border: solid 1px #dfdfdf;
    text-align:center;
    
    background-color: #fff;
    z-index: 99;
}


.header_bar_nav li ul.i_sub_menu {
    display:none;
}

.header_bar_nav li li {
    position:relative;
    margin:0;
    display:block;
    border-bottom:solid 1px #fafafa;
}


.header_bar_nav a {
    line-height:40px;
    padding:0 12px;
    margin:0 12px;
}

.header_bar_nav li.i_dropdown > a em {
border-style: solid;
border-width: 6px 5px 0;
border-color: #74b72c transparent;
display: inline-block;
height: 0;
margin-left: 5px;
overflow: hidden;
width: 0;
}

/* ------------------------------------------------------------------------------------------- */

.popup_col { height: 42px; line-height: 42px; }

.popup_box {
    min-width: 350px;
    width: auto;
    border-radius: 2px;
    display: none;
  	box-shadow: 1px 1px 10px rgba(0,0,0,.3) ;
}

.popup_box_head {
    position: relative;
    cursor:move;
	border-top-left-radius: 2px;
	border-top-right-radius: 2px;
  	height: 35px;
  	line-height: 35px;
	background-color: #49a7df; 
}

.popup_box_head .popup_box_head_title {
    color: #fff;
	font: 14px "Microsoft YaHei", Verdana, SimHei, "Microsoft JhengHei", Tahoma;
	padding-left: 10px;
}

.popup_box_head .popup_box_head_close {
    float: right;    
    margin-right: 10px;
	margin-top: 4px;  
    display: block;height: 22px;width: 22px;
    background: url(${ctx}/static/images/win_close.png) no-repeat -22px 0px;   
}

.popup_box_head .popup_box_head_close:hover {    
    background: url(${ctx}/static/images/win_close.png) no-repeat -22px -22px;        
}

.popup_box_head .ui-box-head-text-360 {
    margin-left: 10px;
    color: #808080;
    float: left;
}

.popup_box_container {
    background: #fff;
    border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;   
}

.popup_box_content {
	text-align:center;
	padding: 0px;
	margin: 0px;
	min-height: 200px;
	max-height: 300px;
	width: auto;
	height: auto;
	display:block;
	font: 12px "Microsoft YaHei", Verdana, SimHei, "Microsoft JhengHei", Tahoma;
	overflow-y:auto;
	overflow-x:hidden;
}

div.popup_box_content span {
    padding-top:90px;
    vertical-align: middle;
    color: #00335E;
}

.popup_box_footer {     
    text-align: center;    
    border-top-style: solid;
    border-top-color: #e4e5e9;
    border-top-width: 1px;	
	height: 42px;
	background-color: #F6F9FC;
	border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;
	padding-top: 5px;
}

.popup_box_btn_gray:hover {
color: #fff;
}
.popup_box_btn_gray:hover {
background: #3083eb;
border-color: #1f74df;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3083eb',endColorstr='#3083eb',GradientType=0);
}
.popup_box_btn_gray {
background-image: -webkit-linear-gradient(bottom,#f5f9fc,#fff);
background-image: -moz-linear-gradient(bottom,#f5f9fc,#fff);
background-image: -o-linear-gradient(bottom,#f5f9fc,#fff);
background-image: linear-gradient(to top,#f5f9fc,#fff);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff',endColorstr='#f5f9fc',GradientType=0);
}
.popup_box_btn {
-khtml-user-select: none;
background: #fff;
border-radius: 2px;
border-width: 1px;
border-style: solid;
border-color: #bbcbdf;
color: #536083;
cursor: pointer;
display: inline-block;
font-size: 12px;
font-family: arial;
min-width: 47px;
overflow: hidden;
padding: 10px 14px;
text-align: center;
-moz-user-select: none;
-webkit-user-select: none;
-ms-user-select: none;
user-select: none;
}

.b-iframe{
    overflow: hidden; border: none; padding: 0; margin: 0; width:600px; height:280px;
}

.popup_box label.error {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
  padding-left: 16px;
  vertical-align:middle;
  font-size: 11px;
  color: #EA5200;
  margin-left: 2px;
  width: auto;
  display: inline;
}

.popup_box label.checked {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
}

/* ------------------------------------------------------------------------------------------- */
.header_main {
	width: 100%;
	height: 79px; 
}

.header_main .main_left{
	float: left;  
}

.header-logo {
	display: inline-block; vertical-align: middle; 
}

.header-ad {
	display: inline-block;vertical-align: middle;
}

.header_main .main_right{
	display: inline-block; float: right;  padding-top: 20px;
}
/* ------------------------------------------------------------------------------------------- */

.header_cart {
	background: #fff;
	border: solid 1px #e5e5e5;
	font-size: 14px;
	height: 37px;
	line-height: 37px;
	width: 167px;
	z-index: 98;
}

.header_cart a {
	display: block; outline:0; text-decoration:none; color:#333;
}

.header_cart a:hover {
	color:#74b72c;
}

#i_cart:hover em {
border-color: #74b72c #fff;
border-width: 0 5px 6px;
}

.header_cart i {
background: url(${ctx}/static/shop/t01eaa0bd87c6c5b77d.png) 0 0 no-repeat;
display: inline-block;
height: 15px;
margin: 0 6px 0 17px;
width: 20px;
}

.header_cart .cart_size {
background: #74b72c;
border-radius: 10px;
color: #fff;
display: inline-block;
font-size: 12px;
height: 19px;
line-height: 19px;
text-align: center;
width: 19px;
}

.header_cart em {
border-style: solid;
border-width: 6px 5px 0;
border-color: #c5c5c5 #fff;
display: inline-block;
height: 0;
margin-left: 10px;
overflow: hidden;
width: 0;
}

.header_cart_popup {
background: red;
border: #c9c9c9 solid 1px;
display: none;
font-size: 12px;
position: relative;
z-index: 97;
}

.cart_tips {
color: #666;
display: none;
padding: 40px 0;
text-align: center;
}

.cart_info {
display: none;
}

.cart_submenu{
	position: relative;
	top:0px;
	width:300px;
	
	display: none;
	right:102px;
	font-size:10px;
	background-color: #fff;
	border: 1px solid #c1698d;
	z-index: 96;
	padding: 5px;
}

.cart_item_remove{
background: url(${ctx}/static/shop/t01eaa0bd87c6c5b77d.png) 0 -33px no-repeat;
  display: inline-block;
  height: 15px;
  margin: 0 6px 0 17px;
  width: 20px;
}


.cart_item_remove:hover{
background: url(${ctx}/static/shop/t01eaa0bd87c6c5b77d.png) 0 -16px no-repeat;
  display: inline-block;
  height: 15px;
  margin: 0 6px 0 17px;
  width: 20px;
}

.cart_col { height: 22px; line-height: 22px; text-align: left; }

/* ------------------------------------------------------------------------------------------- */
.navi_main {
    width: 100%;
    height: 49px;
    line-height: 49px;
    background: #2f2f2f;
}

.navi_main .navi_menu .navi_menu_heading,
.navi_main .navi_menu ul
 {
    display: inline-block;    
	vertical-align:top;
}

.navi_main .navi_menu .navi_menu_heading {
    width: 245px;
    background: #c40000;
    color: #fff;
	text-align: center;
  	cursor: pointer;
  	float: left;
  	height: 49px;
  	position: relative;  
  	z-index: 95;
}

.catalog_link {
  color: #fff;
  display: block;
  font-size: 16px;
  line-height: 49px;
  text-align: center;
  background: #45a53d;
  text-decoration: none;
  margin-right: 0px;
}

.catalog_link em {
  transform: rotate(180deg);
  -ms-transform: rotate(180deg);
  -moz-transform: rotate(180deg);
  -webkit-transform: rotate(180deg);
  -o-transform: rotate(180deg);
  background: url(${ctx}/static/shop/t01eaa0bd87c6c5b77d.png) no-repeat 0 -50px;
  display: inline-block;
  height: 6px;
  margin-left: 8px;
  width: 12px;
}

.navi_menu ul li{
    display: inline-block;    
    width: 100px;
    text-align: center;
}

.navi_menu ul li a {
    color: #fff;
    font: 15px "Microsoft YaHei", Verdana, SimHei, "Microsoft JhengHei", Tahoma;  
    text-decoration: none;
}

.navi_menu  a i.tip-green {
    background: #68b01e
}

.navi_menu  a i.tip-green:after {
    border-color: transparent #68b01e
}

.navi_menu a i {
  border-radius: 2px;
  color: #fff;
  font-size: 12px;
  font-style: normal;
  line-height: 1;
  padding: 5px;
  position: absolute;
  margin-top: -10px;
  z-index: 94;
}

.navi_menu a i:after {
  border-style: solid;
  border-color: transparent #fd7b1c;
  border-width: 0 0 5px 5px;
  bottom: -5px;
  content: '\20';
  height: 0;
  left: 5px;
  overflow: hidden;
  position: absolute;
}

.navi_menu ul li:hover {
    background-color: #000;
}

.catalog_wrapper{
	width: 100%;
}

.flyout_menu {
	width: 245px;	
	z-index: 93;
	position: absolute;
	border: 1px solid #dae0e4;
    border-top: none; 
    font: 14px "Microsoft YaHei", Verdana, SimHei, "Microsoft JhengHei", Tahoma;  
} 

.flyout_menu .flyout_menu_li {
  	list-style: none;
  	margin: 0;
  	padding: 0; 
  
  	background: rgba(255, 255, 255, .96);
  	border-bottom: solid 1px #dae0e4;
	cursor: pointer;
    height: 70px;
    padding: 14px 0;
    position: relative;
}

.flyout_menu .flyout_menu_sub_ul {
	display: none;
	position: absolute;
	top: 0px; 
	left: 100%;
	z-index: 92;
	background: rgba(255,255,255,.97);
  	border: solid 1px #dae0e4;  
  	font-size: 14px;
}

.flyout_menu .flyout_menu_sub_ul li  {
	background: rgba(255,255,255,.97);  
	border-bottom: solid 1px #dae0e4;
    cursor: pointer;
    padding: 5px 0;
    position: relative;	
  	
  	overflow: hidden;
 	width: 180px; 	
 	height: 45px;
  	line-height: 45px;
}

.flyout_menu .flyout_menu_sub_ul li a  {
	font: 12px "Microsoft YaHei",Verdana,SimHei,"Microsoft JhengHei",Tahoma;
	color: #666;
  	text-decoration: none;
  	float: left;
}

.flyout_menu .flyout_menu_sub_ul .list_img {
  display: inline-block;
  text-align: center;
  vertical-align: middle;
  width: 50px;
}

.flyout_menu ul li .flyout_menu_icon {
  float: left;
  line-height: 60px;
  text-align: center;
  vertical-align: middle;
  width: 35%;
}

.flyout_menu ul li .flyout_menu_title {
  color: #000;
  float: left;
  line-height: 30px;
  cursor: pointer;
  font-size: 16px;
  width: 65%;
}

.flyout_menu .flyout_menu_links {
  color: #333;
  float: left;
  font-size: 14px;
  line-height: 30px;
  width: 60%;
}

.flyout_menu .flyout_menu_links a {
  color: #666;
  font-size: 14px;
  line-height: 30px;
  outline: none;
}

.flyout_menu ul li .flyout_menu_links a:hover {
  text-decoration: underline;
}

.flyout_menu li:hover { 
	background: #eee; 
}

.flyout_menu ul li:hover > ul { 
display: block;
top: 0px;
vertical-align: middle;
}

/* ------------------------------------------------------------------------------------------- */
.flexslider { position: relative; height: auto; padding: 0px; width: 100%;   
margin: 0px; overflow: hidden; background: url(${ctx}/static/flexSlider/images/loading.gif) 50% no-repeat;
border: 0;
    outline: 0;
}
.slides { position: relative; z-index: 91; list-style: none outside none;padding: 0; margin: 0;}
.slides li { display: block; cursor:pointer;text-align: center; height: 500px;}
.flex-control-nav { position: absolute; bottom: 10px; z-index: 90; width: 100%; text-align: center;}
.flex-control-nav li { display: inline-block; width: 14px; height: 14px; margin: 0 5px; *display: inline; zoom: 1;}
.flex-control-nav a { display: inline-block; width: 14px; height: 14px; line-height: 40px; overflow: hidden; background: url(${ctx}/static/flexSlider/images/dot.png) right 0 no-repeat; cursor: pointer;}
.flex-control-nav .flex-active { background-position: 0 0;}
/* ------------------------------------------------------------------------------------------- */

.block-item {
  float: left;
  height: 240px;
  margin: 0 10px 10px 0;
  position: relative;
  width: 240px;
}


div.shadow:hover {
	-moz-box-shadow: 0 0 5px rgba(0,0,0,0.5);
	-webkit-box-shadow: 0 0 5px rgba(0,0,0,0.5);
	box-shadow: 0 0 5px rgba(0,0,0,0.5);
}


img {
vertical-align: bottom;
border: 0;
}


.produ_row_content {
height: 490px;
}


.produ_row_content .float_left {
float: left;
width: 480px;
height: 490px;
background: #fff;
}


.produ_row_content .float_right {
float: right;
width: 740px;
height: 490px;
background: #fff;
padding-left: 5px;
}

.produ_row_content .float_left2 {
float: left;
width: 740px;
height: 490px;
background: #fff;
}

.produ_row_content .float_right2 {
float: right;
background: #fff;
padding-left: 0px;
}


.produ_row_ul_content_1 li {
padding-right: 0px;
padding-bottom: 0px;
padding-left: 0px;

width: 490px;
height: 240px;
float: left;
display: inline;
position: relative;
}


.produ_row_ul_content_2 li {
padding-right: 0px;
padding-bottom: 0px;
padding-left: 0px;

width: 240px;
height: 240px;
float: right;
display: inline;
position: relative;
}

.produ_row_ul_content_3 li {
padding-top: 10px;
padding-right: 0px;
padding-bottom: 0px;
padding-left: 0px;

width: 240px;
height: 240px;
float: left;
display: inline;
position: relative;
}


.produ_row_ul_content_4 li {
padding-top: 10px;
padding-right: 0px;
padding-bottom: 0px;
padding-left: 0px;

width: 490px;
height: 240px;
float: right;
display: inline;
position: relative;
}

.produ_row_content .produ_row_ul_content_0:hover img, 
.produ_row_content .produ_row_ul_content_1:hover img,
.produ_row_content .produ_row_ul_content_2:hover img,
.produ_row_content .produ_row_ul_content_3:hover img,
.produ_row_content .produ_row_ul_content_4:hover img {
opacity: .9;
-webkit-transform: translate(-2px,0);
-moz-transform: translate(-2px,0);
transform: translate(-2px,0);
}

.linedivider {
  background: url(../shop/linedividerback.png) repeat-x 0 0.5em;
  color: #889911;
  margin-bottom: 0.5em;
  font-size: 1.75em;
  line-height: 1.2;
position: relative;
padding-left: 70px;
width: 250px;
}

.linedivider span {
  background: #fff;
  padding: 0 20px;
}


.mod-titles {
  border-left: solid 5px #333;
  color: #000;
  font-size: 20px;
  font-weight: 400;
  font-family: arial,黑体;
  height: 40px;
  line-height: 24px;
  margin: 30px 0 15px;
  overflow: hidden;
  padding-left: 15px;
}

.mod-titles i {
    display: block;
    font-style: normal;
    font-size: 14px;
    line-height: 16px;
}

.col a img {
  max-width: 100% !important;
  vertical-align: middle;
  border: 0;
  -ms-interpolation-mode: bicubic;
}

.col a img:hover {
  opacity: 0.8;
}

/* ------------------------------------------------------------------------------------------- */
.footer_top {
	margin-top: 20px; background-color: #333;
	border-bottom: solid 1px #444;
}

.footer_banner_item {
text-align: center;
font: 12px "Microsoft YaHei",Verdana,SimHei,"Microsoft JhengHei",Tahoma;
color: #fff;
font-size: 18px;
padding: 25px 0;
}

.footer_banner_item i.icon1 {
background-position: 0 0;
}

.footer_banner_item i.icon2 {
background-position: 0 -100px;
}

.footer_banner_item i.icon3 {
background-position: 0 -200px;
}

.footer_banner_item i {
background: url(../shop/t01283524b687dc6e26.png) no-repeat 0 0;
display: inline-block;
height: 35px;
margin-right: 10px;
vertical-align: middle;
width: 35px;
}


.footer_middle {
	margin-top: 0px; background-color: #1f1f1f;
}


.footer_middle .header {
	color: #c2c2c2;
	line-height: 40px;
	font: 18px "Microsoft YaHei",Verdana,SimHei,"Microsoft JhengHei",Tahoma;
}


.footer_middle ul {
	list-style: none;
	margin-top: 5px;
}

.footer_middle ul li{
	line-height: 30px;
	
}


.footer_middle ul li a {
	color: #666;
text-decoration: none;
outline: none;
font: 12px "Microsoft YaHei",Verdana,SimHei,"Microsoft JhengHei",Tahoma;
}

.footer_middle ul li a:hover {
color: #77be24;
text-decoration: underline;
}


.footer_middle .call-number {
color: #c2c2c2;
font-size: 26px;
line-height: 40px;
}

.footer_middle .call-time {
font-size: 16px;
color: #666;
font: 12px "Microsoft YaHei",Verdana,SimHei,"Microsoft JhengHei",Tahoma;
}


.footer_copyright {
background: #111;
clear: both;
color: #636363;
font-size: 12px;
height: 80px;
line-height: 80px;
text-align: center;
}
/* ------------------------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------------------------- */


</style>


</head>
<body>
<c:if test="${not empty loginNameErrorMessage || not empty passwordErrorMessage}">			
<script type="text/javascript">		
$(document).ready(function(){	
	$('#loginbox').bPopup({
		autoClose: false,
		modal: false,	 	
	 	modalClose: false, 
	 	opacity: 0.6,
	    speed: 500,
	    transition: 'slideBack'        
	});
});
</script>
</c:if>

<c:choose>
  <c:when test="${not empty httpSession}">		  	
  	<input type="hidden" id="authenticated" value="true" />
  </c:when>
  <c:otherwise>
  	<input type="hidden" id="authenticated" value="false" />
  </c:otherwise> 
</c:choose>

<div class="header_bar">
<div class="col_1_of_1">
	<div class="bar_left">
		<a href="http://mall.360.com/#" onclick="return false">收藏360商城</a>
	</div>
	<div class="bar_right">
	<div class="header_bar_nav">
		<ul>
		    <c:choose>
			  <c:when test="${not empty httpSession}">		  	
			  	<li class="i_dropdown" id="i_dropdown">
		            <a href="#">${httpSession.customer.username}<em></em></a>
		            <ul class="i_sub_menu" id="i_sub_menu">
		                <li><a href="#">个人中心</a></li>
		                <li><a href="#">我的试用</a></li>
		                <li><a href="#">试用积分</a></li>                
		                <li><a href="#">收货地址</a></li>
		                <li><a href="#">账户设置</a></li>
		                <li><a href="javascript:;" onclick="logout()">退出登录</a></li>
		            </ul>
		    	</li>
			  </c:when>
			  <c:otherwise>
			  	<li><a href="javascript:;" onclick="loginShow()">登录</a></li>
				<li><a href="javascript:;" onclick="registerShow()">注册</a></li>
			  </c:otherwise> 
			</c:choose>
		    <li><a href="javascript:;">我的订单</a></li>
			<li><a href="javascript:;">手机360商城</a></li>
			<li><a href="javascript:;">企业采购</a></li>
			<li><a href="javascript:;">帮助中心</a></li>        
		</ul>
	</div>
	</div>
</div>
</div>
<!-- ******************************************************************************************** -->
<div class="header_main">
	<div class="col_1_of_1">
		<div class="main_left">
			<a class="header-logo" href="/shop/index"><img alt="logo" src="/shop/static/images/logo.png" /></a>
			<a class="header-ad" href="/shop/index"><img alt="ad" src="/shop/static/images/ad.gif" /></a>			 
		</div>
		<div class="main_right">
			<div class="header_cart" id="i_cart">			
				<a href="${ctx}/mycart" ><i></i>我的购物车<span class="cart_size">0</span><em></em></a> 
				<div class="cart_submenu" id="i_cart_popup">
				<c:choose>
				  <c:when test="${not empty httpSession}">	
				  	<c:choose>
					  <c:when test="${httpSession.shoppingCart.getItemCount() > -1}">
					    <div class="ungrid_row">
						    <div class="ungrid_col cart_col" style="width:55px"><img alt="" src="${ctx}/static/shop/t0107616523bf23adbe.png" /></div>
						    <div class="ungrid_col cart_col">360超级充电器</div>
						    <div class="ungrid_col cart_col" style="width:50px">299.00</div>
						    <div class="ungrid_col cart_col" style="width:40px"><a href="#"><span class="cart_item_remove"></span></a></div>
						</div>
						<div class="ungrid_row">
						    <div class="ungrid_col cart_col" style="width:55px"><img alt="" src="${ctx}/static/shop/t0107616523bf23adbe.png" /></div>
						    <div class="ungrid_col cart_col">360超级充电器</div>
						    <div class="ungrid_col cart_col" style="width:50px">299.00</div>
						    <div class="ungrid_col cart_col" style="width:40px"><a href="#"><span class="cart_item_remove"></span></a></div>
						</div>
						<div class="ungrid_row" style="height: 42px; background-color: #F6F9FC;padding-top: 5px;">
					        <div class="ungrid_col cart_col">共2件商品</div>
					        <div class="ungrid_col cart_col">总计:￥798.00</div>		
					        <div class="ungrid_col cart_col" style="text-align: right;"><a href="${ctx}/i/mycart/viewCart" target="_blank" class="u-btn">去购物车</a></div>			        		        		        
					    </div>					    
					  </c:when>
					  <c:otherwise>
					       	你的购物车还没有商品，赶紧去选购吧!
					  </c:otherwise> 
					</c:choose>												        	
				  </c:when>
				  <c:otherwise>				  	
				  		请登录后查看你的购物车。				  		  	
				  </c:otherwise> 
				</c:choose>
				</div>					
			</div>			
		</div>
	</div>
</div>
<!-- ******************************************************************************************** -->
<div class="navi_main">	
<div class="col_1_of_1">
<div class="navi_menu">
    <div class="navi_menu_heading">
    	<a class="catalog_link" href="http://mall.360.com/allproduct" target="_blank">全部分类导航<em></em></a>
    </div>
    <ul>
	    <li><a href="${ctx}/index">首页</a></li>
	    <li><a href="javascript:;">路由器</a></li>
	    <li><a href="javascript:;">儿童卫士3</a></li>
	    <li><a href="javascript:;">行车记录仪</a></li>
	    <li><a href="javascript:;">超级充电器<i class="tip-green">New</i></a></li>    
    </ul>
</div>
</div>
</div>
<!-- ******************************************************************************************** -->

<!-- ******************************************************************************************** -->
<h1>checkout</h1>
<!-- ******************************************************************************************** -->

<!-- ******************************************************************************************** -->
<div class="footer_top" >
	<div class="col_1_of_1">
		<div class="section group">
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item" style="border-right: solid 1px #444;"><i class="icon1"></i>7天无理由退货</div>
		    </div>
		
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item" style="border-right: solid 1px #444;"><i class="icon2"></i>15天免费换货</div>		        
		    </div>
		
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item"><i class="icon3"></i>满99元包邮<span style="font-size:12px">(特殊商品除外)</span></div>
		    </div>		    
		</div>
	</div>
</div>

<div class="footer_middle">
	<div class="col_1_of_1">
		<div class="section group">
		    <div class="col span_2_of_10">		        
				<span class="header">新手入门</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">新用户注册</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">用户登录</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">找回密码</a></li>
				</ul>
		    </div>
		
		    <div class="col span_2_of_10">
		        <span class="header">购物指南</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">购买流程</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">支付方式</a></li>
					<li><a href="#" rel="nofollow">配送说明</a></li>					
				</ul>		        
		    </div>
		
		    <div class="col span_2_of_10">
		        <span class="header">售后服务</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">服务条款</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">七日退货</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">十五日换货</a></li>
				</ul>
		    </div>	
		    		    
		    <div class="col span_2_of_10">
		        <span class="header">免费试用</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">试用流程</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">查看申请结果</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">提交报告</a></li>
				</ul>
		    </div>
		    
		    <div class="col span_2_of_10">
		        <span class="header">联系我们</span>
				<ul> 
					<li class="call-number">400-6822-360 </li>
					<li class="call-time">周一到周日9:00-18:00</li>
				</ul>
		    </div>	    
		</div>
	</div>
</div>
<div class="footer_copyright">360商城©2013-2015 深圳奇虎健安智能科技有限公司版权所有｜粤ICP备14094849号-1</div>		
<!-- ******************************************************************************************** -->
<a href="javascript:;" class="cd-top">Top</a>

<!-- ******************************************************************************************** -->
<div id="loginbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">用户登录</span>        
        <a href="javascript:loginClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">  
    	<form id="loginForm">    
        <div class="popup_box_content">               	 
    		 <div style="width: 420px;">
	    		 <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="loginName">用户名:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="loginName" name="loginName" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_loginName_error_div"></div>			        			        
			    </div>			    
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="password">密码:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="password" name="password" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_password_error_div"></div>		        		        
			    </div>
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col">
			        	<div id="login_loginName_msg_div" style="color: red; display: inline;">${loginNameErrorMessage}</div>
			        	<div id="login_password_msg_div" style="color: red; display: inline;">${passwordErrorMessage}</div>							
			        </div>
			        		        		        
			    </div>
			    
    		</div>        	 
        </div>        
        <div class="popup_box_footer">					
			<input type="button" class="popup_box_btn popup_box_btn_gray" value="登 录" onclick="loginPost()" />							
		</div>
		</form>		        
    </div>
</div>
<!-- ******************************************************************************************** -->
<script type="text/javascript" src="${ctx}/static/jquery/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="${ctx}/static/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="${ctx}/static/flexSlider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="${ctx}/static/backtotop/js/backtotop.js"></script>

<script type="text/javascript">
//你好！请登录|注册
//我的订单
//我的消息
var numberItem = 0;

$(document).ready(function(){	
	$("#flexslider_wrapper").flexslider({
		animation: "slide",
		directionNav: true
	});
	
	/* $("#slides_wrapper li").click(function(event) {		 
		event.preventDefault();
		var id = $(this).attr("id").split("-")[1];
		window.location.href = "${ctx}/item?itemId="+id; 
		//alert(id);
	}); */
	
	$('#i_dropdown').hover(
    	function(){
    		$(this).children('#i_sub_menu').slideDown(200);
		},
        function(){
        	$(this).children('#i_sub_menu').slideUp(200);
		}
	);
	
	$('#i_cart').hover(
		function(){			
			$(this).children('#i_cart_popup').slideDown(200);
		},
	    function(){			
			$(this).children('#i_cart_popup').slideUp(200);
		}
	);
	
	$("#loginForm").validate({		 
		rules: {			
			loginName: {
				minlength: 2,
				maxlength: 14,
				required: true				
			},
			password: {
				minlength: 6,
				maxlength: 20,
				required: true
			}
	  	},
	  	messages: {	  		
	  		loginName: {				
	  			minlength: "登录名称至少是1个字符",
	  			maxlength: "登录名称最多为14个字符",
	  			required: "请输入登录名称",
	  			remote: "登录名称已经存在"
			},
			password: {				
	  			minlength: "密码至少是6个字符",
	  			maxlength: "密码最多为20个字符",
	  			required: "请输入密码"
			}    
	  	},	
		errorPlacement: function(error, element) {					
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});	
	
	/* $('#myli').hover(
			function(){			
				$(this).children('#mysub').slideDown(200);
			}
	); */
	
	var top = $('#mysub3').position().top;
	alert(top);
	
	
});



//正在加载购物车...
//我的购物车
//您的购物车还是空的，赶紧行动吧！马上去购物
//请登录后查看你的购物车
//你的购物车还没有商品，赶紧去选购吧!

//商品信息	单价	数量	小计	操作
//商品总计
//继续购物	去结算





function addToMyCart(productItem){	
	//alert(productItem);
	//todo ajax 将购物车写入数据库，返回是否成功失败bool
	var pid = productItem.attr("pid");
	if(pid == null || pid == "undefined"){
		return false;
    }else{
    	var pcategory = productItem.attr("pcategory");
    	alert(pcategory);
    	
    	return true;
    	
    	
    }
}


function addToCart(i,qty){
	
}

function addCartItemDisplay(objProd,Quantity){
	
}


function loginShow(){
	$('#loginbox').bPopup({
	 	modal: false,
	 	opacity: 0.6,
	 	modalClose: false, 
        speed: 500,
        transition: 'slideDown'        
 	});
}

function loginClose(){		
	$("#login_loginName_msg_div").empty();
	$("#login_password_msg_div").empty(); 
	$("#login_loginName_error_div").empty();
	$("#login_password_error_div").empty();	
	
	closeEventFlag = true;
	$("#loginbox").bPopup().close(); 	
}

function loginPost(){		
	var form = $("#loginForm");
	form.attr("action","${ctx}/index/login");
	form.attr("method","post");
	form.submit();
            	
	return false;
}

function logout(){
	var form = $("#loginForm");
	form.attr("action","${ctx}/index/logout");
	form.attr("method","post");
	form.submit();
    return false; 
}

function registerShow(){	
	$("#registerbox").bPopup({
		modal: false,
		modalClose: false, 
	    speed: 450,
	    transition: 'slideDown',
		content: "iframe", 
		contentContainer: "#registerContainer",
		loadUrl:"${ctx}/i/login/create"		
	});
}


function registerClose(){		
	closeEventFlag = true;
	$("#registerContainer").empty();
	$("#registerbox").bPopup().close(); 	
}


</script>

</body>
</html>