<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>360商城</title>
<link type="image/x-icon" rel="shortcut icon" href="${ctx}/static/images/favicon.ico">
<link rel="stylesheet" href="${ctx}/static/grid/col.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/2cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/3cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/4cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/5cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/6cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/7cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/8cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/9cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/10cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/11cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/12cols.css" media="all">

<link type="text/css" rel="stylesheet" href="${ctx}/static/flexSlider/flexslider.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/backtotop/css/style.css">

<link type="text/css" rel="stylesheet" href="${ctx}/static/css/bpopup.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/shop_index.css">

<style type="text/css">

/* ------------------------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------------------------- */


</style>


</head>
<body>

<c:choose>
  <c:when test="${not empty sessionScope.SHOP_HTTP_SESSION}">		  	
  	<input type="hidden" id="authenticated" value="true" />
  </c:when>
  <c:otherwise>
  	<input type="hidden" id="authenticated" value="false" />
  </c:otherwise> 
</c:choose>

<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/common/header.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<div class="header_main">
	<div class="col_1_of_1">
		<div class="main_left">
			<jsp:include page="/WEB-INF/views/shop/common/title.jsp"></jsp:include>
		</div>
		<div class="main_right">		
			<jsp:include page="/WEB-INF/views/shop/common/cart.jsp"></jsp:include>	
		</div>
	</div>
</div>
<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/common/navigation.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/home/catalog.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/home/slider.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/home/product.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/common/footer.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<a href="javascript:;" class="cd-top">Top</a>
<!-- ******************************************************************************************** -->
<jsp:include page="/WEB-INF/views/shop/common/login.jsp"></jsp:include>
<!-- ******************************************************************************************** -->
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-validation-1.14.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="${ctx}/static/flexSlider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="${ctx}/static/backtotop/js/backtotop.js"></script>

<script type="text/javascript">
//你好！请登录|注册
//我的订单
//我的消息
var numberItem = 0;

$(document).ready(function(){	
	$("#flexslider_wrapper").flexslider({
		animation: "slide",
		directionNav: true
	});
	
	$('#i_dropdown').hover(
    	function(){
    		$(this).children('#i_sub_menu').slideDown(200);
		},
        function(){
        	$(this).children('#i_sub_menu').slideUp(200);
		}
	);
	
	$('#i_cart').hover(
		function(){			
			$(this).children('#i_cart_popup').slideDown(200);
		},
	    function(){			
			$(this).children('#i_cart_popup').slideUp(200);
		}
	);
	
	$("#loginForm").validate({		 
		rules: {			
			
			password: {
				minlength: 6,
				maxlength: 20,
				required: true
			}
	  	},
	  	messages: {	  		
	  		
			password: {				
	  			minlength: "密码至少是6个字符",
	  			maxlength: "密码最多为20个字符",
	  			required: "请输入密码"
			}    
	  	},	
		errorPlacement: function(error, element) {					
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});	
	
	/* $('#myli').hover(
			function(){			
				$(this).children('#mysub').slideDown(200);
			}
	); */
	
	var top = $('#mysub3').position().top;
	alert(top);
	
	
});



//正在加载购物车...
//我的购物车
//您的购物车还是空的，赶紧行动吧！马上去购物
//请登录后查看你的购物车
//你的购物车还没有商品，赶紧去选购吧!

//商品信息	单价	数量	小计	操作
//商品总计
//继续购物	去结算





function addToMyCart(productItem){	
	//alert(productItem);
	//todo ajax 将购物车写入数据库，返回是否成功失败bool
	var pid = productItem.attr("pid");
	if(pid == null || pid == "undefined"){
		return false;
    }else{
    	var pcategory = productItem.attr("pcategory");
    	alert(pcategory);
    	
    	return true;
    	
    	
    }
}


function addToCart(i,qty){
	
}

function addCartItemDisplay(objProd,Quantity){
	
}


function loginShow(){
	$('#loginbox').bPopup({
	 	modal: false,
	 	opacity: 0.6,
	 	modalClose: false, 
        speed: 500,
        transition: 'slideDown'        
 	});
}

function loginClose(){		
	$("#login_loginName_msg_div").empty();
	$("#login_password_msg_div").empty(); 
	$("#login_loginName_error_div").empty();
	$("#login_password_error_div").empty();	
	
	closeEventFlag = true;
	$("#loginbox").bPopup().close(); 	
}

function loginPost(){		
	var form = $("#loginForm");
	form.attr("action","${ctx}/index/login");
	form.attr("method","post");
	form.submit();
            	
	return false;
}

function logout(){
	var form = $("#loginForm");
	form.attr("action","${ctx}/index/logout");
	form.attr("method","post");
	form.submit();
    return false; 
}

function registerShow(){	
	$("#registerbox").bPopup({
		modal: false,
		modalClose: false, 
	    speed: 450,
	    transition: 'slideDown',
		content: "iframe", 
		contentContainer: "#registerContainer",
		loadUrl:"${ctx}/customer/register"		
	});
}


function registerClose(){		
	closeEventFlag = true;
	$("#registerContainer").empty();
	$("#registerbox").bPopup().close(); 	
}


</script>

<c:if test="${not empty loginNameErrorMessage || not empty passwordErrorMessage}">			
<script type="text/javascript">		
$('#loginbox').bPopup({
	autoClose: false,
	modal: false,	 	
 	modalClose: false, 
 	opacity: 0.6,
    speed: 500,
    transition: 'slideBack'        
});
</script>
</c:if>
</body>
</html>