<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>新用户注册</title>
<style type="text/css">
html {height: 100%;}
body {
margin: 0;
padding: 0;
border:0;    
width:100%; 
height: 100%;
overflow:hidden; 
display: block;
min-height: 100%; 
background: #FFF url('${ctx}/static/images/corner.png') no-repeat right bottom;
font-size: 14px;
}


.table{ padding:0; margin:0; border-collapse:collapse; border:solid 1px #C1DAD7; }
.table td{ width:100px; height:45px;  text-align: right; padding-right: 5px; border-bottom:1px solid #C1DAD7;}
.table tbody tr.odd { background:#F7F9FC;}
.table tbody tr.even { background:#FFFFFF;}

input[type=text], input[type="password"]{
font-size:13px;     
	height: 25px;	
	width:180px;
	padding: 3px;
	margin: 0px;  		  
    outline:none;
width:180px;border:1px solid #D9D9D9;border-top-color:#c0c0c0;line-height:17px;font-size:14px;color:#777;}

.tip {
    color: #666;
    vertical-align: middle;
    font-size: 13px;
}

#form .status {  
  padding-left: 8px;
  vertical-align: middle;
  width: 246px;
  white-space: nowrap;
  text-align: left;  
}

#form label.error {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
  padding-left: 16px;
  vertical-align:middle;
  font-size: 13px;
  color: #EA5200;
  margin-left: 2px;
	width: auto;
	display: inline;
}

#form label.checked {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
}

.errors {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
  padding-left: 16px;
  vertical-align:middle;
  font-size: 13px;
  color: #EA5200;
}


.u-btn{display:inline-block;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;padding:0 12px;height:28px;line-height:28px;border:1px solid #2d88bf;font-size:12px;letter-spacing:1px;word-spacing:normal;text-align:center;vertical-align:middle;cursor:pointer;background:#54aede;}
.u-btn,.u-btn:hover{color:#fff;text-decoration:none;}
.u-btn:hover,.u-btn:focus{background:#399dd8;}
.u-btn::-moz-focus-inner{padding:0;margin:0;border:0;}

</style>
<script type="text/javascript" src="${ctx}/static/jquery/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="${ctx}/static/validation/jquery.validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){		
	var url = "${ctx}/customer/checkTelephone";	
	
	$("#form").validate({		 
		rules: {			
			telephone: {
				minlength: 2,
				maxlength: 14,
				required: true,
				remote: url
			},
			password: {
				minlength: 6,
				maxlength: 20,
				required: true
			}
	  	},
	  	messages: {	  		
	  		telephone: {				
	  			minlength: "登录名称至少是2个字符",
	  			maxlength: "登录名称最多为14个字符",
	  			required: "请输入登录名称",
	  			remote: "登录名称已经存在"
			},
			password: {				
	  			minlength: "密码至少是6个字符",
	  			maxlength: "密码最多为20个字符",
	  			required: "请输入密码"
			}    
	  	},	
		errorPlacement: function(error, element) {
			$(".tip").hide();			
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});	
	
}); 
</script>
</head>
<body>		
<form:form id="form" modelAttribute="login" method="post" action="${ctx}/customer/register">
    
	<!-- <h4>新用户注册</h4> -->
	<table class="table">        
       
    <tr class="even">   
      <td style="text-align: right;width: 130px;"><form:label path="telephone">手机号码:</form:label></td>
      <td style="width: 160px;"><form:input path="telephone" style="float:left;" /></td>
      <td class="status"><form:errors path="telephone" cssClass="errors" element="div"/></td>     
    </tr>
    
	<tr class="odd">  
		<td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><form:label path="password">密码:</form:label></span></td>
	    <td style="width: 160px;"><form:password path="password" style="float:left;" /></td>
	    <td class="status"><span class="tip">6-20个字符,区分大小写</span><form:errors path="password" cssClass="errors" element="div"/></td>
	</tr>	   
                    
	<tr class="odd">      
      <td colspan="3" style="text-align:center;">      
      	<input class="u-btn" type="submit" value="注册" />            
      </td>      
    </tr>
  	</table>	 		
</form:form>
</body>
</html>