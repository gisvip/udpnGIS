<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>注册成功</title>
<link type="image/x-icon" rel="shortcut icon" href="${ctx}/static/images/favicon.ico" />
</head>
<body>
<h1>恭喜你，注册成功！</h1>
</body>
</html>