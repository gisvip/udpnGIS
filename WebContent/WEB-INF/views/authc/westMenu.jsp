<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>首页</title>
<link href="${ctx}/static/nec/reset.css" type="text/css" rel="stylesheet" />

<style type="text/css">
html,body {
	height: 100%;
	margin:0px;
    padding:0px;
    border: none;   
    left:0px;
    top:0px;
	overflow:hidden;
}

.menu_simple ul {
    margin: 0; 
    padding: 0;
    width:190px;
    list-style-type: none;
}

.menu_simple ul li {
    border-bottom: solid 1px #ebebeb;
} 

.menu_simple ul li a {
    text-decoration: none;
    color: white; 
    padding: 11px 11px;
    text-align:center;
    background-color: #0f8bc0;
    display:block;
}
 
.menu_simple ul li a:hover, .menu_simple ul li a.current {
    color: white;
    background-color: #38c0c8;
}


/* .menu {
  clear: both;
  float: left;
  width: 100%;
  height: 50px;
  background: url(../images/title.jpg) no-repeat left top #2980b9;
}

.menu .logoa {
  display: block;
  float: left;
  width: 145px;
  height: 50px;
}
a {
  font: 12px/18px SimSun;
  color: #0f4784;
  text-decoration: none;
}

.menu ul {
  display: block;
  width: 815px;
  height: 50px;
}

.menu ul li {
  display: block;
  float: left;
  width: 112px;
  height: 50px;
  text-align: center;
}

.menu ul li a {
  display: block;
  padding: 0 15px;
  font: 18px/50px "Microsoft Yahei";
  color: #FFF;
}

.menu ul li .ahover {
  background-color: #3498db;
  text-decoration: none;
}
 */

</style>
    
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
</head>
<body>

<!-- <div class="menu">
  <a class="logoa" href="http://home.ebrun.com"></a>
    <ul>
          <li><a href="http://home.ebrun.com/u-2.html" title="Ta的评论">Ta的评论</a></li>
          <li><a href="topic-2-list.html" title="表态">表态</a></li>      <li><a href="blog-2-list.html" title="专栏" class="ahover">专栏</a></li>
    </ul>
  </div> -->

<div class="menu_simple" >
<ul id="menu">
<c:forEach items="${items}" var="item">				    	
   	<li><a href="${ctx}/${item.url}" target="centerFrame">${item.name}</a></li>				    	
</c:forEach>
</ul>
</div>


<script type="text/javascript">
$(document).ready( function() {		
	$("#menu li a").click(function() {		
		$("#menu li a").removeClass("current");			
		$(this).addClass("current");
	});
	
});
</script>
</body>
</html>
