<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery-ui-1.11.4/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery-ui-1.11.4/jquery-ui.structure.min.css">
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery-ui-1.11.4/jquery-ui.theme.min.css">

<script type="text/javascript" src="${ctx}/static/jquery/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-ui-1.11.4/jquery-ui.min.js"></script>

<link type="text/css" rel="stylesheet" href="${ctx}/static/jtable.2.4.0/themes/metro/lightgray/jtable.min.css" />
<script type="text/javascript" src="${ctx}/static/jtable.2.4.0/jquery.jtable.min.js" ></script>
<script type="text/javascript" src="${ctx}/static/jtable.2.4.0/localization/jquery.jtable.zh-CN.js"></script>
<style type="text/css">
div.filtering {
    border: 1px solid #BBB;
    margin-bottom: 5px;
    padding: 10px;
    background-color: #EEE;
}
</style>
<title>index</title>
</head>
<body>
<div class="filtering">
    <form>
        姓名: <input type="text" name="stdntName" id="stdntName" />
        邮箱: <input type="text" name="email" id="email" />
        
        <button type="submit" id="LoadRecordsButton" style="line-height: 23px; width: 60px;">查询</button>
    </form>
</div>


<div id="studentTableContainer"></div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#studentTableContainer').jtable({
            title: '学生列表',
            paging: true, //Enable paging
            pageSize: 10, //Set page size (default: 10)
            sorting: true, //Enable sorting
            defaultSorting: 'stdntName ASC', //Set default sorting
            actions: {
                listAction: '${ctx}/authc/student/studentList',
                createAction: '/GettingStarted/CreatePerson',
                updateAction: '/GettingStarted/UpdatePerson',
                deleteAction: '/GettingStarted/DeletePerson'
            },
            fields: {
            	id: {
            		key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                stdntName: {
                    title: '学生姓名',
                    width: '30%'
                },
                email: {
                    title: '邮箱地址',
                    width: '30%'
                }
            }
        });
        
      	//Re-load records when user click 'load records' button.
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#studentTableContainer').jtable('load', {
            	stdntName: $('#stdntName').val(),
            	email: $('#email').val()
            });
        });
 
        //Load all records when page is first shown
        //$('#LoadRecordsButton').click();
        
      	//Load student list from server
        $('#studentTableContainer').jtable('load');
    });
    
  	
</script>

</body>
</html>