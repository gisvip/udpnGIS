<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>

</head>
<body>
<h1>Spring Multiple File Upload example</h1>
<form id="importExcelForm" method="post" action="${ctx}/authc/student/import" enctype="multipart/form-data">
	<input type="file" name="file"/>	
	<input type="submit"/>
</form>
</body>
</html>