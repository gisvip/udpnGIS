<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>学生编辑</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-validation-1.14.0/jquery.validate.min.js"></script>
<script type="text/javascript">
function forback(){	
	window.location.replace("${ctx}/authc/student/list");
	e.preventDefault();
	return false;
}

$(document).ready(function(){
	$("#form").validate({		 
		rules: {			
			stdntName: "required",
			email: {
				required: true,
				email: true
			}
	  	},
	  	messages: {	  		
	  		stdntName: "请输入姓名",
	  		email: "请输入一个有效的邮箱地址"    
	  	},	
		errorPlacement: function(error, element) {		
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});		
}); 
</script>
</head>
<body>		
<form:form id="form" modelAttribute="student" method="post" action="${ctx}/authc/student/${action}">
    <form:hidden path="id" />
	<table class="table">        
    <tr class="odd">    
      	<td colspan="2"></td>
       	<td style="text-align: right;">*必填字段</td>
    </tr>   
    
    <tr class="even">   
      	<td style="text-align: right;width: 130px;"><form:label path="stdntName">姓名:</form:label></td>
      	<td style="width: 200px;"><form:input path="stdntName"  style="float:left;" />*</td>
      	<td class="status"><form:errors path="stdntName" cssClass="errors" element="div"/></td>     
    </tr>
    <tr class="odd">  
      	<td style="text-align: right;width: 130px;"><form:label path="email">邮箱:</form:label></td>
      	<td style="width: 200px;"><form:input path="email" name="email" style="float:left;"  /></td>
      	<td class="status"><form:errors path="email" cssClass="errors" element="div"/></td>
    </tr>
    <tr class="even">      
      	<td colspan="3" style="text-align:center;">      
      	<input class="btn" type="submit" value="提 交" /> 
      	<input class="btn" type="button" value="返回" onclick="forback();" />       
      </td>      
    </tr>       
  	</table>	 		
</form:form>
</body>
</html>