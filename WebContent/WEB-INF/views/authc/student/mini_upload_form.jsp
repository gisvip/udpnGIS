<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>mini_upload</title>

<!-- The main CSS file -->
<link href="${ctx}/static/mini_upload/assets/css/style.css" rel="stylesheet" />
</head>
<body>

<form id="upload" method="post" action="${ctx}/authc/student/miniupload" enctype="multipart/form-data">
	<div id="drop">
		Drop Here

		<a>Browse</a>
		<input type="file" name="files[]" multiple>
		<!-- <input type="file" name="upl" multiple /> -->
	</div>

	<ul>
		<!-- The file uploads will be shown here -->
	</ul>

</form>

<footer>
    <h2><a href="http://tutorialzine.com/2013/05/mini-ajax-file-upload-form/"><i>Tutorial:</i> Mini Ajax File Upload Form</a></h2>
    <div id="tzine-actions">
    	
        <a id="tzine-download" href="http://tutorialzine.com/2013/05/mini-ajax-file-upload-form/" title="Download This Example!">Download</a>
    </div>
</footer>

<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script src="${ctx}/static/mini_upload/assets/js/jquery.knob.js"></script>

<!-- jQuery File Upload Dependencies -->
<script src="${ctx}/static/mini_upload/assets/js/jquery.ui.widget.js"></script>
<script src="${ctx}/static/mini_upload/assets/js/jquery.iframe-transport.js"></script>
<script src="${ctx}/static/mini_upload/assets/js/jquery.fileupload.js"></script>

<!-- Our main JS file -->
<script src="${ctx}/static/mini_upload/assets/js/script.js"></script>

</body>
</html>