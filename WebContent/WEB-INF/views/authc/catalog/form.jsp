<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>form</title>

<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/bpopup.css" />

<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.min.js"></script>




<script type="text/javascript">
var closeEventFlag = false;

function forback(){	
	window.location.replace("${ctx}/authc/catalog/list");
	e.preventDefault();
	return false;
}




$(document).ready( function() {		
	closeEventFlag = false;
	
	$('#popupButton').bind("click", function(e) {
        e.preventDefault();
        $("#ui_box").bPopup({
        	modal: true,
        	modalClose: false, 
            speed: 450,
            transition: 'slideDown',
        	content: "iframe", 
        	contentContainer: "#ui_box_content",
        	loadUrl:"${ctx}/authc/catalog/tree"
        	
        });

    });	
});



function popupClose(){		
	closeEventFlag = true;
	var childFrame = $(".b-iframe").contents();
	
	var pId = childFrame.find("#id").val();        		
	$("#parentId").val(pId);	
	var name = childFrame.find("#name").val();        		
	$("#parentName").val(name);          		
	
	$("#ui_box_content").empty();
	$("#ui_box").bPopup().close(); 	
}
</script>
</head>
<body>

<div id="ui_box" class="popup_box">
    <div class="popup_box_head">
        <span class="popup_box_head_title">选择目录</span>        
        <a href="javascript:popupClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">        
        <div class="popup_box_content" id="ui_box_content">
    		        	 
        </div>               
    </div>
</div>




<form:form id="form" modelAttribute="catalog" method="post" action="${ctx}/authc/catalog/${action}">
	<form:hidden path="id" />	
	<table class="table">    
    <tr class="odd" >    
      <td colspan="2" style=" border: none;">
      	<div id="messageDiv">
		<c:if test="${not empty message}">
			<script type="text/javascript">			
			msgBoxImagePath = "${ctx}/static/msgBox/images/";			
			$.msgBox({
			    title:"信息提示：",
			    content:"${message}",
			    autoClose:true,			    					    
			    type:"info"
			});
		  	</script>	
		</c:if>
		<spring:hasBindErrors name="catalog">
			<div id="messageErrorDiv" class="error">
				<spring:bind path="catalog.*">
					<c:forEach items="${status.errorMessages}" var="error">
						<c:out value="${error}"/><br/>
					</c:forEach>
				</spring:bind>
			</div>
		</spring:hasBindErrors>
		</div>
      </td>
       <td style="text-align: right; border: none;">*必填字段</td>
    </tr>    
    <tr class="even">   
      <td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><form:label path="name">部门名称:<strong>*</strong></form:label></span></td>
      <td style="width: 160px;"><form:input path="name" name="name" style="float:left;" /></td>
      <td class="status"><form:errors path="name" cssClass="errors" element="div"/></td>     
    </tr>
    <tr class="odd">  
      <td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><form:label path="parentId">上级部门:</form:label></span></td>
      <td style="width: 160px;">
      
      <form:input id="parentName" name="parentName" path="parentName" />
      <form:hidden id="parentId" name="parentId" path="parentId" />
      </td>
      <td class="status"><input id="popupButton" class="btn" type="button" value="选择" /><form:errors path="parentId" cssClass="errors" element="div"/></td>
    </tr>
    <tr class="even">      
      <td colspan="3" style="text-align:center;">      
      <input class="btn" type="submit" value="提 交" />  
      <input class="btn" type="button" value="返回" onclick="forback();" />      
      </td>      
    </tr>   
    <tr class="odd" style="height: 50px;">    
      <td colspan="3">
      	      
      </td>      
    </tr>
    <tr class="even">      
            
       <td colspan="3">
      	
      </td>
    </tr> 
  	</table>	
  	  
</form:form>

</body>
</html>