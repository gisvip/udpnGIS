<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>catalog</title>


<link type="text/css" rel="stylesheet" href="${ctx}/static/css/layout.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/table.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/pagination.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/toolbar.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery.msgBox/styles/msgBoxLight.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/bpopup.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/ztree/css/zTreeStyle/zTreeStyle.css">	

<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery.msgBox/scripts/jquery.msgBox.js"></script>
<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.min.js"></script>

<script type="text/javascript" src="${ctx}/static/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${ctx}/static/ztree/js/jquery.ztree.excheck-3.5.js"></script>



</head>
<body>


<div class="west">
	<ul id="ztree" class="ztree"></ul>    
</div>         
          
<div class="center" style="border-left: solid 1px #d8d8d8;">

	<form class="form_horizontal" action="${ctx}/authc/catalog/list" id="form">
		<div class="hvcenter">		
			<label for="q_name">部门名称:</label>
			<input type="text" id="q_name" name="q_name" value="${param.q_name}" />	
			<input type="hidden" id="q_parentId" name="q_parentId" value="${param.q_parentId}" />
			
			<input type="hidden" id="pageSize" name="pageSize" value="10" /> 	
			<input type="hidden" id="pageIndex" name="pageIndex" value="1"  />
			
			<input type="hidden" id="sortName" name="sortName" value="${param.sortName}"  />
			<input type="hidden" id="sortOrder" name="sortOrder" value="${param.sortOrder}"  />
					
			<input class="btn" type="submit" value="查询" style="margin-left:7px;" />			
		</div>										 							  
	</form>
	

	<div id="toolbar" class="toolbar">
		<ul>			
		<li onclick="addRow()"><span><i class="tool_icon add_icon"></i><i class="tool_label">添加</i></span></li>
		<li onclick="editRow()"><span><i class="tool_icon edit_icon"></i><i class="tool_label">编辑</i></span></li>	
		<li onclick="deleteRow()"><span><i class="tool_icon delete_icon"></i><i class="tool_label">删除</i></span></li>	
		<li onclick="moveupRow()"><span><i class="tool_icon moveup_icon"></i><i class="tool_label">上移</i></span></li>
		<li onclick="movedownRow()"><span><i class="tool_icon movedown_icon"></i><i class="tool_label">下移</i></span></li>
		<li onclick="refresh()"><span><i class="tool_icon refresh_icon"></i><i class="tool_label">刷新</i></span></li>
		<li onclick="exportExcel()"><span><i class="tool_icon export_icon"></i><i class="tool_label">导出</i></span></li>		
		<li onclick="importExcel()"><span><i class="tool_icon import_icon"></i><i class="tool_label">导入</i></span></li>			
		<li class="right"><span><i class="tool_icon add_icon"></i><i class="tool_label">添加</i></span></li>
		</ul>
	</div>


	<table id="table" class="fixed_headers">  
		<thead>
			<tr style="cursor:pointer">		 	  			
	  			<th width="100px" id="ID">ID</th>		
	  			<th width="260px" id="NAME" abbr="部门名称">部门名称</th>	
	  			<th style="border: none; background-color: #fff;"></th>      	  
			</tr>
		</thead>
		<tbody>
	  		<c:forEach items="${catalogs}" var="catalog" varStatus="rowCounter">		  		
	        <c:choose>
	        	<c:when test="${rowCounter.count % 2 == 0}">
	           		<c:set var="rowStyle" value="odd" />
	        	</c:when>
	        	<c:otherwise>
	           		<c:set var="rowStyle" value="even" />
	        	</c:otherwise> 
	        </c:choose>
	    	<tr class="${rowStyle}" id="tr_${catalog.id}" onclick="hilightRow(this)" style="cursor:pointer">			          	
	      		<td width="100px"><c:out value="${catalog.id}"  /></td>
				<td width="260px"><c:out value="${catalog.name}" /></td>													
	    	</tr>		
			</c:forEach>						 
		</tbody>
	</table>
       
    <div class="south">
	<c:choose>
	  	<c:when test="${total > 0}">
	    	<tags:pagination current="${current}" totalPages="${totalPages}" pageSize="${pageSize}"/>
	  	</c:when>
	  	<c:otherwise>
	  		<span style="display:block; margin-left: 10px; padding-top: 10px;">查无记录!</span>
	  	</c:otherwise> 
	</c:choose>	
	</div>  
</div>




<script type="text/javascript">
var zTreeObj = null;


function onClick(event, treeId, treeNode, clickFlag) {
	//if(treeNode.isParent){
		//alert(treeNode.id);
		$("#q_parentId").val(treeNode.id);
		var form = $("#form");		
		form.attr("action","${ctx}/authc/catalog/list");
		form.attr("method","get");
		form.submit();	
		return true;
	//}
}

var setting = {
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		//beforeClick: beforeClick,
		onClick: onClick
	}
};



function filterTree(){
	var catalogName = $("#catalogNameFilter").val().trim();
	if(catalogName.length>0){
		alert( catalogName );
	}else{
		alert("360");
	}
}

var selectedRow = null;

function hilightRow(this_row){	
    $('#table tbody tr').not(this).removeClass('clicked');
    $(this_row).toggleClass('clicked');
    selectedRow = $(this_row);
}

function addRow(){
	var id = zTreeObj.getSelectedNodes()[0].id;
    //alert(catalogId);
    
	//var catalogName = zTreeObj.getSelectedNodes()[0].name;
    //alert(catalogName);
    //var parm = "?catalogId="+catalogId+"?catalogName="+catalogName; 
    
    //$("#catalogId").val(catalogId);
	//$("#catalogName").val(catalogName);
	
	var form = $("#form");
	//form.attr("action","${ctx}/catalog/create");
	form.attr("action","${ctx}/authc/catalog/create/"+id);
	form.submit();
            	
	return false;
}

function editRow(){
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法编辑！",
		    type:"info"
		});
		return false;
	}
	var catalogId =  selectedRow.attr('id').replace('tr_','');	
	var form = $("#form");
	form.attr("action","${ctx}/authc/catalog/update/"+catalogId);
	form.attr("method","get");
	form.submit();
            	
	return false;
}

function deleteRow(){
	
	if(selectedRow==null){		
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法删除！",
		    type:"info"
		});
		return false;
	}	
	var catalogId =  selectedRow.attr("id").replace("tr_", "");
	$.msgBox({
        title: "确认删除提示",
        content: "确实要删除该条记录吗？",        
        type: "confirm",        
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result) {
            if (result == "确认") {
            	var form = $("#form");
            	form.attr("action","${ctx}/authc/catalog/delete/"+catalogId);
            	form.attr("method","get");
            	form.submit();
            		
            	return false;
            }
        }
    });	
}

function exportExcel(){	
	var form = $("#form");
	form.attr("action","${ctx}/authc/catalog/exportExcel");
	form.attr("method","get");
	form.submit();
    return false; 
    
    var id = zTreeObj.getSelectedNodes()[0].id;
    alert(id);
    
	var catalogName = zTreeObj.getSelectedNodes()[0].name;
    alert(catalogName);
}

function exportExcel4(){	
	$.ajax( {
		url : "${ctx}/authc/catalog/exportExcel",	
		type : "get",
		dataType : "json",
		cache : false,
		//async: false,
		//data : { keyword : keyword	},
		success:function(data){
			//alert(data);	
			$.msgBox({
			    title:"导出EXCEL提示：",
			    content:"导出EXCEL成功，请保存到目录中！",
			    autoClose:true,	
			    timeOut: 2000,
			    type:"info"
			});
		}
	});	
    return false;	
}




function importExcel(){	
	$.jBox("iframe:${ctx}/biz/customer/uploadExcelForm", {
        title: "360搜索",
        width: 400,
        height: 400,
        buttons: { "确定":"ok","清除":"clear", "关闭":true },
        submit:function(v, h, f){
			if (v=="ok"){					
									
			}
			else if (v=="clear"){
				
            }
		}
    });
    return false;	
}


$(document).ready(function() {
	msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";		
	
	$.ajax( {
		url : "${ctx}/authc/catalog/getZtreeJson",	
		type : "get",
		dataType : "json",
		cache : false,
		async: false,
		//data : { keyword : keyword	},
		success:function(data){
			$.fn.zTree.init($("#ztree"), setting, data);	
			zTreeObj = $.fn.zTree.getZTreeObj("ztree");	
		}
	});		
	
	var nodes = zTreeObj.getNodesByParam("id", $("#q_parentId").val(), null);
	if (nodes.length>0) {
		zTreeObj.selectNode(nodes[0]);
	}else{
		var firstNodes = zTreeObj.getNodes();
		if (firstNodes.length>0) {
			zTreeObj.selectNode(firstNodes[0]);
		}		
	}
	
	$("#table tbody tr").hover(function() {   
        $(this).addClass("hover"); 
    }, function() {   
        $(this).removeClass("hover"); 
    });
    
	var sortName = $("#sortName").val();
	var sortOrder = $("#sortOrder").val();		
	var row0 = $('#table thead  tr').get()[0];
	var cell0 = row0.cells[0];
	var cell1 = row0.cells[1];
	
	if(sortName=="STUNAME"){
		if(sortOrder=="ASC"){
			cell0.innerHTML += " <img src=${ctx}/static/images/up.gif />";
			cell0.setAttribute("class", "ASC");
		}else{
			cell0.innerHTML += " <img src=${ctx}/static/images/down.gif />";
			cell0.setAttribute("class", "DESC");
		}		
	}
	
	if(sortName=="EMAIL"){
		if(sortOrder=="ASC"){
			cell1.innerHTML += " <img src=${ctx}/static/images/up.gif />";
			cell1.setAttribute("class", "ASC");
		}else{
			cell1.innerHTML += " <img src=${ctx}/static/images/down.gif />";
			cell1.setAttribute("class", "DESC");
		}
	}	
	
	$("#table th").click(function(event){		    
		var thid = $(this).attr("id");		
		$("#sortName").val(thid);
		
		var abbr = $(this).attr("abbr");	
		
		if($(this).hasClass("ASC")){				
			$("#sortOrder").val("DESC");	
			$(this).html(abbr+"<img src=${ctx}/static/images/down.gif />");
		}else{
			$("#sortOrder").val("ASC");	
			$(this).html(abbr+"<img src=${ctx}/static/images/up.gif />");
		}		
		
		var url = "${ctx}/authc/catalog/list?pageSize=10&pageIndex=1&sortName=";
		url += $("#sortName").val();
		url += "&sortOrder=";
		url += $("#sortOrder").val();
		
		$("#form").attr("action",url);
		$("#form").submit();
		
		return false;		
	}); 	
	
	$("#pageSizeSelect").change(function() {		
		$("#pageSize").val($(this).val());				
		$("#form").attr("action","${ctx}/authc/catalog/list");
		$("#form").submit();
		return false;		
    });	
});
</script>
</body>
</html>