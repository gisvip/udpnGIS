<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh">
<head>
<meta charset="utf-8" />
<title>test</title>
<style type="text/css">
.block-box {
border: 1px solid #f38809;
}

.block-box h3 {
background: #afbbdc;
color: #fff;
margin-top: 0;
padding: 15px 5px;
text-transform: uppercase;
font-weight: 700;
font-size: 15px;
}

.block-box .content {
padding: 0px;
}


#btn-call-for-action {
display: block;
margin-top: 14px;
}
.btn-huge {
font-size: 1.1em;
padding: 10px 35px;
}
.btn {
text-transform: uppercase;
font-weight: 700;
}
.btn-warning {
color: #fff;
background-color: #f7941e;
border-color: #f38809;
}
.btn {
margin-bottom: 0;
text-align: center;
vertical-align: middle;
cursor: pointer;
background-image: none;
border: 1px solid transparent;
white-space: nowrap;
line-height: 1.42857143;
border-radius: 4px;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
}

a {
background: transparent;
text-decoration: none;
}

.btn-huge i {
margin-right: 2px;
}
.fa {
display: inline-block;
font-family: FontAwesome;
font-style: normal;
font-weight: normal;
line-height: 1;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}

.fa-download:before {
content: "\f019";
}





</style>
</head>
<body>
<div class="block-box">
	<h3>Highlights</h3>
  	<div class="content">
  		<a id="btn-call-for-action" href="https://www.eclipse.org/downloads/" class="btn btn-huge btn-warning"><i class="fa fa-download"></i> Download</a>
	</div>
</div>


</body>
</html>