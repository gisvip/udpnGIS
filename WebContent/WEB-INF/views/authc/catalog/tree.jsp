<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>catalogTree</title>
<style type="text/css">
html {    
	margin:0px;
    padding:0px;
    border:0px;    
    overflow:hidden;    
}
body {
    margin:0px;
    padding:0px;
    border:0px;
    width:100%;
    height:100%;
    font-size:12px;
    background-color: #F7F9FC;
    overflow:hidden;
}


div, ul, li, h3, img, form, label, span, input { margin: 0px; padding: 0px; }

ul.ztree {
margin-top: 0px;
display:block;
overflow-y:auto;
overflow-x:hidden;
}

</style>

<link type="text/css" rel="stylesheet" href="${ctx}/static/ztree/css/zTreeStyle/zTreeStyle.css">		
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/ztree/js/jquery.ztree.core-3.5.js"></script>

</head>
<body>
<input type="hidden" id="name" /><input type="hidden" id="id" />
<ul id="ztree" class="ztree"></ul>


<script type="text/javascript">
	
var zTreeObj = null;

function onClick(event, treeId, treeNode, clickFlag) {
	/* if(treeNode.isParent){
		alert(treeNode.id);
		return false;
	}else{			
		//alert(treeNode.id);
	} */
	
		//alert("==id=="+treeNode.id);
		//alert("==pId=="+treeNode.pId);
		//alert("==name=="+treeNode.name);
		
	$("#id").val(treeNode.id);
	$("#name").val(treeNode.name);	  
	
	//alert($("#id").val());
	//alert($("#name").val());
	
	return false;
}

var setting = {
	view: {
		dblClickExpand: false,
		showLine: true,
		selectedMulti: false
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		onClick: onClick
	}
};


$(document).ready( function() {		
	$.ajax( {
		url : "${ctx}/authc/catalog/getZtreeJson",	
		type : "get",
		dataType : "json",
		cache : false,
		async: false,
		//data : { keyword : keyword	},
		success:function(data){
			$.fn.zTree.init($("#ztree"), setting, data);	
			//zTreeObj = $.fn.zTree.getZTreeObj("ztree");	
		}
	});		
	
	/* var nodes = zTreeObj.getNodes();
	if (nodes.length>0) {
		zTreeObj.selectNode(nodes[0]);
	} */
	
});
</script>
</body>
</html>