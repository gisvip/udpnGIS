<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>管理员编辑</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-validation-1.14.0/jquery.validate.min.js"></script>

<script type="text/javascript">
function forback(){	
	window.location.replace("${ctx}/authc/user/list");
	e.preventDefault();
	return false;
}

$(document).ready(function(){		
	var url = "${ctx}/authc/user/checkLoginName";
	var userId = document.getElementById("id").value;
	if(userId != ""){
		url = "${ctx}/authc/user/checkLoginNameById/" + userId; 
	}
	
	$("#form").validate({		 
		rules: {			
			username: {
				minlength: 6,
				maxlength: 32,
				required: true,
				remote: url
			},
			password: {
				minlength: 6,
				maxlength: 32,
				required: true
			}
	  	},
	  	messages: {	  		
	  		username: {				
	  			minlength: "登录名称至少是6个字符",
	  			maxlength: "登录名称最多为32个字符",
	  			required: "请输入登录名称",
	  			remote: "登录名称已经存在"
			},
			password: {				
	  			minlength: "密码至少是6个字符",
	  			maxlength: "密码最多为32个字符",
	  			required: "请输入密码"
			}    
	  	},	
		errorPlacement: function(error, element) {
			$(".tip").hide();			
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});	
	
}); 
</script>
</head>
<body>		
<form:form id="form" modelAttribute="user" method="post" action="${ctx}/authc/user/${action}">
	<form:hidden path="id" />
    <form:hidden path="salt" />
    <form:hidden path="locked" />
    <c:if test="${action ne 'create'}">
    	<form:hidden path="password"/>
    </c:if>
	
	<h1></h1>
	<table class="table">        
    <tr class="even">   
      <td style="text-align: right;width: 130px;"><form:label path="username">*登录名称:</form:label></td>
      <td style="width: 160px;"><form:input path="username" style="float:left;" /></td>
      <td class="status"><span class="tip">6~32个字符,以英文字母开头,包括英文字母或阿拉伯数字</span><form:errors path="username" cssClass="errors" element="div"/></td>     
    </tr>    
    
    <c:if test="${action eq 'create'}">	
	<tr class="odd">  
		<td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><form:label path="password">*密码:</form:label></span></td>
	    <td style="width: 160px;"><form:password path="password" style="float:left;" /></td>
	    <td class="status"><span class="tip">6~32个字符,以英文字母开头,包括英文字母或阿拉伯数字,推荐字母与数字混合组成</span><form:errors path="password" cssClass="errors" element="div"/></td>
	</tr>
	</c:if>	
	
	<tr class="even">   
      <td style="text-align: right;width: 130px;"><form:label path="realname">管理员姓名:</form:label></td>
      <td style="width: 160px;"><form:input path="realname" style="float:left;" /></td>
      <td class="status"><span class="tip">管理员真实中文姓名</span><form:errors path="realname" cssClass="errors" element="div"/></td>     
    </tr>
    
    <tr class="odd">   
      <td style="text-align: right;width: 130px;"><form:label path="telephone">联系电话:</form:label></td>
      <td style="width: 160px;"><form:input path="telephone" style="float:left;" /></td>
      <td class="status"><form:errors path="telephone" cssClass="errors" element="div"/></td>     
    </tr>
    
    <tr class="even">   
      <td style="text-align: right;width: 130px;"><form:label path="email">电子邮件:</form:label></td>
      <td style="width: 160px;"><form:input path="email" style="float:left;" /></td>
      <td class="status"><form:errors path="email" cssClass="errors" element="div"/></td>     
    </tr>
                    
	<tr class="odd">      
      <td colspan="3" style="text-align:center;">      
      	<input class="btn" type="submit" value="提 交" />        	
      	<input class="btn" type="button" value="返回" onclick="forback();" />           
      </td>      
    </tr>
  	</table>	 		
</form:form>
</body>
</html>