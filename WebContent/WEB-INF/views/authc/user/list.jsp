<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>用户管理</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/layout.css" />

<link type="text/css" rel="stylesheet" href="${ctx}/static/css/pagination.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/toolbar.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery.msgBox/styles/msgBoxLight.css" />

<link type="text/css" rel="stylesheet" href="${ctx}/static/css/table.css" />

<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery.msgBox/scripts/jquery.msgBox.js"></script>

<script type="text/javascript">
var selectedRow = null;

function select(thisRow){	
    $('#table tbody tr').not(this).removeClass('clicked');
    $(thisRow).toggleClass('clicked');
    selectedRow = $(thisRow);   
}

function create(){
	var form = $("#form");
	form.attr("action","${ctx}/authc/user/create");
	form.attr("method","get");
	form.submit();
            	
	return false;
}

function showRolesByUserId(){	
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法查看/分配角色！",
		    type:"info"
		});
		return false;
	}
	
	var userId =  selectedRow.attr('id').replace('tr_','');
	//alert(userId);
	window.location.href="${ctx}/authc/user/showRolesByUserId/"+userId;
	return false;
}

function update(){
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法编辑！",
		    type:"info"
		});
		return false;
	}
	var userId =  selectedRow.attr('id').replace('tr_','');	
	var form = $("#form");
	form.attr("action","${ctx}/authc/user/update/"+userId);
	form.attr("method","get");
	form.submit();  
            	
	return false;
}

function query(){	
	var form = $("#form");
	form.attr("action","${ctx}/authc/user/list");
	form.attr("method","post");
	form.submit();
}

function confirmResetPasswordShow(){
	if(selectedRow==null){
		$.msgBox({
		    title:"信息提示：",
		    content:"未选择任何记录，无法重置用户密码！",
		    type:"info"        
		});
		return false;
	}
	
	confirmResetPassword(); 	
}


function confirmResetPassword(){
	$.msgBox({
        title: "重置用户密码操作提示：",       
        type: "prompt",      
        inputs: [{ type: "password", name:"newPassword", header: "请输入新密码：" }],
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result, values) {
        	if (result == "确认") {        		
        		$("#password").val(values[0].value);
        		updatePassword();
            }
        }
    });
}

function updatePassword(){	
	var userId =  selectedRow.attr('id').replace('tr_','');	
	var form = $("#form");
	form.attr("action","${ctx}/authc/user/updatePassword/"+userId);
	form.attr("method","POST");
	form.submit();
            	
	return false;
}

function confirmUpdateAccountShow(){
	if(selectedRow==null){
		$.msgBox({
		    title:"账户锁定/解锁操作提示",
		    content:"未选择任何记录，无法进行账户锁定/解锁操作！",
		    type:"info"        
		});
		return false;
	}	
	
	var status = $("td:eq(1)", selectedRow).attr("abbr");	

	if(status=="false"){		
		var tip = "当前账户状态为正常，你确定锁定该用户账户吗？";
		$("#status").val("true");
	}else{		
		var tip = "当前账户状态已锁定，你确定恢复该用户账户状态为正常吗？";		
		$("#status").val("false");
	}  	
	confirmUpdateAccount(tip);
}


function confirmUpdateAccount(tip){
	$.msgBox({
        title: "账户锁定/解锁操作提示：",
        content: tip,        
        type: "confirm",        
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result) {
        	if (result == "确认") {
        		updateAccount();
            }
        }
    });
}

function updateAccount(){
	var userId =  selectedRow.attr('id').replace('tr_','');	
	var form = $("#form");
	form.attr("action","${ctx}/authc/user/updateAccount/"+userId);
	form.attr("method","post");
	form.submit();
            	
	return false; 
}

function deleted(){
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法删除！",
		    type:"info"
		});
		return false;
	}
	var userId =  selectedRow.attr("id").replace("tr_", "");
	$.msgBox({
        title: "确认删除提示",
        content: "确实要删除该条记录吗？",        
        type: "confirm",        
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result) {
            if (result == "确认") {
            	var form = $("#form");
            	form.attr("action","${ctx}/authc/user/delete/"+userId);
            	form.attr("method","get");
            	form.submit();
            		
            	return false;
            }
        }
    });
}


$(document).ready(function() {
	msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";		
	
	$("#table tbody tr").hover(function() {   
        $(this).addClass("hover"); 
    }, function() {   
        $(this).removeClass("hover"); 
    });
    
	var sortName = $("#sortName").val();
	var sortOrder = $("#sortOrder").val();		
	var row0 = $('#table thead tr').get()[0];
	var cell0 = row0.cells[0];		
	
	if(sortName=="USERNAME"){
		if(sortOrder=="ASC"){
			$("#USERNAME_SPAN").css("background-image", "url(${ctx}/static/images/up-arrow.png)");
			cell0.setAttribute("class", "ASC");			
		}else{
			$("#USERNAME_SPAN").css("background-image", "url(${ctx}/static/images/down-arrow.png)");
			cell0.setAttribute("class", "DESC");							
		}		
	}	
	
	$("#table thead tr th").click(function(event){		    
		var thid = $(this).attr("id");			
		
		if(thid != null){
			$("#sortName").val(thid);						
			
			if($(this).hasClass("ASC")){				
				$("#sortOrder").val("DESC");					
			}else{
				$("#sortOrder").val("ASC");					
			}		
			
			$("#form").submit();
		}		
		
		return false;		
	}); 	
	
	$("#pageSizeSelect").change(function() {		
		$("#pageSize").val($(this).val());				
		$("#form").submit();
		return false;		
    });	
});
</script>
</head>
<body>
<c:if test="${not empty message}">		
<script type="text/javascript">
msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";			
$.msgBox({
    title:"信息提示：",
    content:"${message}",
    autoClose:true,	
    timeOut: 2000,
    type:"info"
});
</script>
</c:if>	

<div class="north_form">	
	<form class="form_horizontal" id="form">
		<div class="hvcenter">
			<label for="username">姓名:</label>
			<input type="text" class="q-input" id="username" name="username" value="${param.username}" /> 			
			
			<input type="hidden" name="pageSize" id="pageSize" value="10" /> 	
			<input type="hidden" name="pageIndex" id="pageIndex" value="1" />
			
			<input type="hidden" name="sortName" value="${param.sortName}" id="sortName" />
			<input type="hidden" name="sortOrder" value="${param.sortOrder}" id="sortOrder" />			
					
			<input type="hidden" name="status" id="status" />		
			<input type="hidden" id="password" name="password" />	
			 
			<input type="button" value="查询" class="btn" style="margin-left: 20px;" onclick="query()" />       
		</div>										 							  
	</form>
</div>

<div class="north_toolbar">
	<div class="toolbar">
		<ul>			
		<shiro:hasPermission name="user:create">
        	<li onclick="create()"><span><i class="tool_icon add_icon"></i><i class="tool_label">添加</i></span></li>
        </shiro:hasPermission>
        <shiro:hasPermission name="user:update">
        	<li onclick="update()"><span><i class="tool_icon edit_icon"></i><i class="tool_label">编辑</i></span></li>
        </shiro:hasPermission> 
        <shiro:hasPermission name="user:delete">
        	<li onclick="deleted()"><span><i class="tool_icon delete_icon"></i><i class="tool_label">删除</i></span></li>	
        </shiro:hasPermission>
        <shiro:hasPermission name="user:update">
        	<li onclick="confirmResetPasswordShow()"><span><i class="tool_icon key_icon"></i><i class="tool_label">重置密码</i></span></li>
        </shiro:hasPermission>
        <shiro:hasPermission name="user:update">
        	<li onclick="confirmUpdateAccountShow()"><span><i class="tool_icon lock_icon"></i><i class="tool_label">账号锁定/解锁</i></span></li>
        </shiro:hasPermission>		
        <shiro:hasPermission name="user:view">
        	<li onclick="showRolesByUserId()"><span><i class="tool_icon edit_icon"></i><i class="tool_label">查看/分配角色</i></span></li>
        </shiro:hasPermission>					
		</ul>
	</div>
</div>
     
  <div id="wrapper">
	<table id="table" >
		<thead>
	    <tr>		      
	      <th width="120px" id="USERNAME" class="icansort"><span style="cursor: pointer;color:#07519a;" id="USERNAME_SPAN">登录名称</span></th>		      
	      <th width="200px"><span>账户状态</span></th>
	      <th width="120px"><span>管理员姓名</span></th>	
	      <th width="100px"><span>电话号码</span></th>	
	      <th width="125px"><span>电子邮件</span></th>	  
	      <th style="border: none; background-color: #fff;"></th>         	  
	    </tr>
	    </thead>
	  <tbody class="scrollContent">
	  	<c:forEach items="${users}" var="user" varStatus="rowCounter">		  		
	        <c:choose>
	          <c:when test="${rowCounter.count % 2 == 0}">
	            <c:set var="rowStyle" value="odd" />
	          </c:when>
	          <c:otherwise>
	            <c:set var="rowStyle" value="even" />
	          </c:otherwise> 
	        </c:choose>
	        <tr class="${rowStyle}" id="tr_${user.id}" onclick="select(this)" style="cursor:pointer">
				<td width="120px">${user.username}</td>				                
                <td width="200px" abbr="${user.locked}">                
	                <c:choose>
			          <c:when test="${user.locked}">
			            <c:out value="已锁定" />
			          </c:when>
			          <c:otherwise>
			            <c:out value="正常"/>
			          </c:otherwise> 
			        </c:choose>
                </td>
                <td width="120px">${user.realname}</td>	
                <td width="100px">${user.telephone}</td>	
                <td width="120px">${user.email}</td>	
	        </tr>		
		</c:forEach>				 
	  </tbody>
	</table>
  </div>        

<div class="south">
	<c:choose>
	  <c:when test="${total > 0}">
	    <tags:pagination current="${current}" totalPages="${totalPages}" pageSize="${pageSize}"/>
	  </c:when>
	  <c:otherwise>
	    <span style="display:block; margin-left: 20px; padding-top: 15px;">查无记录!</span>
	  </c:otherwise> 
	</c:choose>
</div>
</body>
</html>