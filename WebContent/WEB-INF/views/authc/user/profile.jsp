<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>profile</title>
<link href="${ctx}/static/styles/form_xui.css" type="text/css" rel="stylesheet" />

<style type="text/css">

html {height: 100%;}
body {
margin: 0;
padding: 0;
border:0;    
width:100%; 
height: 100%;
overflow:hidden; 
display: block;width: 100%;height: 100%; min-height: 100%; background: #FFF url('${ctx}/static/images/corner.png') no-repeat right bottom;
}


* { font-family: Verdana; font-size: 96%; }
label { width: 10em; float: left; }
label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
p { clear: both; }
.submit { margin-left: 12em; }
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

#form .status {  
  padding-left: 8px;
  vertical-align: middle;
  width: 246px;
  white-space: nowrap;
  text-align: left;  
}

#form label.error {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
  padding-left: 16px;
  vertical-align:middle;
  font-weight: bold;
  color: #EA5200;
}

#form label.checked {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
}

.errors {
  background:url("${ctx}/static/images/unchecked.gif") no-repeat 0 0;
  padding-left: 16px;
  vertical-align:middle;
  font-weight: bold;
  color: #EA5200;
}

#form label.error {
	margin-left: 2px;
	width: auto;
	display: inline;
}

table{ border-collapse:collapse; border:solid 1px Black; }
table td{ width:50px; height:20px;  border:solid 1px Black; padding:5px;}


.xui_button {
display: inline-block;
width: 71px;
height: 32px;
padding: 5px 0px;


background: -moz-linear-gradient(top,#c3dfef 0,#C2D5E3 100%);
background: -o-linear-gradient(top,#c3dfef 0,#C2D5E3 100%);
background: linear-gradient(to bottom,#c3dfef 0,#C2D5E3 100%);
background-repeat: repeat-x;
border: 1px solid #C2D5E3;


-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
border:1px solid rgba(0,0,0,0.25);


-webkit-box-shadow:0 1px 3px rgba(0,0,0,0.25);
-moz-box-shadow:0 1px 3px rgba(0,0,0,0.25);
box-shadow:0 1px 3px rgba(0,0,0,0.25);
text-shadow:0 -1px 1px rgba(0,0,0,0.25);
color: #4f6b72 !important;

font-weight:bold;
font-size: 15px;
font-family: SimSun, Arial !important; 
text-decoration: none;
cursor: pointer;
}

.xui_button:hover {	
	/* background-color:#C2D5E3; */
	background: linear-gradient(to bottom, #2daebf 0px, #C2D5E3 100%) repeat-x scroll 0% 0% transparent;
}


.ui-box-360 {
    font-size: 12px;
    margin: 0;
    padding: 0;  
    opacity: 0;  
    height: auto; 
    width: 460px;
    border-radius: 2px;
    display: none; 
    /* border: 1px solid rgb(112, 156, 210); */   
}

.ui-box-head-360 {
    position: relative;
    cursor:move;
    /* background-color: #4592f0; */
	border-bottom: 1px solid #3484e5; 
	border-top-left-radius: 2px;
	border-top-right-radius: 2px;
	line-height: 32px;
	height: 32px;   
	background-color: rgb(112, 156, 210); 
	font-size: 14px; font-weight: bold; 
	
}

.ui-box-head-360 .ui-box-head-title-360 {
    color: #d6ebf8;
	font-size: 14px;    
    font-weight: bold;
    float: left;
    display: inline;
    margin: 0;
	padding-left: 14px;
}

.ui-box-head-360 .ui-box-head-close-360 {
    float: right;    
    margin-right: 10px;
	margin-top: 4px;  
    display: block;height: 22px;width: 22px;
    background: url(${ctx}/static/images/win_close.png) no-repeat -22px 0px;   
}

.ui-box-head-360 .ui-box-head-close-360:hover {    
    background: url(${ctx}/static/images/win_close.png) no-repeat -22px -22px;        
}

.ui-box-head-360 .ui-box-head-text-360 {
    margin-left: 10px;
    color: #808080;
    float: left;
}

.ui-box-container-360 {
    background: #fff;
    border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;   
}

.ui-box-content-360 {
    padding: 10px;
    height: 300px; 
    width: 600px; 
	display:block;
	overflow-y:auto;overflow-x:hidden;
}


.ui-box-footer-360 {     
    text-align: center;    
    border-top-style: solid;
    /* border-top-color: #6C92AD; */
    border-top-color: #e4e5e9;
    border-top-width: 1px;	
	height: 42px;
	
	background-color: #F6F9FC;
	border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;
}


#uploadStatusBtn {
bottom: 10px;
height: 16px;
line-height: 16px;
overflow: hidden;
position: absolute;
right: 9px;
}
.y-btn {
margin: 0 5px;
min-width: 52px;
}
.y-btn-gray:hover {
color: #fff;
}
.y-btn-gray:hover {
background: #3083eb;
border-color: #1f74df;
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#3083eb',endColorstr='#3083eb',GradientType=0);
}
.y-btn-gray {
background-image: -webkit-linear-gradient(bottom,#f5f9fc,#fff);
background-image: -moz-linear-gradient(bottom,#f5f9fc,#fff);
background-image: -o-linear-gradient(bottom,#f5f9fc,#fff);
background-image: linear-gradient(to top,#f5f9fc,#fff);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff',endColorstr='#f5f9fc',GradientType=0);
}
.y-btn {
-khtml-user-select: none;
background: #fff;
border-radius: 2px;
border-width: 1px;
border-style: solid;
border-color: #bbcbdf;
color: #536083;
cursor: pointer;
display: inline-block;
font-size: 12px;
font-family: arial;
height: 16px;
min-width: 47px;
overflow: hidden;
padding: 6px 14px;
text-align: center;
-moz-user-select: none;
-webkit-user-select: none;
-ms-user-select: none;
user-select: none;
}

</style>
<link type="text/css" rel="stylesheet" href="${ctx}/static/ztree/css/zTreeStyle/zTreeStyle.css">
<link href="${ctx}/static/msgBox/styles/msgBoxLight.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jQuery.msgBox/scripts/jquery.msgBox.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/static/ztree/js/jquery.ztree.core-3.5.js"></script>

<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$("#form").validate({
		rules: {			
			plainPassword: {
				required: true,
				minlength: 6
			},
			confirmPassword: {
				required: true,
				minlength: 6,
				equalTo: "#plainPassword"
			}			
		},
		messages: {			
			plainPassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			},
			confirmPassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long",
				equalTo: "Please enter the same password as above"
			}			
		},
		errorPlacement: function(error, element) {
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}
	});

});
</script>
</head>
<body>
	<form id="form" method="post" action="${ctx}/authc/user/profile">
		<input type="hidden" name="id" value="${user.id}"/>
		<table class="table">      
		    <tr class="even">   
		      <td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><label for="plainPassword">密码:<strong>*</strong></label></span></td>
		      <td style="width: 160px;"><input type="password" id="plainPassword" name="plainPassword" style="float:left;" /></td>
		      <td class="status"></td>     
		    </tr>    
	    
		    <tr class="odd">  
		      <td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><label for="confirmPassword">确认密码:</label></span></td>
		      <td style="width: 160px;"><input type="password" id="confirmPassword" name="confirmPassword" class="input-large" /></td>
		      <td class="status"></td>
		    </tr>               
	                
		    <tr class="even">      
		      <td colspan="3" style="text-align:center;">      
		      <input class="xui_button" type="submit" value="提 交" />  
		      <input class="xui_button" type="button" value="返回" onclick="history.back()" />		           
		      </td>      
		    </tr>    
	  	</table>		
	</form>
</body>
</html>