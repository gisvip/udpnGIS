<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>logout</title>
</head>
<body>
<h1>Logout Successful</h1>
<h3>Authentication credentials have been cleared from your browser.</h3>
<h4>You should close the login window if it is still open.</h4>

Copyright 1998-2011 Regents of the University of Minnesota.
The University of Minnesota is an equal opportunity educator and employer.
</body>
</html>