<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>首页</title>
<link type="image/x-icon" rel="shortcut icon" href="${ctx}/static/images/favicon.ico" />

<style type="text/css">
html,body {
	height: 100%;
	overflow:hidden;
}

.north {
    margin:0px;
    padding:0px;
    border:0px;    
    left:0px;
    top:0px;
    width:100%; 
    height:46px;        
    z-index: 1023;
    position: relative;
}

.west {
	position: relative;
	left:0px;
	top:0px;
	float:left;
	width: 200px;	
	border:none;	
	height: calc(100% - 46px);
	overflow:hidden;
}

.center{
	position: relative;
	z-index: 0;
	top:0px;
	left:0px;
	width:auto;
	height:-moz-calc(100% - 76px);
	height:-webkit-calc(100% - 76px);
	height: calc(100% - 76px);
	border-left: solid 1px #d8d8d8;
	overflow: hidden;
}  

.south{   
	position: fixed;
	left:0px;
	bottom: 0px;
	margin:0px;
	border:0px;
	width:100%;
	height:30px;
	overflow:hidden;
	background-color: silver;
	z-index: 1023;
}

.topnav{position:fixed;top:0;left:0;right:0;z-index:10000;overflow:visible;text-shadow:none;height:46px;width:100%;-webkit-box-shadow:0 1px 1px #ccc;-moz-box-shadow:0 1px 1px #ccc;-o-box-shadow:0 1px 1px #ccc;box-shadow:0 1px 1px #ccc}
.innerwrap{position:relative;background-color:#0f8bc0;height:46px}

.logo{display:inline-block;vertical-align:top}
.logo span{background:url('${ctx}/static/images/logo.png') no-repeat left center;width:181px;height:42px;display:inline-block;margin-left: 30px;}
ul.site-nav{list-style-type:none;display:inline-block;margin:0;padding:0;height:46px;margin-left: 60px;}
ul.site-nav li{display:inline-block;border-left:1px solid #149acb;height:100%;margin:0;line-height:46px;cursor:pointer}
ul.site-nav li:hover{background-color:#0c6185}
.active{background-color: #38c0c8} /* #38c0c8 #0c6185 */
ul.site-nav li.last{border-right:1px solid #149acb}
ul.site-nav li a{color:#fff;font-size:15px;padding:0 15px;display:inline-block; text-decoration: none;}
ul.site-nav li a:hover{text-decoration:none; }

ul.account-nav{display:inline-block;list-style-type:none;margin:0;float:right; margin-right:60px;}
ul.account-nav>li{display:inline-block;margin:0;vertical-align:top;line-height:46px;border-left:1px solid #149acb;cursor:pointer}
ul.account-nav>li.sign-in:hover{background-color:#0c6185}
ul.account-nav>li.join-scratch:hover{background-color:#0c6185}
ul.account-nav>li>span{color:#fff;font-weight:bold;font-size:15px;display:inline-block;padding:0 10px}
ul.account-nav li a{color:#fff; text-decoration:none;}

#scratch-loader, #scratch .scratch_loading {
  position: absolute;
  top: 0;
  background-image: url(${ctx}/static/images/ajax_loader_blue_48.gif);
  background-position: center center;
  background-repeat: no-repeat;
  width: 100%;
  height: 100%;
}

.copyright {
font-size:13px;
display: inline-block;
height: 32px;
line-height: 32px;
color: #1b3767;
background: url("${ctx}/static/images/copyright_bg.jpg") repeat-x;
left: 0;
width: 100%;
bottom: 0px;
text-align:center; 
border-top: solid 1px #688fd0;
}

</style>
    

</head>
<body>
<div class="north">	
  <div class="topnav">
      <div class="innerwrap" id="innerwrap">        
          <a href="/" class="logo"><span></span></a>
          <ul class="site-nav">
            <c:if test="${not empty menus}">			
				<c:forEach items="${menus}" var="menu">
          			<li><a href="${ctx}/${menu.url}" target="westFrame">${menu.name}</a></li>	        	
	    		</c:forEach>
			</c:if>            
          </ul>            
            
          <ul class="account-nav">          
          	<li class="join-scratch" data-control="registration"><span><shiro:user>欢迎<shiro:principal/></shiro:user></span></li>             
            <li class="sign-in mobile" data-control="modal-login"><span><a href="${ctx}/admin/logout">退出</a></span></li>
          </ul>       
      </div>
    </div>	
</div>        

<div class="west">	
	<iframe id="westFrame" name="westFrame" width="100%" height="100%" src="about:blank" style="border: none; padding: 0; margin: 0;"></iframe>
</div>          
          
<div class="center">
	<iframe id="centerFrame" name="centerFrame" width="100%" height="100%" src="about:blank" style="border: none; padding: 0; margin: 0;"></iframe>
</div>

<div class="south">
<div class="copyright">	
	<span style="font-family:arial;">版权所有：Copyright &copy; 2015 - 海棠花互联网+科技有限公司版权所有</span>
	<span style="margin-left: 15px">海棠花互联网+科技承建</span>	
	<span style="margin-left: 15px">服务监督电话：+86-15921867671</span>
	<span style="float: right; padding-right: 20px;">意见反馈</span>
</div>
</div>
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript">
$(document).ready( function() {				
	$("#innerwrap li").click(function() {		
		$(this).addClass("active").siblings().removeClass("active"); 		
	});	
});
</script>
</body>
</html>