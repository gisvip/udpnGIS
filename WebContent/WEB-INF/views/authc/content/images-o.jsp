<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" /> 
<title>图片管理</title>
<link rel="stylesheet" type="text/css" href="${ctx}/static/lightbox/jquery.lightbox.css">
<link rel="stylesheet" href="${ctx}/static/grid/col.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/2cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/3cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/4cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/5cols.css" media="all">
<link rel="stylesheet" href="${ctx}/static/grid/12cols.css" media="all">
<style type="text/css">
.container {
        min-width: 800px;
        margin: 0 auto;
        
    }
.gallery {
        list-style: none;
        overflow: hidden;
        padding: 0;
        margin: 0;
        display: inline-block;    
		vertical-align: middle;
    }
    
    .gallery li {
        float: left;
        margin: 4px; 
        border: 1px solid #e7e7e7;
    box-shadow: 0 0 8px #dcdcdc;
        
    }
    

.li-tags {
    position: absolute;
    left: 10px;
}

.gallery img{ max-width: 200px; max-height: 200px; border: 0; }

.img_alt {
    
    color: #3eaaee;
    margin: 0;
    max-width: 200px;
    
    font-weight: bold;
    border-top: 1px #dedbd2 dashed;
    
    text-shadow: 2px 2px 0px rgba(0, 0, 0, 0.05);
}




</style>
</head>
<body style="background: #f9f9f9;">

<div class="container">
<ul class="gallery">
<c:forEach items="${images}" var="image" varStatus="rowCounter">		  		
    
    	<li>
    	<a href="${path}/${image.name}" data-caption="${image.description}">
    	<span class="li-tags">
				<strong>del</strong>
			</span>
    	<img src="${path}/${image.name}" alt="${image.description}"></a>
    	
    	</li>    
    		
</c:forEach>
</ul>
</div>


<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/lightbox/jquery.lightbox.min.js"></script>
<script type="text/javascript">
$(function() {
    $('.gallery a').lightbox(); 
 

 
});
</script>
</body>
</html>