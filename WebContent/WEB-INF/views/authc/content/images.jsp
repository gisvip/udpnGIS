<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" /> 
<title>图片管理</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/layout.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/table.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/pagination.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/toolbar.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery.msgBox/styles/msgBoxLight.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/bpopup.css" />

<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery.msgBox/scripts/jquery.msgBox.js"></script>
<script type="text/javascript" src="${ctx}/static/bpopup/jquery.bpopup.min.js"></script>

<script type="text/javascript">
var selectedRow = null;

function select(thisRow){	
    $('#table tbody tr').not(this).removeClass('clicked');
    $(thisRow).toggleClass('clicked');
    selectedRow = $(thisRow);
}

function query(){	
	var form = $("#form");
	form.attr("action","${ctx}/authc/content/images");
	form.attr("method","post");
	form.submit();
}

function create(){
	window.location.href = "${ctx}/authc/student/create";
}

function update(){
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法编辑！",
		    type:"info"
		});
		return false;
	}
	var roleId =  selectedRow.attr('id').replace('tr_','');	
	window.location.href = "${ctx}/authc/student/"+roleId+"/update";
}

function deleted(){		
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法删除！",
		    type:"info"
		});
		return false;
	}
	var roleId =  selectedRow.attr("id").replace("tr_", "");
	$.msgBox({
        title: "确认删除提示",
        content: "确实要删除该条记录吗？",        
        type: "confirm",        
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result) {
            if (result == "确认") {
            	var form = $("#form");
            	form.attr("action","${ctx}/authc/student/"+roleId+"/delete");
            	form.attr("method","get");
            	form.submit();
            }
        }
    });	
}

function export2Excel(){	
	$.msgBox({
        title: "确认导出为Excel提示",
        content: "确实要导出为Excel吗？",        
        type: "confirm",        
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result) {
            if (result == "确认") {
            	var form = $("#form");
            	form.attr("action","${ctx}/authc/student/export");
            	form.attr("method","post");
            	form.submit();
            		
            	return false;
            }
        }
    });	
}

function import2Excel(){	
	$("#registerbox").bPopup({
		modal: false,
		modalClose: false, 
	    speed: 450,
	    transition: 'slideDown',
		content: "iframe", 
		contentContainer: "#registerContainer",
		loadUrl:"${ctx}/authc/student/import"		
	});
}


function registerClose(){		
	closeEventFlag = true;
	$("#registerContainer").empty();
	$("#registerbox").bPopup().close(); 	
}

function import2Excel(){	
	$("#registerbox").bPopup({
		modal: false,
		modalClose: false, 
	    speed: 450,
	    transition: 'slideDown',
		content: "iframe", 
		contentContainer: "#registerContainer",
		loadUrl:"${ctx}/authc/student/import"		
	});
}


function preview(the){
	//alert(the.src);
	$('#registerbox').bPopup({
        content:'image', //'ajax', 'iframe' or 'image'
        contentContainer:'#registerContainer',
        loadUrl: the.src
    });
}

/* function import2Excel(){		
	$("#element_to_pop_up").bPopup({
		autoClose: false,
		modal: false,	 	
	 	modalClose: false, 
	 	opacity: 0.6,
	    speed: 500,
	    transition: 'slideBack'
		
	});
}

function bPopupClose(){	
	closeEventFlag = true;
	$("#element_to_pop_up").bPopup().close(); 
}

function bPopupCancel(){	
	closeEventFlag = false;
	$("#element_to_pop_up").bPopup().close(); 
} */


$(document).ready(function() {
	msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";		
	
	$("#table tbody tr").hover(function() {   
        $(this).addClass("hover"); 
    }, function() {   
        $(this).removeClass("hover"); 
    });
    
	var sortName = $("#sortName").val();
	var sortOrder = $("#sortOrder").val();		
	var row0 = $('#table thead tr').get()[0];
	var cell0 = row0.cells[0];		
	
	if(sortName=="STDNTNAME"){
		if(sortOrder=="ASC"){
			$("#STDNTNAME_SPAN").css("background-image", "url(${ctx}/static/images/up-arrow.png)");
			cell0.setAttribute("class", "ASC");			
		}else{
			$("#STDNTNAME_SPAN").css("background-image", "url(${ctx}/static/images/down-arrow.png)");
			cell0.setAttribute("class", "DESC");							
		}		
	}	
	
	$("#table thead tr th").click(function(event){		    
		var thid = $(this).attr("id");			
		
		if(thid != null){
			$("#sortName").val(thid);						
			
			if($(this).hasClass("ASC")){				
				$("#sortOrder").val("DESC");					
			}else{
				$("#sortOrder").val("ASC");					
			}		
			
			query();
			//$("#form").submit();
		}		
		
		return false;		
	}); 	
	
	$("#pageSizeSelect").change(function() {		
		$("#pageSize").val($(this).val());				
		query();		
    });	
}); 
</script>

</head>
<body>
<c:if test="${not empty message}">			
<script type="text/javascript">		
msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";
$.msgBox({
    title: "信息提示：",
    content: "${message}",
    autoClose: true,	
    timeOut: 2000,
    type: "info"
});
</script>
</c:if>

<div class="north_form">			
	<form class="form_horizontal" id="form">		
		<div class="hvcenter">			
			<label for="description">图片描述:</label>
			<input type="text" id="description" name="description" value="${param.description}" />
			
			<span>上传时间:</span>
			<input type="text" id="beginTime" name="beginTime" value="${param.beginTime}" />		
			<span>至</span>					
			<input type="text" id="endTime" name="endTime" value="${param.endTime}" />
		
			<input type="hidden" id="pageSize" name="pageSize" value="10" /> 	
			<input type="hidden" id="pageIndex" name="pageIndex" value="1" />
			
			<input type="hidden" id="sortName" name="sortName" value="${param.sortName}" />
			<input type="hidden" id="sortOrder" name="sortOrder" value="${param.sortOrder}" />					
			
			<input type="button" value="查询" class="btn" style="margin-left: 20px;" onclick="query()" /> 	
		</div>										 							  
	</form>
</div>

<div class="north_toolbar">
	<div class="toolbar">
		<ul>			
		<li onclick="create()"><span><i class="tool_icon add_icon"></i><i class="tool_label">添加</i></span></li>
        <li onclick="update()"><span><i class="tool_icon edit_icon"></i><i class="tool_label">编辑</i></span></li>
        <li onclick="deleted()"><span><i class="tool_icon delete_icon"></i><i class="tool_label">删除</i></span></li>
        <li onclick="export2Excel()"><span><i class="tool_icon export_icon"></i><i class="tool_label">导出</i></span></li>		
		<li onclick="import2Excel()"><span><i class="tool_icon import_icon"></i><i class="tool_label">导入</i></span></li>					
		</ul>
	</div>
</div>
  
<div class="center">
  <div id="wrapper">
	<table id="table">  
		<thead>
	    <tr>		      
	      <th width="80px">ID</th>
	      <th width="210px">缩略图</th>
	      <th width="180px" id="NAME" class="icansort"><span style="cursor: pointer;color:#07519a;" id="NAME_SPAN">名称</span></th>		      
	      <th width="400px"><span>描述</span></th>
	      <th width="120px">上传时间</th>	
	      <th style="border: none; background-color: #fff;"></th>	           	  
	    </tr>	    
	    </thead>
	  <tbody class="scrollContent">
	  	<c:forEach items="${images}" var="image" varStatus="rowCounter">		  		
	        <c:choose>
	          <c:when test="${rowCounter.count % 2 == 0}">
	            <c:set var="rowStyle" value="odd" />
	          </c:when>
	          <c:otherwise>
	            <c:set var="rowStyle" value="even" />
	          </c:otherwise> 
	        </c:choose>
	        <tr class="${rowStyle}" id="tr_${image.id}" onclick="select(this)" style="cursor:pointer">			          	
	          	<td width="80px"><c:out value="${image.id}" /></td>
	          	<td width="210px">
	          	<img alt="" src="${path}/${image.name}"  style="max-width: 200px; max-height: 100px;" onclick="preview(this)" />
	          	</td>
	          	<td width="180px"><c:out value="${image.name}" /></td>
	          	<td width="400px"><c:out value="${image.description}" /></td>
	          	<td width="120px"><c:out value="${image.uploadTime}" /></td>				               												
	        </tr>		
		</c:forEach>				 
	  </tbody>
	</table>
  </div>      
</div>
	
<div class="south">
	<c:choose>
	  <c:when test="${total > 0}">
	    <tags:pagination current="${current}" totalPages="${totalPages}" pageSize="${pageSize}"/>
	  </c:when>
	  <c:otherwise>
	    <span style="display:block; margin-left: 20px; padding-top: 15px;">查无记录！</span>
	  </c:otherwise> 
	</c:choose>
</div>

<div id="registerbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">图片预览</span>        
        <a href="javascript:registerClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">        
        <div class="popup_box_content" id="registerContainer">
    		        	 
        </div>               
    </div>
</div><!-- end of registerbox -->

</body>
</html>