<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Header管理 </title>
<script type="text/javascript" src="${ctx}/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${ctx}/static/ueditor/lang/zh-cn/zh-cn.js"></script>
<style type="text/css">
        div{
            width:100%;
        }
		
		
</style>
</head>
<body>
<div>    
    <script id="editor" type="text/plain" style="width:1024px;height:400px;"></script>
</div>


<script type="text/javascript">
//var ue = UE.getEditor('editor');
UE.getEditor('editor',{
            //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个
            toolbars:[['FullScreen', 'Source', 'Undo', 'Redo','Bold','test']],
            //focus时自动清空初始化时的内容
            autoClearinitialContent:true,
            //关闭字数统计
            wordCount:false,
            //关闭elementPath
            elementPathEnabled:false,
            //默认的编辑区域高度
            initialFrameHeight:300
            //更多其他参数，请参考ueditor.config.js中的配置项
            //serverUrl: '/server/ueditor/controller.php'
        });

/* var ue = UE.getEditor('editor', {
	toolbars: [
		[]
	],
	sourceEditorFirst: 'true',
	autoHeightEnabled: true,
	autoFloatEnabled: true
}); */
</script>
</body>
</html>