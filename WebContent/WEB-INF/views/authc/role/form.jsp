<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>角色编辑</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-validation-1.14.0/jquery.validate.min.js"></script>

<script type="text/javascript">
function forback(){	
	window.location.replace("${ctx}/authc/role/list");
	e.preventDefault();
	return false;
}

$(document).ready(function(){		
	var url = "${ctx}/authc/role/checkRoleUnique";
	var roleId = document.getElementById("id").value;
	if(roleId != ""){
		url = "${ctx}/authc/role/checkRoleUniqueById/" + roleId; 
	}
	
	$("#form").validate({		 
		rules: {			
			role: {
				required: true,
				remote: url
			}
	  	},
	  	messages: {	  		
	  		role: {				
	  			required: "请输入角色名称",
	  			remote: "角色名称已经存在"
			}   
	  	},	
		errorPlacement: function(error, element) {			
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});	
	
});
</script>
</head>
<body>		
<form:form id="form" modelAttribute="role" method="post" action="${ctx}/authc/role/${action}">
	<form:hidden path="id" />    
	
	<h1></h1>
	<table class="table">        
    <tr class="even">   
      <td style="text-align: right;width: 130px;"><form:label path="role">*角色名称:</form:label></td>
      <td style="width: 160px;"><form:input path="role" style="float:left;" /></td>
      <td class="status"><form:errors path="role" cssClass="errors" element="div"/></td>     
    </tr>        
    	
	<tr class="odd">  
		<td style="text-align: right;width: 130px;"><span style="padding-right: 10px;"><form:label path="description">角色描述:</form:label></span></td>
	    <td style="width: 160px;"><form:input path="description" style="float:left;" /></td>
	    <td class="status"><form:errors path="description" cssClass="errors" element="div"/></td>
	</tr>	
	       
	<tr class="even">      
      <td colspan="3" style="text-align:center;">      
      	<input class="btn" type="submit" value="提 交" />      	
      	<input class="btn" type="button" value="返回" onclick="forback();" />           
      </td>      
    </tr>
  	</table>	 		
</form:form>
</body>
</html>