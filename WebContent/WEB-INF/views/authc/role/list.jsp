<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>角色管理</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/layout.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/table.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/pagination.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/toolbar.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/form.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/static/jquery.msgBox/styles/msgBoxLight.css" />

<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery.msgBox/scripts/jquery.msgBox.js"></script>

<script type="text/javascript">
var selectedRow = null;

function select(thisRow){	
    $('#table tbody tr').not(this).removeClass('clicked');
    $(thisRow).toggleClass('clicked');
    selectedRow = $(thisRow);
}

function query(){	
	var form = $("#form");
	form.attr("action","${ctx}/authc/role/list");
	//form.attr("method","get");
	form.submit();
    //return true;	
}

function create(){
	var form = $("#form");
	form.attr("action","${ctx}/authc/role/create");
	form.attr("method","get");
	form.submit();
            	
	return false;
}

function query(){	
	var form = $("#form");
	form.attr("action","${ctx}/authc/role/list");
	form.attr("method","post");
	form.submit();
}

function update(){
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法编辑！",
		    type:"info"
		});
		return false;
	}
	var roleId =  selectedRow.attr('id').replace('tr_','');	
	var form = $("#form");
	form.attr("action","${ctx}/authc/role/update/"+roleId);
	form.attr("method","get");
	form.submit();
            	
	return false;
}

function deleted(){		
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法删除！",
		    type:"info"
		});
		return false;
	}
	var roleId =  selectedRow.attr("id").replace("tr_", "");
	$.msgBox({
        title: "确认删除提示",
        content: "确实要删除该条记录吗？",        
        type: "confirm",        
        buttons: [{ value: "确认" }, { value: "取消" }],            
        success: function (result) {
            if (result == "确认") {
            	var form = $("#form");
            	form.attr("action","${ctx}/authc/role/delete/"+roleId);
            	form.attr("method","get");
            	form.submit();
            		
            	return false;
            }
        }
    });	
}




function showResourcesByRoleId(){	
	if(selectedRow==null){
		$.msgBox({
		    title:"删除提示：",
		    content:"未选择任何记录，无法编辑！",
		    type:"info"
		});
		return false;
	}
	var roleId =  selectedRow.attr('id').replace('tr_','');	
	var form = $("#form");
	form.attr("action","${ctx}/authc/role/getResourcesRoleId/"+roleId);
	form.attr("method","get");
	form.submit(); 
            	
	return false;
}


$(document).ready(function() {
	msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";		
	
	$("#table tbody tr").hover(function() {   
        $(this).addClass("hover"); 
    }, function() {   
        $(this).removeClass("hover"); 
    });
    
	var sortName = $("#sortName").val();
	var sortOrder = $("#sortOrder").val();		
	var row0 = $('#table thead tr').get()[0];
	var cell0 = row0.cells[0];		
	
	if(sortName=="ROLE"){
		if(sortOrder=="ASC"){
			$("#ROLE_SPAN").css("background-image", "url(${ctx}/static/images/up-arrow.png)");
			cell0.setAttribute("class", "ASC");			
		}else{
			$("#ROLE_SPAN").css("background-image", "url(${ctx}/static/images/down-arrow.png)");
			cell0.setAttribute("class", "DESC");							
		}		
	}	
	
	$("#table thead tr th").click(function(event){		    
		var thid = $(this).attr("id");			
		
		if(thid != null){
			$("#sortName").val(thid);						
			
			if($(this).hasClass("ASC")){				
				$("#sortOrder").val("DESC");					
			}else{
				$("#sortOrder").val("ASC");					
			}		
			
			$("#form").submit();
		}		
		
		return false;		
	}); 	
	
	$("#pageSizeSelect").change(function() {		
		$("#pageSize").val($(this).val());				
		$("#form").submit();
		return false;		
    });	
});
</script>

</head>
<body>
<c:if test="${not empty message}">			
<script type="text/javascript">			
msgBoxImagePath = "${ctx}/static/jquery.msgBox/images/";		
$.msgBox({
    title:"信息提示：",
    content:"${message}",
    autoClose:true,	
    timeOut: 2000,
    type:"info"
});
</script>
</c:if>

<div class="north_form">	
	<form class="form_horizontal" id="form">
		<div class="hvcenter">
			<label for="role">角色名称:</label>
			<input type="text" id="role" name="role" class="q-input" value="${param.role}" />							
		
			<input type="hidden" name="pageSize" id="pageSize" value="10" /> 	
			<input type="hidden" name="pageIndex" id="pageIndex" value="1" />
			
			<input type="hidden" name="sortName" value="${param.sortName}" id="sortName" />
			<input type="hidden" name="sortOrder" value="${param.sortOrder}" id="sortOrder" />					
			
			<input type="button" value="查询" class="btn" style="margin-left: 20px;" onclick="query()" />
		</div>										 							  
	</form>
</div>

<div class="north_toolbar">
	<div class="toolbar">
		<ul>			
		<shiro:hasPermission name="role:create">
        	<li onclick="create()"><span><i class="tool_icon add_icon"></i><i class="tool_label">添加</i></span></li>
        </shiro:hasPermission>
        <shiro:hasPermission name="role:update">
        	<li onclick="update()"><span><i class="tool_icon edit_icon"></i><i class="tool_label">编辑</i></span></li>
        </shiro:hasPermission>
        <shiro:hasPermission name="role:delete">
        	<li onclick="deleted()"><span><i class="tool_icon delete_icon"></i><i class="tool_label">删除</i></span></li>	
        </shiro:hasPermission>
        <shiro:hasPermission name="role:view">
        	<li onclick="showResourcesByRoleId();"><span><i class="tool_icon edit_icon"></i><i class="tool_label">查看/分配角色权限</i></span></li>
        </shiro:hasPermission>		
		</ul>
	</div>
</div>
  
<div class="center">
  <div id="wrapper">
	<table id="table">  
		<thead>
	    <tr>		      
	      <th width="120px" id="ROLE" class="icansort"><span style="cursor: pointer;color:#07519a;" id="ROLE_SPAN">角色名称</span></th>		      
	      <th width="200px"><span>角色描述</span></th>	
	      <th style="border: none; background-color: #fff;"></th>	           	  
	    </tr>	    
	    </thead>
	  <tbody class="scrollContent">
	  	<c:forEach items="${roles}" var="role" varStatus="rowCounter">		  		
	        <c:choose>
	          <c:when test="${rowCounter.count % 2 == 0}">
	            <c:set var="rowStyle" value="odd" />
	          </c:when>
	          <c:otherwise>
	            <c:set var="rowStyle" value="even" />
	          </c:otherwise> 
	        </c:choose>
	        <tr class="${rowStyle}" id="tr_${role.id}" onclick="select(this)" style="cursor:pointer">			          	
	          	<td width="120px"><c:out value="${role.role}" /></td>
	          	<td width="200px"><c:out value="${role.description}" /></td>				               												
	        </tr>		
		</c:forEach>				 
	  </tbody>
	</table>
  </div>        
</div>
	
<div class="south">
	<c:choose>
	  <c:when test="${total > 0}">
	    <tags:pagination current="${current}" totalPages="${totalPages}" pageSize="${pageSize}"/>
	  </c:when>
	  <c:otherwise>
	    <span style="display:block; margin-left: 20px; padding-top: 15px;">查无记录!</span>
	  </c:otherwise> 
	</c:choose>
</div>
</body>
</html>