<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title>查看/分配角色权限</title>
<link type="text/css" rel="stylesheet" href="${ctx}/static/ztree/css/zTreeStyle/zTreeStyle.css">	
<script type="text/javascript" src="${ctx}/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="${ctx}/static/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${ctx}/static/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/static/css/button.css" />

</head>
<body>

<form action="${ctx}/authc/role/updateRoleResourceByRoleId" method="post">
	<ul id="ztree" class="ztree"></ul>
	<input type="hidden" id="roleId" name="roleId" />
	<input type="hidden" id="resourceIds" name="resourceIds" />
	<br/><br/>
	<input class="btn" type="submit" value="提 交" /> 
	<input class="btn" type="button" value="返回" onclick="forback();" />
	<br/><br/>
</form>    	
    	
<script type="text/javascript">
var setting = {
	check: {
		enable: true
	},	
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		onCheck: onCheck
	} 
};


$(document).ready( function() {		
	var roleId = '${roleId}';
	$("#roleId").val(roleId);
	
	$.ajax( {
		url : "${ctx}/authc/resource/getResourcesJsonByRoleId/"+roleId,	
		type : "post",
		dataType : "json",
		contentType : "application/json",
		cache : false,
		async: false,
		success:function(data){			
			$.fn.zTree.init($("#ztree"), setting, data);
			count();
		}
	});		
});


function onCheck(e, treeId, treeNode) {
	count();
} 

function count() {
	var zTree = $.fn.zTree.getZTreeObj("ztree");	
    var nodes=zTree.getCheckedNodes(true);
    
    var resourceIdArray = new Array();
    for(var i=0;i<nodes.length;i++){
    	var isParent = nodes[i].isParent;
    	if(!isParent){    		
    		resourceIdArray.push(nodes[i].id);
    	}    	
    }
    
    var resourceIds="";
    for(var i=0;i<resourceIdArray.length;i++){
    	resourceIds += resourceIdArray[i];
    	if(i<resourceIdArray.length-1){
    		resourceIds += ",";
    	}
    }
    //alert(resourceIds); 
    $("#resourceIds").val(resourceIds);
}

function forback(){	
	window.location.replace("${ctx}/authc/role/list");
	e.preventDefault();
	return false;
}
</script>
</body>
</html>