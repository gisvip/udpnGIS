<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>武汉大学</title>
<link type="image/x-icon" rel="shortcut icon" href="/link/static/images/favicon.ico">
<link rel="stylesheet" href="/link/static/grid/col.css" media="all">
<link rel="stylesheet" href="/link/static/grid/2cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/3cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/4cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/5cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/6cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/7cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/8cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/9cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/10cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/11cols.css" media="all">
<link rel="stylesheet" href="/link/static/grid/12cols.css" media="all">

<link type="text/css" rel="stylesheet" href="/link/static/flexSlider/flexslider.css">
<link type="text/css" rel="stylesheet" href="/link/static/backtotop/css/style.css">

<link type="text/css" rel="stylesheet" href="/link/static/css/bpopup.css" />
<link type="text/css" rel="stylesheet" href="/link/static/css/index_wuda.css">

<style type="text/css">

/* ------------------------------------------------------------------------------------------- */
.header_bar_nav a {
    text-decoration: none;
    
    color: rgb(191, 37, 1) !important;
        font-size: 14px;
    font-family: "微软雅黑";
    
    background: url(/link/static/images/listdash.png) 0px 5px no-repeat;
    
}

.header_bar_nav li:hover  {
    background-color: #e8f3fb;
}

.header_bar_nav li li:hover  {
    background-color: #DAE5EE;
}


/* ------------------------------------------------------------------------------------------- */


</style>


</head>
<body>
<input type="hidden" id="authenticated" value="true" />
<!-- ******************************************************************************************** -->
<div class="header_bar">
<div class="col_1_of_1">
	<div class="bar_left">
		<a href="http://mall.360.com/#" onclick="return false">收藏360商城</a>
	</div>
	<div class="bar_right">
	<div class="header_bar_nav">
		<ul>
		    
			  <li><a href="javascript:;">我的订单</a></li>
			<li><a href="javascript:;">手机360商城</a></li>
			<li><a href="javascript:;">企业采购</a></li>
			<li><a href="javascript:;">帮助中心</a></li>        
		</ul>
	</div>
	</div>
</div>
</div>
<!-- ******************************************************************************************** -->
<div class="header_main">
	<div class="col_1_of_1">
		<div class="main_left">
			<div class="header_title">
	<a class="header-logo" href="/shop/index"><img alt="logo" src="/link/static/images/logo_wuda.png" /></a>
		
</div>

		</div>
		<div class="main_right">	
			
			<div class="header_bar_nav">
			<ul>
			    <li class="i_dropdown" id="navigation">
			            <a href="#">学校概况<em></em></a>
			            <ul class="i_sub_menu" id="navi_sub_menu">
			                <li><a href="#">个人中心</a></li>
			                <li><a href="#">我的试用</a></li>
			                <li><a href="#">试用积分</a></li>                
			                <li><a href="#">收货地址</a></li>
			                <li><a href="#">账户设置</a></li>
			                <li><a href="javascript:;" onclick="logout()">退出登录</a></li>
			            </ul>
			    	</li>
				  <li><a href="javascript:;">机构设置</a></li>
				<li><a href="javascript:;">师资队伍</a></li>
				<li><a href="javascript:;">人才培养</a></li>
				<li><a href="javascript:;">科学研究</a></li> 
				<li><a href="javascript:;">社会服务</a></li> 
				<li><a href="javascript:;">交流合作</a></li>
				<li><a href="javascript:;">文化生活</a></li>  
				    
			</ul>
		</div>
		
		</div>
	</div>
</div>
<!-- ******************************************************************************************** -->
<div class="flexslider" id="flexslider_wrapper">
<ul class="slides" id="slides_wrapper">
	<li id="slide-6" style="background: url(/link/static/banner/2.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-3" style="background: url(/link/static/banner/3.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-4" style="background: url(/link/static/banner/4.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-5" style="background: url(/link/static/banner/5.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-10" style="background: url(/link/static/banner/10.png) 50% 0 no-repeat;"></li>
	<li id="slide-11" style="background: url(/link/static/banner/11.jpg) 50% 0 no-repeat;"></li>
	<li id="slide-14" style="background: url(/link/static/banner/12.jpg) 50% 0 no-repeat;"></li>
</ul>
</div>
<!-- ******************************************************************************************** -->
<div class="col_1_of_1">
	<h2 class="mod-titles">明星单品 <i>Fashionate Ones</i></h2>
	<div class="produ_row_content">
		<div class="produ_row_ul_content_0 float_left">
			<a href="http://pop.jumei.com/dress_sport?from=all_null_index_floor_cloth_chanel&amp;lo=3222&amp;mat=21618" target="_blank">
				<img src="/link/static/shop/pl.jpg">
			</a>
		</div>
		<div class="float_right">
			<ul class="produ_row_ul_content_1">
				<li>
					<a	href="http://pop.jumei.com/promotion/40146.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42608"	target="_blank"> 
						<img width="490" height="240" src="/link/static/shop/p1.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_2">
				<li>
					<a	href="http://pop.jumei.com/promotion/40078.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42609"	target="_blank"> 
						<img width="240" height="240" src="/link/static/shop/p2.jpg">

				</a></li>
			</ul>
			<ul class="produ_row_ul_content_3">
				<li>
					<a	href="http://pop.jumei.com/promotion/41034.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42610"	target="_blank"> 
						<img width="240" height="240" src="/link/static/shop/p3.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_4">
				<li>
					<a	href="http://pop.jumei.com/promotion/39944.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42607"	target="_blank"> 
						<img width="490" height="240" src="/link/static/shop/p4.jpg">
					</a>
				</li>
			</ul>
		</div>
	</div>	
</div>
<!-- end of product list 1 -->
<div class="col_1_of_1">
	<h2 class="mod-titles">新品首发 <i>New Arrivals</i></h2>
	<div class="produ_row_content">
		<div class="produ_row_ul_content_0 float_right2">
			<a href="http://pop.jumei.com/dress_sport?from=all_null_index_floor_cloth_chanel&amp;lo=3222&amp;mat=21618" target="_blank">
				<img src="/link/static/shop/pl.jpg">
			</a>
		</div>
		<div class="float_left2">
			<ul class="produ_row_ul_content_1">
				<li>
					<a	href="http://pop.jumei.com/promotion/40146.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42608"	target="_blank"> 
						<img width="490" height="240" src="/link/static/shop/p1.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_2">
				<li>
					<a	href="http://pop.jumei.com/promotion/40078.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42609"	target="_blank"> 
						<img width="240" height="240" src="/link/static/shop/p2.jpg">

				</a></li>
			</ul>
			<ul class="produ_row_ul_content_3">
				<li>
					<a	href="http://pop.jumei.com/promotion/41034.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42610"	target="_blank"> 
						<img width="240" height="240" src="/link/static/shop/p3.jpg">
					</a>
				</li>
			</ul>
			<ul class="produ_row_ul_content_4">
				<li>
					<a	href="http://pop.jumei.com/promotion/39944.html?from=all_null_index_floor_cloth_activity&amp;lo=3223&amp;mat=42607"	target="_blank"> 
						<img width="490" height="240" src="/link/static/shop/p4.jpg">
					</a>
				</li>
			</ul>
		</div>
	</div>		
</div>
<!-- end of product list 2 -->
<div class="col_1_of_1">
<h2 class="mod-titles">社区动态 <i>Community News</i></h2>
	<div class="section group">        
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/eyelet-curtains/"><img src="/link/static/shop/h-portal1.jpg" alt="Eyelet Curtains"></a>       	    	
		</div>
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/contemporary-curtains/"><img src="/link/static/shop/h-portal2.jpg" alt="Pencil Pleat Curtains"></a>                
		</div>
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/duvet-covers/"><img src="/link/static/shop/h-portal3.jpg" alt="Duvets Sets"></a>                
		</div>
        <div class="col span_2_of_8">
        	<a href="http://www.linen4less.co.uk/venetian-blinds/"><img src="/link/static/shop/h-portal4.jpg" alt="Venetian Blinds"></a>
		</div>    
	</div>
</div>
<!-- end of product list 3 -->
<div class="col_1_of_1">	
    <div class="section group">
        <div class="col span_2_of_6">
        	<a href="http://www.linen4less.co.uk/eyelet-curtains/"><img src="/link/static/shop/h-portal1.jpg" alt="Eyelet Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
        	<a href="http://www.linen4less.co.uk/contemporary-curtains/"><img src="/link/static/shop/h-portal2.jpg" alt="Pencil Pleat Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
        	<a href="http://www.linen4less.co.uk/duvet-covers/"><img src="/link/static/shop/h-portal3.jpg" alt="Duvets Sets"></a>               
		</div>       
	</div>
</div>
<!-- end of product list 4 -->
<textarea rows="30" cols="200">
<div class="col_1_of_1">
	<div class="section group">        
    	<div class="col span_2_of_6">
           	<a href="http://www.linen4less.co.uk/eyelet-curtains/"><img src="/link/static/shop/web1.jpg" alt="Eyelet Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
           	<a href="http://www.linen4less.co.uk/contemporary-curtains/"><img src="/link/static/shop/web3.jpg" alt="Pencil Pleat Curtains"></a>               
		</div>
        <div class="col span_2_of_6">
           	<a href="http://www.linen4less.co.uk/duvet-covers/"><img src="/link/static/shop/web2.jpg" alt="Duvets Sets"></a>               
		</div>            
   	</div>
</div>
</textarea>
<!-- end of product list 5 -->
<div class="col_1_of_1">
<h2 class="mod-titles">免费试用 <i>Get-to-Use Freely</i></h2>
	<div class="section group">
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/teachers/"><img src="/link/static/shop/h-portal-parents.jpg" alt="Teachers">		        
	        </a>
	    </div>
	
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/parents/"><img src="/link/static/shop/h-portal-teachers.jpg" alt="Parents">		        
	        </a>
	    </div>
	
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/tutors/"><img src="/link/static/shop/h-portal-tutors.jpg" alt="Tutors">		        
	        </a>
	    </div>
	    
	    <div class="col span_2_of_8">
	        <a href="http://www.schofieldandsims.co.uk/teachers/"><img src="/link/static/shop/h-portal-parents.jpg" alt="Teachers">
	        </a>
	    </div>
	</div>
</div>
<!-- end of product list 6 -->
<!-- ******************************************************************************************** -->

<div style="display: block;	width: 100%;">
<div class="footer_top">
	<div class="col_1_of_1">
		<div class="section group">
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item" style="border-right: solid 1px #444;"><i class="icon1"></i>7天无理由退货</div>
		    </div>
		
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item" style="border-right: solid 1px #444;"><i class="icon2"></i>15天免费换货</div>		        
		    </div>
		
		    <div class="col span_2_of_6">
		        <div class="footer_banner_item"><i class="icon3"></i>满99元包邮<span style="font-size:12px">(特殊商品除外)</span></div>
		    </div>		    
		</div>
	</div>
</div>

<div class="footer_middle">
	<div class="col_1_of_1">
		<div class="section group">
		    <div class="col span_2_of_10">		        
				<span class="header">新手入门</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">新用户注册</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">用户登录</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">找回密码</a></li>
				</ul>
		    </div>
		
		    <div class="col span_2_of_10">
		        <span class="header">购物指南</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">购买流程</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">支付方式</a></li>
					<li><a href="#" rel="nofollow">配送说明</a></li>					
				</ul>		        
		    </div>
		
		    <div class="col span_2_of_10">
		        <span class="header">售后服务</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">服务条款</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">七日退货</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">十五日换货</a></li>
				</ul>
		    </div>	
		    		    
		    <div class="col span_2_of_10">
		        <span class="header">免费试用</span>
				<ul> 
					<li><a href="http://mall.360.com/help/newuser#d1" rel="nofollow">试用流程</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d2" rel="nofollow">查看申请结果</a> </li>
					<li><a href="http://mall.360.com/help/newuser#d3" rel="nofollow">提交报告</a></li>
				</ul>
		    </div>
		    
		    <div class="col span_2_of_10">
		        <span class="header">联系我们</span>
				<ul> 
					<li class="call-number">400-6822-360 </li>
					<li class="call-time">周一到周日9:00-18:00</li>
				</ul>
		    </div>	    
		</div>
	</div>
</div>
<div class="footer_copyright">360商城©2013-2015 深圳奇虎健安智能科技有限公司版权所有｜粤ICP备14094849号-1</div>
</div>
<!-- ******************************************************************************************** -->
<a href="javascript:;" class="cd-top">Top</a>
<!-- ******************************************************************************************** -->
<div id="loginbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">用户登录</span>        
        <a href="javascript:loginClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">  
    	<form id="loginForm">    
        <div class="popup_box_content">               	 
    		 <div style="width: 420px;">
	    		 <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="telephone">电话号码:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="telephone" name="telephone" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_loginName_error_div"></div>			        			        
			    </div>			    
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col" style="width:80px; text-align: right; padding-right: 10px;"><label for="password">密码:<strong>*</strong></label></div>
			        <div class="ungrid_col popup_col" style="width:200px; text-align: left;"><input type="text" id="password" name="password" /></div>
			        <div class="ungrid_col popup_col" style="width:140px; text-align: left;" id="login_password_error_div"></div>		        		        
			    </div>
			    
			    <div class="ungrid_row">
			        <div class="ungrid_col popup_col">
			        	<div id="login_loginName_msg_div" style="color: red; display: inline;"></div>
			        	<div id="login_password_msg_div" style="color: red; display: inline;"></div>							
			        </div>
			        		        		        
			    </div>
			    
    		</div>        	 
        </div>        
        <div class="popup_box_footer">					
			<input type="button" class="popup_box_btn popup_box_btn_gray" value="登 录" onclick="loginPost()" />							
		</div>
		</form>		        
    </div>
</div>

<div id="registerbox" class="popup_box" >
    <div class="popup_box_head">
        <span class="popup_box_head_title">新用户注册</span>        
        <a href="javascript:registerClose();" class="popup_box_head_close"></a>
    </div>
    <div class="popup_box_container">        
        <div class="popup_box_content" id="registerContainer">
    		        	 
        </div>               
    </div>
</div><!-- end of registerbox -->
<!-- ******************************************************************************************** -->
<script type="text/javascript" src="/link/static/jquery/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="/link/static/jquery-validation-1.14.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="/link/static/bpopup/jquery.bpopup.min.js"></script>
<script type="text/javascript" src="/link/static/flexSlider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="/link/static/backtotop/js/backtotop.js"></script>

<script type="text/javascript">
//你好！请登录|注册
//我的订单
//我的消息
var numberItem = 0;

$(document).ready(function(){	
	$("#flexslider_wrapper").flexslider({
		animation: "slide",
		directionNav: true
	});
	
	$('#i_dropdown').hover(
    	function(){
    		$(this).children('#i_sub_menu').slideDown(200);
		},
        function(){
        	$(this).children('#i_sub_menu').slideUp(200);
		}
	);
	
		$('#navigation').hover(
	    	function(){
	    		$(this).children('#navi_sub_menu').slideDown(200);
			},
	        function(){
	        	$(this).children('#navi_sub_menu').slideUp(200);
			}
		);
	
	$('#i_cart').hover(
		function(){			
			$(this).children('#i_cart_popup').slideDown(200);
		},
	    function(){			
			$(this).children('#i_cart_popup').slideUp(200);
		}
	);
	
	$("#loginForm").validate({		 
		rules: {			
			
			password: {
				minlength: 6,
				maxlength: 20,
				required: true
			}
	  	},
	  	messages: {	  		
	  		
			password: {				
	  			minlength: "密码至少是6个字符",
	  			maxlength: "密码最多为20个字符",
	  			required: "请输入密码"
			}    
	  	},	
		errorPlacement: function(error, element) {					
			error.appendTo( element.parent().next() );				
		},
		highlight: function(element, errorClass) {
			$(element).parent().next().find("." + errorClass).removeClass("checked");
		}	  
	});	
	
	/* $('#myli').hover(
			function(){			
				$(this).children('#mysub').slideDown(200);
			}
	); */
	
	var top = $('#mysub3').position().top;
	alert(top);
	
	
});



//正在加载购物车...
//我的购物车
//您的购物车还是空的，赶紧行动吧！马上去购物
//请登录后查看你的购物车
//你的购物车还没有商品，赶紧去选购吧!

//商品信息	单价	数量	小计	操作
//商品总计
//继续购物	去结算





function addToMyCart(productItem){	
	//alert(productItem);
	//todo ajax 将购物车写入数据库，返回是否成功失败bool
	var pid = productItem.attr("pid");
	if(pid == null || pid == "undefined"){
		return false;
    }else{
    	var pcategory = productItem.attr("pcategory");
    	alert(pcategory);
    	
    	return true;
    	
    	
    }
}


function addToCart(i,qty){
	
}

function addCartItemDisplay(objProd,Quantity){
	
}


function loginShow(){
	$('#loginbox').bPopup({
	 	modal: false,
	 	opacity: 0.6,
	 	modalClose: false, 
        speed: 500,
        transition: 'slideDown'        
 	});
}

function loginClose(){		
	$("#login_loginName_msg_div").empty();
	$("#login_password_msg_div").empty(); 
	$("#login_loginName_error_div").empty();
	$("#login_password_error_div").empty();	
	
	closeEventFlag = true;
	$("#loginbox").bPopup().close(); 	
}

function loginPost(){		
	var form = $("#loginForm");
	form.attr("action","/link/index/login");
	form.attr("method","post");
	form.submit();
            	
	return false;
}

function logout(){
	var form = $("#loginForm");
	form.attr("action","/link/index/logout");
	form.attr("method","post");
	form.submit();
    return false; 
}

function registerShow(){	
	$("#registerbox").bPopup({
		modal: false,
		modalClose: false, 
	    speed: 450,
	    transition: 'slideDown',
		content: "iframe", 
		contentContainer: "#registerContainer",
		loadUrl:"/link/customer/register"		
	});
}


function registerClose(){		
	closeEventFlag = true;
	$("#registerContainer").empty();
	$("#registerbox").bPopup().close(); 	
}


</script>


</body>
</html>