<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ tag trimDirectiveWhitespaces="true"%>
<%@ attribute name="current" type="java.lang.Integer" required="true"%>
<%@ attribute name="pageSize" type="java.lang.Integer" required="true"%>
<%@ attribute name="totalPages" type="java.lang.Integer" required="true"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
int begin = Math.max(1, current - pageSize/2);
int end = Math.min(begin + (pageSize - 1), totalPages);
request.setAttribute("current", current);
request.setAttribute("begin", begin);
request.setAttribute("end", end);
%>
<div class="pagingcenter">
<div style="float: left;font-size: 14px;padding-top: 6px; ">
	<span>当前第${current}/${totalPages}页</span>
	<span class="separator"></span>
	<label for="pageSizeSelect">每页:</label>
	<select id="pageSizeSelect" name="pageSizeSelect">	
	<option value="10" <c:if test="${param.pageSize == '10'}">selected="selected"</c:if>>10</option>
	<option value="20" <c:if test="${param.pageSize == '20'}">selected="selected"</c:if>>20</option>
	<option value="30" <c:if test="${param.pageSize == '30'}">selected="selected"</c:if>>30</option>
	<option value="40" <c:if test="${param.pageSize == '40'}">selected="selected"</c:if>>40</option>
	<option value="50" <c:if test="${param.pageSize == '50'}">selected="selected"</c:if>>50</option>
	<option value="100" <c:if test="${param.pageSize == '100'}">selected="selected"</c:if>>100</option>	
	</select>	
	<span>条</span>	
	<span class="separator"></span>
</div>
<div style="float: left; margin-left: 10px;margin-right: 10px;height: 32px;">
	<ul id="leftmenu" class="pagination paginationB paginationB08">
		 <% if ( current - 1 >= 1){%>
               	<li><a class="first" href="javascript:void(0)" onclick="pagination(${pageSize},1);">&lt;&lt;</a></li>
                <li><a class="previous" href="javascript:void(0)" onclick="pagination(${pageSize},${current-1});">&lt;</a></li>
         <%}else{%>
                <li class="disabled"><a href="javascript:return false;">&lt;&lt;</a></li>
                <li class="disabled"><a href="javascript:return false;">&lt;</a></li>
         <%}  %> 
		<c:forEach var="i" begin="${begin}" end="${end}">
            <c:choose>
                <c:when test="${i == current}">
                    <li><a class="current" href="javascript:void(0)" onclick="pagination(${pageSize},${i});">${i}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="javascript:void(0)" onclick="pagination(${pageSize},${i});">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>	  
	  	 <% if ( current + 1 <= totalPages ){%>
               	<li><a class="next" href="javascript:void(0)" onclick="pagination(${pageSize},${current+1});">&gt;</a></li>
                <li><a class="last" href="javascript:void(0)" onclick="pagination(${pageSize},${totalPages});">&gt;&gt;</a></li>
         <%}else{%>
                <li class="disabled"><a href="javascript:return false;">&gt;</a></li>
                <li class="disabled"><a href="javascript:return false;">&gt;&gt;</a></li>
         <%} %>
	</ul>	
</div>
<div style="float: left;font-size: 14px;padding-top: 6px;">	
	<span class="separator"></span>
	<span id="refresh_span"><img alt="" src="${ctx}/static/images/load.png" /></span><span>显示${from}到${to}记录</span>
	<span class="separator"></span>
	<span>总共${total}条记录</span>	
</div>
<script type="text/javascript">
function pagination(pageSize, pageIndex){	
	document.getElementById("pageSize").value = pageSize;
	document.getElementById("pageIndex").value = pageIndex;
	query();
	return false;		
}
</script>
</div>