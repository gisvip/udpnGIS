package com.chuanqi.shop.filter;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.chuanqi.shop.domain.CustomerRegister;
import com.chuanqi.shop.constants.Constants;
import com.chuanqi.shop.dto.Account;
import com.chuanqi.shop.dto.ShopHttpSession;
import com.chuanqi.shop.service.CustomerRegisterService;
import com.chuanqi.shop.service.CartService;

public class ShoppingAuthenticationFilter {
	public static String loginPost(CustomerRegisterService customerLoginService, CartService shoppingCartService, HttpServletRequest request, RedirectAttributes redirectAttributes, String redirect) {     	
    	String telephone = request.getParameter("telephone");
        String password = request.getParameter("password");
        
        System.out.println(telephone);
        System.out.println(password); 
        
        CustomerRegister customerRegister = new CustomerRegister();
        customerRegister.setTelephone(telephone);
        customerRegister.setPassword(password);
        
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();        
        Set<ConstraintViolation<CustomerRegister>> constraintViolations = validator.validate( customerRegister );
        if(constraintViolations.size()>0){        	
        	System.out.println("*error*");
        	String loginNameErrorMessage = validator.validateProperty( customerRegister, "loginName" ).iterator().next().getMessage();
        	redirectAttributes.addFlashAttribute("loginNameErrorMessage", loginNameErrorMessage);
        	
        	String passwordErrorMessage = validator.validateProperty( customerRegister, "password" ).iterator().next().getMessage();        	
        	redirectAttributes.addFlashAttribute("passwordErrorMessage", passwordErrorMessage); 
        	return redirect;
        }                
        
        CustomerRegister iLogin = customerLoginService.getByTelephone(telephone);
        
        if(iLogin != null){
        	System.out.println(iLogin.getTelephone());
            System.out.println(iLogin.getSalt());
            
            String algorithmName = "md5";
        	int hashIterations = 2;        	
        	String encryptPassword = new SimpleHash(algorithmName, password, ByteSource.Util.bytes(iLogin.getSalt()), hashIterations).toHex();
            
        	if(encryptPassword.equals(iLogin.getPassword())){    	           
            	System.out.println("i have login in");
            	Account account = new Account();
            	account.setCustomerId(customerRegister.getId());
            	account.setCustomerName(customerRegister.getTelephone());           	
            	
            	ShopHttpSession shopHttpSession = new ShopHttpSession();      
            	shopHttpSession.setAccount(account);           	         	
            	
                HttpSession session = request.getSession(true);  	
                session.setAttribute(Constants.SHOP_HTTP_SESSION, shopHttpSession);
                
                //todo 登录记录要入库
                
            }else{
            	System.out.println("**密码不对!**");
            	redirectAttributes.addFlashAttribute("passwordErrorMessage", "密码不对!");		
            }
        }else{
        	System.out.println("***用户名不对!***");        	     
        	redirectAttributes.addFlashAttribute("loginNameErrorMessage", "用户名不对!");
        }        
          
        return redirect;
    }    
    
	//todo 登录记录要入库
	
	
	
	
    public static String logoutPost(HttpServletRequest request, String redirect) {    	
    	HttpSession session = request.getSession(false); 	
		if ( session != null && session.getAttribute(Constants.SHOP_HTTP_SESSION) != null ) {
			session.invalidate();			
		}
		return redirect;
    }    
}
