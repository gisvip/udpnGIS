package com.chuanqi.shop.domain;

import java.io.Serializable;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

public class CustomerRegister implements Serializable {
	private static final long serialVersionUID = 1L;    

	private Long id;	

	private String telephone;

	@NotEmpty(message = "密码不能为空。")	
	@Size(min = 6, max = 20, message = "密码的长度应该在6至20位之间。")
    private String password; 
    
    private String salt;   

	private Boolean locked = Boolean.FALSE;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
}
