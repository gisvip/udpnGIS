package com.chuanqi.shop.dto;

import java.io.Serializable;

public class ShoppingCart implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int numberOfItems;
	private double total;
	
	public int getNumberOfItems() {
		return numberOfItems;
	}
	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
}
