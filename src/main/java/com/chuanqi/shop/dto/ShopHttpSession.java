package com.chuanqi.shop.dto;

import java.io.Serializable;

public class ShopHttpSession implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Account account;
	private ShoppingCart shoppingCart;

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
}
