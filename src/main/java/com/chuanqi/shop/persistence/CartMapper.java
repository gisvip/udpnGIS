package com.chuanqi.shop.persistence;

import java.util.List;

import com.chuanqi.shop.domain.Cart;

public interface CartMapper {
	public List<Cart> getNFYShoppingCartByCustomerId(Long customerId);//not finished yet shop cart
	public void insertShoppingCart(Cart shoppingCart);
	public void deleteShoppingCartByItemId(Long itemId);	
}
