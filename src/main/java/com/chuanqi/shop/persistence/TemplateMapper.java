package com.chuanqi.shop.persistence;

import com.chuanqi.shop.domain.Template;

public interface TemplateMapper {
	public Template getById(Long id);
	public void update(Template template);
	
	
}
