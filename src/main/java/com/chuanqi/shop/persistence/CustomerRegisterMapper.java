package com.chuanqi.shop.persistence;

import com.chuanqi.shop.domain.CustomerRegister;

public interface CustomerRegisterMapper {
	public CustomerRegister getById(Long userid);	
	public CustomerRegister getByTelephone(String loginName);
	public void createCustomerRegister(CustomerRegister customerLogin);	
}
