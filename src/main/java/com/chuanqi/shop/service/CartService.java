package com.chuanqi.shop.service;

import java.util.List;

import com.chuanqi.shop.domain.Cart;

public interface CartService {
	public List<Cart> getNFYShoppingCartByCustomerId(Long customerId);
	public void insertShoppingCart(Cart shoppingCart);
	public void deleteShoppingCartByItemId(Long itemId);
}
