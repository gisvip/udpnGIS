package com.chuanqi.shop.service;

import com.chuanqi.shop.dto.Account;

/**
 * @author XIONGXQ
 *
 */
public interface AccountService {
  public Account getAccount(String username);
  public Account getAccount(String username, String password);
  public void insertAccount(Account account);
  public void updateAccount(Account account);
}
