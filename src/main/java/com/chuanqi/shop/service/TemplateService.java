package com.chuanqi.shop.service;

import com.chuanqi.shop.domain.Template;

public interface TemplateService {
	public Template getById(Long id);
	public void update(Template template);
}
