package com.chuanqi.shop.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.chuanqi.shop.domain.Template;
import com.chuanqi.shop.persistence.TemplateMapper;
import com.chuanqi.shop.service.TemplateService;

@Service("templateService")
public class TemplateServiceImpl implements TemplateService {

	@Resource
	private TemplateMapper templateMapper;
	
	@Override
	public Template getById(Long id) {		
		return templateMapper.getById(id);
	}

	@Override
	public void update(Template template) {
		templateMapper.update(template);		
	}
	
}
