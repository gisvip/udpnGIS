package com.chuanqi.shop.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.shop.domain.CustomerRegister;
import com.chuanqi.shop.persistence.CustomerRegisterMapper;
import com.chuanqi.shop.service.CustomerRegisterService;

@Service("customerLoginService")
public class CustomerRegisterServiceImpl implements CustomerRegisterService{

	@Resource
	private CustomerRegisterMapper customerLoginMapper;
	
	@Autowired
    private PasswordGenerator passwordGenerator;
	
	@Override
	public CustomerRegister getById(Long userid) {
		return customerLoginMapper.getById(userid);
	}
	
	@Override
	public CustomerRegister getByTelephone(String loginName) {
		return customerLoginMapper.getByTelephone(loginName);
	}

	@Override
	public void createCustomerRegister(CustomerRegister customerLogin) {
		passwordGenerator.encryptPassword(customerLogin);//加密密码
		customerLoginMapper.createCustomerRegister(customerLogin);
	}

}
