package com.chuanqi.shop.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.chuanqi.shop.domain.Cart;
import com.chuanqi.shop.persistence.CartMapper;
import com.chuanqi.shop.service.CartService;

@Service("shoppingCartService")
public class CartServiceImpl implements CartService{

	@Resource
	private CartMapper shoppingCartMapper;
	
	@Override
	public List<Cart> getNFYShoppingCartByCustomerId(Long customerId) {
		return shoppingCartMapper.getNFYShoppingCartByCustomerId(customerId);
	}

	@Override
	public void insertShoppingCart(Cart shoppingCart) {
		shoppingCartMapper.insertShoppingCart(shoppingCart);
	}

	@Override
	public void deleteShoppingCartByItemId(Long itemId) {
		shoppingCartMapper.deleteShoppingCartByItemId(itemId);
	}

}
