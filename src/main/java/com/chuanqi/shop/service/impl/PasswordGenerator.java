package com.chuanqi.shop.service.impl;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Service;
import com.chuanqi.shop.domain.CustomerRegister;

@Service("passwordGenerator")
public class PasswordGenerator {
	private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

    private String algorithmName = "md5";
    private int hashIterations = 2;

    public void setRandomNumberGenerator(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }

    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }

    public void setHashIterations(int hashIterations) {
        this.hashIterations = hashIterations;
    }

    public void encryptPassword(CustomerRegister customerLogin) {
    	customerLogin.setSalt(randomNumberGenerator.nextBytes().toHex());
        String newPassword = new SimpleHash(algorithmName,customerLogin.getPassword(),ByteSource.Util.bytes(customerLogin.getSalt()),hashIterations).toHex();
        customerLogin.setPassword(newPassword);
    }
    
    public static void main(String[] args){
    	String algorithmName = "md5";
    	int hashIterations = 2;
    	
    	//4cbcd14a086dba91ddc99e8945f2573a
    	String password = "xiongxq123";
    	String salt = "2dfb38fdc82779a1ce5ba75dc09a7521";
    	String newPassword = new SimpleHash(algorithmName,password,ByteSource.Util.bytes(salt),hashIterations).toHex();
    	
    	System.out.println(newPassword);
    }
}
