package com.chuanqi.shop.service;

import com.chuanqi.shop.domain.CustomerRegister;

public interface CustomerRegisterService {
	public CustomerRegister getById(Long userid);
	public CustomerRegister getByTelephone(String loginName);
	public void createCustomerRegister(CustomerRegister customerLogin);
}
