package com.chuanqi.shop.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/nanda")
public class NandaController {
	@RequestMapping(path = "/index", method = RequestMethod.GET)
    public String indexGet(HttpServletRequest request, Model model) {	
		return "shop/index_nanda";
    }
}
