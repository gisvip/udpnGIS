package com.chuanqi.shop.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/i/order")
public class CategoryController {
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listGet(HttpServletRequest request) { 	
		return "shop/i/order/list";
    }
}
