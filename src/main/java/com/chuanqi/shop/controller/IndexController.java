package com.chuanqi.shop.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.chuanqi.shop.filter.ShoppingAuthenticationFilter;
import com.chuanqi.shop.service.CustomerRegisterService;
import com.chuanqi.shop.service.CartService;

@Controller
@RequestMapping("/index")
public class IndexController {    
	@Autowired
    private CustomerRegisterService customerLoginService;	
	
	@Autowired
    private CartService shoppingCartService;
    
	@RequestMapping(method = RequestMethod.GET)
    public String indexGet(HttpServletRequest request, Model model) {	
		return "shop/index";
		
    }			
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginPost(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		return ShoppingAuthenticationFilter.loginPost(customerLoginService, shoppingCartService, request, redirectAttributes, "redirect:/index");
	}	
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logoutPost(HttpServletRequest request) {
		return ShoppingAuthenticationFilter.logoutPost(request, "redirect:/index");
	}
}
