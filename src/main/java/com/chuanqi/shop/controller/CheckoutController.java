package com.chuanqi.shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/i/checkout")
public class CheckoutController {
	
	@RequestMapping(method = RequestMethod.GET)
    public String defaultGet(Model model) {    	
		//CustomerLogin login = new CustomerLogin();    	
        //model.addAttribute("login", login);            	
        //<input type="hidden" id="userId" value="${sessionScope.userInfo.userId}" />
		
        return "shop/checkout/default";
    }
}
