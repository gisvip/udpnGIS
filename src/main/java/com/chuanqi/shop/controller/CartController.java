package com.chuanqi.shop.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.chuanqi.shop.filter.ShoppingAuthenticationFilter;
import com.chuanqi.shop.service.CartService;
import com.chuanqi.shop.service.CustomerRegisterService;

@Controller
@RequestMapping("/mycart")
public class CartController {
	@Autowired
    private CustomerRegisterService customerRegisterService;	
	
	@Autowired
    private CartService cartService;
	
	@RequestMapping(value = "/removeItemFromCart", method = RequestMethod.POST)
	public String removeItemFromCartPost(HttpServletRequest request) {

		return null;
	}

	@RequestMapping(value = "/updateCartQuantities", method = RequestMethod.POST)
	public String updateCartQuantitiesPost(HttpServletRequest request) {

		return null;
	}

	@RequestMapping(value = "/checkOut", method = RequestMethod.POST)
	public String checkOutPost(HttpServletRequest request) {

		return null;
	}

	@RequestMapping(value = "/clear", method = RequestMethod.POST)
	public String clearPost(HttpServletRequest request) {

		return null;
	}
	
	
	
	@RequestMapping(method = RequestMethod.GET)
    public String mycartGet(HttpServletRequest request, Model model) {		
		return "shop/mycart/default";	
    }	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginPost(HttpServletRequest request, RedirectAttributes redirectAttributes) {		
		return ShoppingAuthenticationFilter.loginPost(customerRegisterService, cartService, request, redirectAttributes, "redirect:/mycart");
	}	
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logoutPost(HttpServletRequest request) {
		return ShoppingAuthenticationFilter.logoutPost(request, "redirect:/mycart");
	}
}
