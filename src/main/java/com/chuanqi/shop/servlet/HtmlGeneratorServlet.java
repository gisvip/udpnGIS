package com.chuanqi.shop.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.chuanqi.common.utils.HtmlGenerator;

@WebServlet(name = "staticServlet",  urlPatterns = {"/static.html"})
public class HtmlGeneratorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		doGet(request, response);
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 		  
		if(request.getParameter("chapterId") != null){ 
	        String chapterFileName = request.getParameter("chapterId")+".html"; 
	        String chapterFilePath = getServletContext().getRealPath("/") + "chuanqi\\" + chapterFileName; 
	        File chapterFile = new File(chapterFilePath); 
	        if(chapterFile.exists()){
	        	System.out.println(chapterFilePath);
	        	
	        	response.sendRedirect("chuanqi/"+chapterFileName);
	        	return;
	        }
			//String jspFilePath = getServletContext().getRealPath("/")+"";
			//System.out.println(jspFilePath);
	        
	        HtmlGenerator.createStaticHTMLPage(request, response, getServletContext(), chapterFileName, chapterFilePath, "/chuanqi/22.jsp"); 
	    } 
	}     
}
