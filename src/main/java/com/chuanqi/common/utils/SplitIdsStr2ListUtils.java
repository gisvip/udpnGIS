package com.chuanqi.common.utils;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class SplitIdsStr2ListUtils {
	public static List<Long> splitIdsStr2List(String idsStr) {
		List<Long> ids = new ArrayList<Long>();
		if(StringUtils.isNotEmpty(idsStr)) {
        	String[] idStrs = idsStr.split(",");
        	for(String idStr : idStrs) {
                if(StringUtils.isEmpty(idStr)) {
                    continue;
                }
                ids.add(Long.valueOf(idStr));
            }
        }	    

        return ids;
    }    
}
