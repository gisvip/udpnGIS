package com.chuanqi.common.utils;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import org.apache.commons.codec.binary.Base64;
import java.io.UnsupportedEncodingException;

public class EncodeFilename {
	// 其中 Base64 类来自 org.apache.commons.codec 组件 一个40多k的jar 要比javamail里的那个简洁很多
		public static String encodeFileName(HttpServletRequest request, String fileName) throws UnsupportedEncodingException {
			String agent = request.getHeader("USER-AGENT");
			if (null != agent && -1 != agent.indexOf("MSIE")) {
				return URLEncoder.encode(fileName, "UTF8");
			}else if (null != agent && -1 != agent.indexOf("Chrome")) {
				return URLEncoder.encode(fileName, "UTF8");
			}else if (null != agent && -1 != agent.indexOf("Mozilla")) {
				return "=?UTF-8?B?"+(new String(Base64.encodeBase64(fileName.getBytes("UTF-8"))))+"?=";
			} else {
				return fileName;
			}
		    
		}	
}
