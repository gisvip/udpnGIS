package com.chuanqi.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DatetimeDemo {
	public static void main(String[] args){
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyyyyyy-MM-dd HH(hh):mm:ss S E D F w W a k K z");  
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//定义格式，不显示毫秒   
		 
        Date date = new Date(System.currentTimeMillis());  
        String timeStr = sdf.format(date);  
        System.out.println(timeStr);  
        
        /*CREATE TABLE test_dt (
        	    id INT AUTO_INCREMENT PRIMARY KEY,
        	    created_at DATETIME
        	);
        	 
        	INSERT INTO test_dt(created_at)
        	VALUES('2015-11-05 14:29:36');*/
	}
}
