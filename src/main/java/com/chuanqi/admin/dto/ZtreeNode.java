package com.chuanqi.admin.dto;

import java.io.Serializable;

public class ZtreeNode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;	
	private Long pId;	
	private String name;
	private Boolean open = true;
	private Boolean checked = false;	
	//private Boolean isParent = false;	

	public ZtreeNode(){
		
	}	
	
	public ZtreeNode(Long id, Long pId, String name, Boolean open, Boolean checked){
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.open = open;
		this.checked = checked;
		//this.isParent = isParent;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}
	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	/*public Boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}*/
}