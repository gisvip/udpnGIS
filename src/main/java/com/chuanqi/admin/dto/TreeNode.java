package com.chuanqi.admin.dto;

import java.io.Serializable;

public class TreeNode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;	
	private Long pId;	
	private String name;	
	private String type;	

	public TreeNode(){
		
	}	
	
	public TreeNode(Long id, Long pId, String name, String type){
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.type = type;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}