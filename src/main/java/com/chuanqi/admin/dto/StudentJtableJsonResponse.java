package com.chuanqi.admin.dto;

import java.util.List;
import com.chuanqi.admin.domain.Student;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StudentJtableJsonResponse {
	@JsonProperty("Result")
	private String result;
	
	@JsonProperty("Records")
	private List<Student> records;
	
	@JsonProperty("TotalRecordCount")
	private long totalRecordCount;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<Student> getRecords() {
		return records;
	}

	public void setRecords(List<Student> records) {
		this.records = records;
	}

	public long getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}		
}
