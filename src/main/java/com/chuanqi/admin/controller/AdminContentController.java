package com.chuanqi.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.chuanqi.admin.domain.Image;
import com.chuanqi.admin.domain.Video;
import com.chuanqi.admin.qp.ImageQp;
import com.chuanqi.admin.qp.VideoQp;
import com.chuanqi.admin.service.ImageService;
import com.chuanqi.admin.service.VideoService;
import com.chuanqi.admin.uploadconfig.PropertyPlaceholderConfig;

@Controller
@RequestMapping("/authc/content")
@Import(PropertyPlaceholderConfig.class)
public class AdminContentController {
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private VideoService videoService;
	
	@Value("${images.gallery.path}")
    private String imagesPath;
	
	@Value("${videos.gallery.path}")
    private String videosPath;
	
	@RequestMapping(path = "/header", method = RequestMethod.GET)
    public String headerUpdateGet(HttpServletRequest request, Model model) { 
		return "authc/content/header";
	}
	
	@RequestMapping(path = "/jplayer", method = RequestMethod.GET)
    public String jplayerGet(HttpServletRequest request, Model model) { 
		String video = request.getParameter("video"); 
		model.addAttribute("video", video);
		return "authc/content/jplayer";
	}
	
	@RequestMapping(path = "/header", method = RequestMethod.POST)
    public String headerUpdatePost(HttpServletRequest request, Model model) { 
		return "authc/content/header";
	}
	
	
	@RequestMapping(value = "/images", method = RequestMethod.GET)	
	public String imagesFirstPageGet(Model model) {			
		final long rowCount = imageService.getTotalCount();			
		List<Image> images = imageService.getFirstPage();		
		int pageIndex = 1;
		int pageSize = 10;
		
		String sortName = "UPLOADTIME";
		String sortOrder = "ASC";
	    
	    long totalPages = rowCount / pageSize;
	    long mod = rowCount % pageSize;
	    if (mod > 0) {
	    	totalPages++;
	    }
		
	    if( rowCount - (pageIndex-1) * pageSize > 0 ){
	    	model.addAttribute("current", pageIndex);    	   	
	    	model.addAttribute("pageSize", pageSize);	
			
	    	model.addAttribute("sortName", sortName);
			model.addAttribute("sortOrder", sortOrder);	
			
			model.addAttribute("totalPages", totalPages);
			model.addAttribute("total", rowCount);				    
		    
			model.addAttribute("from", pageIndex);
		    model.addAttribute("to", pageSize);
		    
		    model.addAttribute("path", imagesPath);
		    
		    model.addAttribute("images", images);  
	    }     			
		 
		return "authc/content/images";  
	}
	
	
	@RequestMapping(value = "/images", method = RequestMethod.POST)	
	public String queryImagesPagePost(
			@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,    		  
    		HttpServletRequest request,
    		Model model) {			
		
		String sortName = request.getParameter("sortName"); 		
		String sortOrder = request.getParameter("sortOrder");	
				
		String description = request.getParameter("description");
		String beginTime = request.getParameter("beginTime");
		String endTime = request.getParameter("endTime");	
		//String uploadTime = request.getParameter("uploadTime");	
		
		if( StringUtils.isBlank(sortName) || StringUtils.isBlank(sortOrder) ){
			sortName = "UPLOADTIME";
			sortOrder = "ASC";
		}
		
		/*
		SELECT * FROM images WHERE 
		UNIX_TIMESTAMP(upload_time) > UNIX_TIMESTAMP('2011-03-03 17:39:05') 
		AND UNIX_TIMESTAMP(upload_time) < UNIX_TIMESTAMP('2016-04-17 17:39:52')
		*/

		
		long offset = pageIndex > 0 ? (pageIndex - 1) * pageSize : 0;
		ImageQp qp = new ImageQp();
		qp.setOffset(offset);		
		qp.setPageSize(pageSize);
		qp.setSortName(sortName);
		qp.setSortOrder(sortOrder);	
		
		qp.setDescription(description);
		//qp.setUploadTime(uploadTime);
		qp.setBeginTime(beginTime);
		qp.setEndTime(endTime);		
		
		final long rowCount = imageService.getCountByQueryBean(qp);				
		List<Image> images = imageService.getByQueryBean(qp);			
		
	    long totalPages = rowCount / pageSize;
	    long mod = rowCount % pageSize;
	    if (mod > 0) {
	    	totalPages++;
	    }    
	    
	    long from = (pageIndex - 1) * pageSize + 1;
		long to = 0l;
		
		if(pageIndex<totalPages){			
			to = pageIndex * pageSize;
		}else{
			to = rowCount;
		}			
		
	    if( rowCount - (pageIndex-1)*pageSize > 0 ){
	    	model.addAttribute("current", pageIndex);    	   	
	    	model.addAttribute("pageSize", pageSize);	
			
			model.addAttribute("sortName", sortName);
			model.addAttribute("sortOrder", sortOrder);			
			
			model.addAttribute("totalPages", totalPages);
			model.addAttribute("total", rowCount);				    
		    
			model.addAttribute("from", from);
		    model.addAttribute("to", to);
		    
		    model.addAttribute("path", imagesPath);
		    
		    model.addAttribute("images", images);  
	    }     			
		 
	    return "authc/content/images";   
	}
	
	
	@RequestMapping(value = "/videos", method = RequestMethod.GET)	
	public String videosFirstPageGet(Model model) {			
		final long rowCount = videoService.getTotalCount();			
		List<Video> videos = videoService.getFirstPage();		
		int pageIndex = 1;
		int pageSize = 10;
		
		String sortName = "UPLOADTIME";
		String sortOrder = "ASC";
	    
	    long totalPages = rowCount / pageSize;
	    long mod = rowCount % pageSize;
	    if (mod > 0) {
	    	totalPages++;
	    }
		
	    if( rowCount - (pageIndex-1) * pageSize > 0 ){
	    	model.addAttribute("current", pageIndex);    	   	
	    	model.addAttribute("pageSize", pageSize);	
			
	    	model.addAttribute("sortName", sortName);
			model.addAttribute("sortOrder", sortOrder);	
			
			model.addAttribute("totalPages", totalPages);
			model.addAttribute("total", rowCount);				    
		    
			model.addAttribute("from", pageIndex);
		    model.addAttribute("to", pageSize);
		    
		    model.addAttribute("path", videosPath);
		    
		    model.addAttribute("videos", videos);  
	    }     			
		 
		return "authc/content/videos";  
	}
	
	
	@RequestMapping(value = "/videos", method = RequestMethod.POST)	
	public String queryVideosPagePost(
			@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,    		  
    		HttpServletRequest request,
    		Model model) {			
		
		String sortName = request.getParameter("sortName"); 		
		String sortOrder = request.getParameter("sortOrder");	
				
		String description = request.getParameter("description");
		String beginTime = request.getParameter("beginTime");
		String endTime = request.getParameter("endTime");	
		//String uploadTime = request.getParameter("uploadTime");	
		
		if( StringUtils.isBlank(sortName) || StringUtils.isBlank(sortOrder) ){
			sortName = "UPLOADTIME";
			sortOrder = "ASC";
		}
		
		/*
		SELECT * FROM images WHERE 
		UNIX_TIMESTAMP(upload_time) > UNIX_TIMESTAMP('2011-03-03 17:39:05') 
		AND UNIX_TIMESTAMP(upload_time) < UNIX_TIMESTAMP('2016-04-17 17:39:52')
		*/

		
		long offset = pageIndex > 0 ? (pageIndex - 1) * pageSize : 0;
		VideoQp qp = new VideoQp();
		qp.setOffset(offset);		
		qp.setPageSize(pageSize);
		qp.setSortName(sortName);
		qp.setSortOrder(sortOrder);	
		
		qp.setDescription(description);
		//qp.setUploadTime(uploadTime);
		qp.setBeginTime(beginTime);
		qp.setEndTime(endTime);		
		
		final long rowCount = videoService.getCountByQueryBean(qp);				
		List<Video> videos = videoService.getByQueryBean(qp);			
		
	    long totalPages = rowCount / pageSize;
	    long mod = rowCount % pageSize;
	    if (mod > 0) {
	    	totalPages++;
	    }    
	    
	    long from = (pageIndex - 1) * pageSize + 1;
		long to = 0l;
		
		if(pageIndex<totalPages){			
			to = pageIndex * pageSize;
		}else{
			to = rowCount;
		}			
		
	    if( rowCount - (pageIndex-1)*pageSize > 0 ){
	    	model.addAttribute("current", pageIndex);    	   	
	    	model.addAttribute("pageSize", pageSize);	
			
			model.addAttribute("sortName", sortName);
			model.addAttribute("sortOrder", sortOrder);			
			
			model.addAttribute("totalPages", totalPages);
			model.addAttribute("total", rowCount);				    
		    
			model.addAttribute("from", from);
		    model.addAttribute("to", to);
		    
		    model.addAttribute("path", videosPath);
		    
		    model.addAttribute("videos", videos);  
	    }     			
		 
	    return "authc/content/videos";   
	}
	
	
	
	
	
	
	
	
	
	
}
