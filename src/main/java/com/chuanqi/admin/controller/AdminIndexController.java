package com.chuanqi.admin.controller; 

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.chuanqi.admin.domain.Resourse;
import com.chuanqi.admin.shiro.UserRealm.ShiroUser;
import com.chuanqi.admin.service.ResourceService;
import com.chuanqi.admin.service.UserRoleService;

@Controller
@RequestMapping("/authc")
public class AdminIndexController {
	
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private UserRoleService userRoleService;
    
    @RequestMapping("/index")
    public String index(HttpServletRequest request, Model model) {          
    	Subject currentUser = SecurityUtils.getSubject();
    	ShiroUser shiroUser = (ShiroUser) currentUser.getPrincipal();
    	Long userId = shiroUser.getId();
    	
        if(userRoleService.getUserRolesByUserId(userId).size()>0){        	
        	List<Long> permittedItemIds = shiroUser.getItemIds();
        	if(permittedItemIds.size()>0){
        		List<Resourse> permittedMenus = resourceService.getMenusByItemIds(permittedItemIds);    	
            	model.addAttribute("menus", permittedMenus);
        	}       	
    	}    	        
        
        return "authc/index";
    }
    
    @RequestMapping(value="/menu/{menuId}", method=RequestMethod.GET)
	public String deleteById(@PathVariable("menuId") Long menuId, Model model){		
		
    	Subject currentUser = SecurityUtils.getSubject();
    	ShiroUser shiroUser = (ShiroUser) currentUser.getPrincipal();
    	List<Long> itemIds = shiroUser.getItemIds();
    	
    	Map<String,Object> params = new HashMap<String,Object>();
    	params.put("list", itemIds);
    	params.put("menuId", menuId);
    	
    	List<Resourse>  items = resourceService.getItemIdsByMenuId(params);
    	System.out.println(items.get(0).getUrl());
    	
    	model.addAttribute("items", items);
        return "authc/westMenu";
	}    
}

