package com.chuanqi.admin.qp;

/** 
 * 
 * */
public class Page {
	/** 每页条数 */	
	private int pageSize;
	
	private long offset;
	
	/** 获取每页显示条数 */
	public int getPageSize() {
		return pageSize;
	}

	/** 设置每页显示条数 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}	
	
	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}
	
	
	
}