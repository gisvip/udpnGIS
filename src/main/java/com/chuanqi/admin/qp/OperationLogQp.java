package com.chuanqi.admin.qp;

import java.sql.Timestamp;

public class OperationLogQp extends Page {
	private String ip;	
	private int userId;		
	private Timestamp loginTime;	
	private Timestamp beginTime;		
	private String opPermission;	
	private Boolean opStatus;	
	private Timestamp opTime;	
	
	private String sortName;
	private String sortOrder;
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Timestamp getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}	
	
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	
	public String getOpPermission() {
		return opPermission;
	}
	public void setOpPermission(String opPermission) {
		this.opPermission = opPermission;
	}
	public Boolean getOpStatus() {
		return opStatus;
	}
	public void setOpStatus(Boolean opStatus) {
		this.opStatus = opStatus;
	}
	public Timestamp getOpTime() {
		return opTime;
	}
	public void setOpTime(Timestamp opTime) {
		this.opTime = opTime;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}	
}
