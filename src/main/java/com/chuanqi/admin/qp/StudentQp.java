package com.chuanqi.admin.qp;

public class StudentQp extends Page {
	private String stdntName;	
	private String email;	
	
	private String sortName;
	private String sortOrder;
	
	public String getStdntName() {
		return stdntName;
	}
	public void setStdntName(String stdntName) {
		this.stdntName = stdntName;
	}	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}	
}
