package com.chuanqi.admin.qp;

import java.sql.Timestamp;

public class OnlineQp extends Page {
	private String ip;	
	private Timestamp hitTime;
	private Timestamp beginTime;	
	private Timestamp endTime;
	
	private String sortName;
	private String sortOrder;
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Timestamp getHitTime() {
		return hitTime;
	}
	public void setHitTime(Timestamp hitTime) {
		this.hitTime = hitTime;
	}	
	
	public Timestamp getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Timestamp beginTime) {
		this.beginTime = beginTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}	
}
