package com.chuanqi.admin.qp;

public class RoleQp extends Page{ 
	private String role;     
	private String sortName;
	private String sortOrder;
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}	
	
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}		
}
