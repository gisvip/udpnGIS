package com.chuanqi.admin.persistence;

import java.util.List;

import com.chuanqi.admin.domain.Online;
import com.chuanqi.admin.qp.OnlineQp;

public interface OnlineMapper {
	public void insert(Online online);	
	public List<Online> getByQueryBean(OnlineQp qp);	
	public Long getCountByQueryBean(OnlineQp qp);
	
	public Long getTotalCount();
}
