package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.PermittedRecordModel;

public interface PermittedItemResourceIdMapper {
	public List<PermittedRecordModel> getPermittedItemResourceIdByUserId(Long userId);
	
	public List<PermittedRecordModel> getPermittedItemResourceIdByUserIdForTest(Long userId);
}
