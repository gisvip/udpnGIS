package com.chuanqi.admin.persistence;

import java.util.List;

import com.chuanqi.admin.domain.LoginLog;
import com.chuanqi.admin.qp.LoginLogQp;

public interface LoginLogMapper {
	public void insert(LoginLog loginLog);	
	public List<LoginLog> getByQueryBean(LoginLogQp qp);	
	public Long getCountByQueryBean(LoginLogQp qp);
	
	public Long getTotalCount();
}
