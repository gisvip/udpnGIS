package com.chuanqi.admin.persistence;

import java.util.List;

import com.chuanqi.admin.domain.Role;
import com.chuanqi.admin.qp.RoleQp;

public interface RoleMapper {
	public int createRole(Role role);
    public int updateRole(Role role);
    
    public void deleteRole(Long roleId);

    public Role findOne(Long roleId);
    public List<Role> findAll();
    public List<Role> getRolesByUserId(Long userId);
    
    
    public List<Role> getByIdsStr(String roleIdsStr);
    public List<Role> selectRolesIn(List<Long> roleIds);
    
    
    public Role getByRolename(String role);  
    public Role getOneById(Long roleId);
    
    public List<Role> getFirstPageRoles();
	public Long getAllRolesTotalCount();
	
	public List<Role> getByQueryBean(RoleQp qp);	
	public Long getCountByQueryBean(RoleQp qp);
}
