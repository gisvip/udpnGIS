package com.chuanqi.admin.persistence;

import java.util.List;

import com.chuanqi.admin.domain.User;
import com.chuanqi.admin.qp.UserQp;

public interface UserMapper {
	public void createUser(User user);
    public void updateUser(User user);
    public void deleteUser(Long userId);

    public User getOneById(Long userId);
    public void updateUserPassword(User user);
    public void updateAccount(User user);

    public List<User> findAll();

    public User findByUsername(String username);
    
    
    public List<User> getByQueryBean(UserQp qp);	
	public Long getCountByQueryBean(UserQp qp);
	
	public List<User> getFirstPageUsers();	
	public Long getAllUsersTotalCount();
	
}
