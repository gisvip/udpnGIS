package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.PermittedRecordModel;

public interface PermittedRecordModelMapper {
	public List<PermittedRecordModel> getPermittedRecordModelsByByUserId(Long userId);
}
