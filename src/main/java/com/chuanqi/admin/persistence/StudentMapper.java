package com.chuanqi.admin.persistence;

import java.util.List;

import com.chuanqi.admin.domain.Student;
import com.chuanqi.admin.qp.StudentQp;

public interface StudentMapper {	 
	public Student getById(Long id);
	public void insert(Student student);
	public void update(Student student);
	public void deleteById(Long id);
	
	public List<Student> getFirstPage();
	public Long getTotalCount();
	
	public Long getCountByQueryBean(StudentQp qp);
	public List<Student> getByQueryBean(StudentQp qp);
	
	public int insertListBatch(List<Student> list);
	public List<Student> getList(StudentQp qp);
	
	public List<Student> getAll();
}
