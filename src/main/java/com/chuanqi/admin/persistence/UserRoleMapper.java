package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.UserRole;

public interface UserRoleMapper {
	public List<UserRole> getUserRolesByUserId(Long userId);
	public void deleteRbyUR(Long roleId);
	public void insertRbyUR(UserRole rr);
	
	public List<UserRole> getUserRoleByRoleId(Long roleId);
	
}
