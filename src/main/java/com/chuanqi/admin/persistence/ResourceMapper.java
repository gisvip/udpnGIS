package com.chuanqi.admin.persistence;

import java.util.List;
import java.util.Map;
import com.chuanqi.admin.domain.Resourse;

public interface ResourceMapper {	
	public List<Resourse> findAll();
	public Resourse findOne(Long resourceId);   
    public List<Resourse> getMenusByItemIds(List<Long> itemIds);
    public List<Resourse> getItemIdsByMenuId(Map<String,Object>  params);  
    public List<Resourse> getResourceIdsByRoleId(Long roleId);
}
