package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.RoleResource;

public interface RoleResourceMapper {
	public List<RoleResource> getRoleResourcesByRoleId(Long roleId);
	public void deleteRbyR(Long roleId);
	public void insertRoleResource(RoleResource rr);
}
