package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.Image;
import com.chuanqi.admin.qp.ImageQp;

public interface ImageMapper {
	public List<Image> getFirstPage();
	public Long getTotalCount();
	
	public Long getCountByQueryBean(ImageQp qp);
	public List<Image> getByQueryBean(ImageQp qp);
}
