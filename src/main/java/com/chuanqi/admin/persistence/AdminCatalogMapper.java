package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.Catalog;
import com.chuanqi.admin.qp.CatalogQp;

public interface AdminCatalogMapper {
	public void insert(Catalog catalog);
	
	public void deleteById(Long id);	
	public void update(Catalog catalog);
	public Catalog getById(Long id);	
	
	public Catalog getById2(Long id);	
	
	public List<Catalog> getCatalogListByPid(String parentId);
	public List<Catalog> getCatalogListByName(String catalogName);
	public List<Catalog> getAllCatalogList();
	
	public List<Catalog> getByQueryBean(CatalogQp qp);	
	public Long getCountByQueryBean(CatalogQp qp);
}
