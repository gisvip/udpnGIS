package com.chuanqi.admin.persistence;

import java.util.List;
import com.chuanqi.admin.domain.Video;
import com.chuanqi.admin.qp.VideoQp;

public interface VideoMapper {
	public List<Video> getFirstPage();
	public Long getTotalCount();
	
	public Long getCountByQueryBean(VideoQp qp);
	public List<Video> getByQueryBean(VideoQp qp);
}
