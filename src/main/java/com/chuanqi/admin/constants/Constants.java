package com.chuanqi.admin.constants;

public class Constants {
	public static final String create_sucess_message = "添加成功！";
	public static final String create_failure_message = "添加失败！";
	
	public static final String update_sucess_message = "更新成功！";
	public static final String update_failure_message = "更新失败！";
	
	public static final String delete_sucess_message = "删除成功！";
	public static final String delete_failure_message = "删除失败！";
	
}
