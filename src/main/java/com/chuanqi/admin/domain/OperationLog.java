package com.chuanqi.admin.domain;

import java.io.Serializable;
import java.sql.Timestamp;

public class OperationLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;	
	private Long userId;	
	private String opPermission;			
	private Timestamp opTime;
	
	public OperationLog(){}
	
	public OperationLog(Long userId,String opPermission,Timestamp opTime){
		this.userId = userId;
		this.opPermission = opPermission;
		this.opTime = opTime;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getOpPermission() {
		return opPermission;
	}
	public void setOpPermission(String opPermission) {
		this.opPermission = opPermission;
	}
	public Timestamp getOpTime() {
		return opTime;
	}
	public void setOpTime(Timestamp opTime) {
		this.opTime = opTime;
	}	
}
