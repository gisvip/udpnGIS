package com.chuanqi.admin.domain;

import java.io.Serializable;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;     

    @NotEmpty
    @Length(min=6,max=32)
    private String username; 
    
    @NotEmpty
    @Length(min=6,max=32)
    private String password; 
    private String salt;     
    
	private Boolean locked = Boolean.FALSE;
	
	private String realname;
	private String telephone;
	private String email;
	
	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }    

    public String getUsername() {
        return username;
    }   

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    /*public String getCredentialsSalt() {
        return username + salt;
    }*/	
    
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }   
}

