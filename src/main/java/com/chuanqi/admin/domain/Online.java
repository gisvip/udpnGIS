package com.chuanqi.admin.domain;

import java.io.Serializable;
import java.sql.Timestamp;

public class Online implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;	
	private String ip;	
	private Timestamp hitTime;
	
	public Online(String ip, Timestamp hitTime){
		this.ip = ip;
		this.hitTime = hitTime;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Timestamp getHitTime() {
		return hitTime;
	}
	public void setHitTime(Timestamp hitTime) {
		this.hitTime = hitTime;
	}	
}
