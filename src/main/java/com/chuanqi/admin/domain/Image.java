package com.chuanqi.admin.domain;

import java.io.Serializable;

public class Image implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;	
	private String description;
	private String uploadTime; 	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}
}
