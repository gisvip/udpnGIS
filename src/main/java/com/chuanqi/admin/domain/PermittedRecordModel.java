package com.chuanqi.admin.domain;

import java.io.Serializable;

public class PermittedRecordModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long recordId;

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
}
