package com.chuanqi.admin.domain;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;	
	private String stdntName;	
	private String email;		

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotBlank
	public String getStdntName() {
		return stdntName;
	}

	public void setStdntName(String stdntName) {
		this.stdntName = stdntName;
	}

	@NotBlank
	@Email
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}