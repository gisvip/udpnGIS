package com.chuanqi.admin.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

public class LoginLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;	
	private int userId;	
	private String ip;	
	private String mac;	
	private Boolean type = Boolean.FALSE;	
	
	@JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp loginTime;
	
	public LoginLog(){}
	
	public LoginLog(int userId,String ip, String mac, Boolean type, Timestamp loginTime){
		this.userId = userId;
		this.ip = ip;
		this.mac = mac;
		this.type = type;
		this.loginTime = loginTime;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public Boolean getType() {
		return type;
	}
	public void setType(Boolean type) {
		this.type = type;
	}
	public Timestamp getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}	  
}
