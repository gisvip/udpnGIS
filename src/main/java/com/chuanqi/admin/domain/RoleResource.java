package com.chuanqi.admin.domain;

import java.io.Serializable;

public class RoleResource implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id; 
	private Long roleId;
	private Long resourceId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getResourceId() {
		return resourceId;
	}
	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}
	 
}