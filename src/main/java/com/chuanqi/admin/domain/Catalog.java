package com.chuanqi.admin.domain;

import java.beans.Transient;
import java.io.Serializable;

public class Catalog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;	
	private String name;			
	private Long parentId;	
	private String parentName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	@Transient
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
