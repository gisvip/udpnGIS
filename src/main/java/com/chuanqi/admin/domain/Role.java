package com.chuanqi.admin.domain;

import java.io.Serializable;

import org.hibernate.validator.constraints.NotEmpty;

public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id; 
	
	@NotEmpty
    private String role; 
    private String description;  

    public Role() {
    }

    public Role(String role, String description) {
        this.role = role;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }   
}

