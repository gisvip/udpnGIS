package com.chuanqi.admin.service;

import java.util.List;
import java.util.Set;

import com.chuanqi.admin.domain.User;
import com.chuanqi.admin.qp.UserQp;

public interface UserService {
	/**
     * 创建用户
     * @param user
     */
    public void createUser(User user);

    public void updateUser(User user);

    public void deleteUser(Long userId);

    /**
     * 修改密码
     * @param userId
     * @param newPassword
     */
    public void changePassword(Long userId, String newPassword);
    
    public void updateAccount(Long userId, Boolean locked);


    public User getOneById(Long userId);

    public List<User> findAll();

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public User findByUsername(String username);

    /**
     * 根据用户名查找其角色
     * @param username
     * @return
     */
    public Set<String> findRoles(String username);

    /**
     * 根据用户名查找其权限
     * @param username
     * @return
     */
    public Set<String> findPermissions(String username);
    
    
    public List<User> getByQueryBean(UserQp qp);	
	public Long getCountByQueryBean(UserQp qp);
	
	public List<User> getFirstPageUsers();
	public Long getAllUsersTotalCount();
}
