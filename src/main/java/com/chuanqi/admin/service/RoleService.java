package com.chuanqi.admin.service;

import java.util.List;
import java.util.Set;

import com.chuanqi.admin.domain.Role;
import com.chuanqi.admin.qp.RoleQp;

public interface RoleService {
	public int createRole(Role role);
    public int updateRole(Role role);
    
    public void deleteRole(Long roleId);

    public Role findOne(Long roleId);
    public List<Role> findAll();
    public List<Role> getRolesByUserId(Long userId);
    /**
     * 根据角色编号得到角色标识符列表
     * @param roleIds
     * @return
     */
    Set<String> findRoles(Long... roleIds);

    /**
     * 根据角色编号得到权限字符串列表
     * @param roleIds
     * @return
     */
    Set<String> findPermissions(Long[] roleIds);
    
    
    public List<Role> selectRolesIn(List<Long> roleIds);
    
    
    public Role getByRolename(String role); 
    public Role getOneById(Long roleId);
    
    public List<Role> getFirstPageRoles();
	public Long getAllRolesTotalCount();
	
	public List<Role> getByQueryBean(RoleQp qp);	
	public Long getCountByQueryBean(RoleQp qp);
    
}
