package com.chuanqi.admin.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.Catalog;
import com.chuanqi.admin.persistence.AdminCatalogMapper;
import com.chuanqi.admin.qp.CatalogQp;
import com.chuanqi.admin.service.AdminCatalogService;

@Service("catalogService")
public class AdminCatalogServiceImpl implements AdminCatalogService {
	@Resource
	private AdminCatalogMapper catalogMapper;
	
	@Override
	public void insert(Catalog catalog) {		
		catalogMapper.insert(catalog);
	}

	@Override
	public void deleteById(Long id) {		
		catalogMapper.deleteById(id);
	}

	@Override
	public void update(Catalog catalog) {		
		catalogMapper.update(catalog);
	}

	@Override
	public Catalog getById(Long id) {
		return catalogMapper.getById(id);
	}

	@Override
	public Catalog getById2(Long id) {
		return catalogMapper.getById2(id);
	}

	@Override
	public List<Catalog> getCatalogListByPid(String parentId) {
		return catalogMapper.getCatalogListByPid(parentId);
	}

	@Override
	public List<Catalog> getCatalogListByName(String catalogName) {
		return catalogMapper.getCatalogListByName(catalogName);
	}

	@Override
	public List<Catalog> getAllCatalogList() {
		return catalogMapper.getAllCatalogList();
	}

	@Override
	public List<Catalog> getByQueryBean(CatalogQp qp) {
		return catalogMapper.getByQueryBean(qp);
	}

	@Override
	public Long getCountByQueryBean(CatalogQp qp) {
		return catalogMapper.getCountByQueryBean(qp);
	}
}