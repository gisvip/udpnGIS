package com.chuanqi.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.Online;
import com.chuanqi.admin.persistence.OnlineMapper;
import com.chuanqi.admin.qp.OnlineQp;
import com.chuanqi.admin.service.OnlineService;

@Service
public class OnlineServiceImpl implements OnlineService {
	@Autowired
	private OnlineMapper onlineMapper;	
	
	@Override
	public void insert(Online online) {
		onlineMapper.insert(online);		
	}

	@Override
	public List<Online> getByQueryBean(OnlineQp qp) {		
		return onlineMapper.getByQueryBean(qp);
	}

	@Override
	public Long getCountByQueryBean(OnlineQp qp) {		
		return onlineMapper.getCountByQueryBean(qp);
	}

	@Override
	public Long getTotalCount() {
		return onlineMapper.getTotalCount();
	}

}
