package com.chuanqi.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.OperationLog;
import com.chuanqi.admin.persistence.OperationLogMapper;
import com.chuanqi.admin.qp.OperationLogQp;
import com.chuanqi.admin.service.OperationLogService;

@Service
public class OperationLogServiceImpl implements OperationLogService {
	@Autowired
	private OperationLogMapper operationLogMapper;	
	
	@Override
	public void insert(OperationLog operationLog) {
		operationLogMapper.insert(operationLog);		
	}

	@Override
	public List<OperationLog> getByQueryBean(OperationLogQp qp) {		
		return operationLogMapper.getByQueryBean(qp);
	}

	@Override
	public Long getCountByQueryBean(OperationLogQp qp) {		
		return operationLogMapper.getCountByQueryBean(qp);
	}

	@Override
	public Long getTotalCount() {
		return operationLogMapper.getTotalCount();
	}

}
