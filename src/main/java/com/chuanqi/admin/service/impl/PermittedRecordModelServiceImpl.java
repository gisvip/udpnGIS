package com.chuanqi.admin.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.chuanqi.admin.domain.PermittedRecordModel;
import com.chuanqi.admin.persistence.PermittedRecordModelMapper;
import com.chuanqi.admin.service.PermittedRecordModelService;


@Service("permittedRecordModelService")
public class PermittedRecordModelServiceImpl implements PermittedRecordModelService{
	@Resource
	private PermittedRecordModelMapper permittedRecordIdMapper;
	
	@Override
	public List<PermittedRecordModel> getPermittedRecordModelsByByUserId(Long userId) {
		return permittedRecordIdMapper.getPermittedRecordModelsByByUserId(userId);
	}
}
