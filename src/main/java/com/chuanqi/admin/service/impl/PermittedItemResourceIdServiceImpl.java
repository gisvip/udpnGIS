package com.chuanqi.admin.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.PermittedRecordModel;
import com.chuanqi.admin.persistence.PermittedItemResourceIdMapper;
import com.chuanqi.admin.service.PermittedItemResourceIdService;

@Service("permittedItemResourceIdService")
public class PermittedItemResourceIdServiceImpl implements PermittedItemResourceIdService{

	@Resource
	private PermittedItemResourceIdMapper permittedItemResourceIdMapper;
	
	@Override
	public List<PermittedRecordModel> getPermittedItemResourceIdByUserId(Long userId) {
		return permittedItemResourceIdMapper.getPermittedItemResourceIdByUserId(userId);
	}

	@Override
	public List<PermittedRecordModel> getPermittedItemResourceIdByUserIdForTest(Long userId) {
		return permittedItemResourceIdMapper.getPermittedItemResourceIdByUserIdForTest(userId);
	}

}
