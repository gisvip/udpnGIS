package com.chuanqi.admin.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.User;
import com.chuanqi.admin.domain.UserRole;
import com.chuanqi.admin.persistence.UserMapper;
import com.chuanqi.admin.qp.UserQp;
import com.chuanqi.admin.service.RoleService;
import com.chuanqi.admin.service.UserRoleService;
import com.chuanqi.admin.service.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

@Service("userService")
public class UserServiceImpl implements UserService {
	@Resource
	private UserMapper userMapper;
	
	@Autowired
    private RoleService roleService;
	
	@Autowired
    private UserRoleService userRoleService;
	
	@Autowired
    private PasswordHelper passwordHelper;
	
	@Override
	public void createUser(User user) {
		passwordHelper.encryptPassword(user);//加密密码
		userMapper.createUser(user);
	}	

	@Override
	public void updateUser(User user) {
		userMapper.updateUser(user);
	}	
	
	/**
	 * 判断是否超级管理员.
	 */
	private boolean isSupervisor(Long id) {
		return id == 1;
	}
	
	@Override
	public void deleteUser(Long userId) {
		if (isSupervisor(userId)) {			
			throw new RuntimeException("不能删除超级管理员用户");
		}
		userMapper.deleteUser(userId);
	}

	@Override
	public void changePassword(Long userId, String newPassword) {
		User user = userMapper.getOneById(userId);
        user.setPassword(newPassword);
        passwordHelper.encryptPassword(user);
        userMapper.updateUserPassword(user);		
	}

	@Override
	public User getOneById(Long userId) {
		return userMapper.getOneById(userId);
	}

	@Override
	public List<User> findAll() {
		return userMapper.findAll();
	}

	@Override
	public User findByUsername(String username) {
		return userMapper.findByUsername(username);
	}

	@Override
	public Set<String> findRoles(String username) {
		User user = findByUsername(username);
        if(user == null) {
            return Collections.EMPTY_SET;
        }
        //return roleService.findRoles(user.getRoleIds().toArray(new Long[0]));
        //todo xiongxq
        return null;
	}

	
	
	@Override
	public Set<String> findPermissions(String username) {
		User user = findByUsername(username);		
		
		Long userId = user.getId();
		List<Long> roleIds = new ArrayList<Long>();
	    List<UserRole> userRoles = userRoleService.getUserRolesByUserId(userId);
	    for(UserRole userRole: userRoles){        	
	    	Long roleId = userRole.getRoleId();
			roleIds.add(roleId);
	    }
	    
	    return roleService.findPermissions(roleIds.toArray(new Long[roleIds.size()]));       
	}
	
	private List<Long> getRoleIds(String roleIdsStr) {
		List<Long> roleIds = new ArrayList<Long>();
		if(StringUtils.isNotEmpty(roleIdsStr)) {
        	String[] roleIdStrs = roleIdsStr.split(",");
        	for(String roleIdStr : roleIdStrs) {
                if(StringUtils.isEmpty(roleIdStr)) {
                    continue;
                }
                roleIds.add(Long.valueOf(roleIdStr));
            }
        }
		return roleIds;        
    }

	@Override
	public List<User> getByQueryBean(UserQp qp) {		
		return userMapper.getByQueryBean(qp);
	}

	@Override
	public Long getCountByQueryBean(UserQp qp) {		
		return userMapper.getCountByQueryBean(qp);
	}

	@Override
	public void updateAccount(Long userId, Boolean locked) {
		User user = userMapper.getOneById(userId);
        user.setLocked(locked);
        userMapper.updateAccount(user); 
	}

	@Override
	public List<User> getFirstPageUsers() {
		return userMapper.getFirstPageUsers();
	}

	@Override
	public Long getAllUsersTotalCount() {
		return userMapper.getAllUsersTotalCount();
	}
	
	
}