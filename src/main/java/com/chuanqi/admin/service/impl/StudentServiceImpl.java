package com.chuanqi.admin.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.chuanqi.admin.domain.Student;
import com.chuanqi.admin.persistence.StudentMapper;
import com.chuanqi.admin.qp.StudentQp;
import com.chuanqi.admin.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	private StudentMapper studentMapper;
	
	@Override
	public Student getById(Long id) {
		return studentMapper.getById(id);
	}
	
	@Override
	public void insert(Student student) {
		studentMapper.insert(student);
	}	
	
	@Override
	public void update(Student student) {
		studentMapper.update(student);
	}
	
	@Override
	public void deleteById(Long id) {
		studentMapper.deleteById(id);		
	}

	@Override
	public List<Student> getAll() {
		return studentMapper.getAll();
	}

	@Override
	public List<Student> getFirstPage() {		
		return studentMapper.getFirstPage();
	}

	@Override
	public Long getTotalCount() {		
		return studentMapper.getTotalCount();
	}

	@Override
	public Long getCountByQueryBean(StudentQp qp) {
		return studentMapper.getCountByQueryBean(qp);
	}

	@Override
	public List<Student> getByQueryBean(StudentQp qp) {
		return studentMapper.getByQueryBean(qp);
	}

	@Override
	public int insertListBatch(List<Student> list) {
		return studentMapper.insertListBatch(list);
	}

	@Override
	public List<Student> getList(StudentQp qp) {
		return studentMapper.getList(qp);
	}
}