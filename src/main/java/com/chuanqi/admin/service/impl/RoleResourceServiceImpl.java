package com.chuanqi.admin.service.impl;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.chuanqi.admin.domain.RoleResource;
import com.chuanqi.admin.persistence.RoleResourceMapper;
import com.chuanqi.admin.service.RoleResourceService;

/**
 * 用户角色服务层接口实现
 * <p>User: XiongXianquan
 * <p>Date: 2015-03-03
 * <p>Version: 1.2
 */
@Service("roleResourceService")
public class RoleResourceServiceImpl implements RoleResourceService{
	@Resource
	private RoleResourceMapper roleResourceMapper;
	

	@Override
	public List<RoleResource> getRoleResourcesByRoleId(Long roleId) {
		return roleResourceMapper.getRoleResourcesByRoleId(roleId);
	}
	
	@Override
	public void deleteRbyR(Long roleId) {
		roleResourceMapper.deleteRbyR(roleId);
	}
	
	@Override
	public void insertRoleResource(RoleResource rr) {
		roleResourceMapper.insertRoleResource(rr);
	}

}
