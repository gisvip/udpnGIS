package com.chuanqi.admin.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.UserRole;
import com.chuanqi.admin.persistence.UserRoleMapper;
import com.chuanqi.admin.service.UserRoleService;


/**
 * 用户角色服务层接口实现
 * <p>User: XiongXianquan
 * <p>Date: 2015-03-03
 * <p>Version: 1.2
 */
@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService{
	@Resource
	private UserRoleMapper userRoleMapper;
	
	@Override
	public List<UserRole> getUserRolesByUserId(Long userId) {		
		return userRoleMapper.getUserRolesByUserId(userId);
	}

	@Override
	public void deleteRbyUR(Long roleId) {
		userRoleMapper.deleteRbyUR(roleId);
	}

	@Override
	public void insertRbyUR(UserRole rr) {
		userRoleMapper.insertRbyUR(rr);
	}

	@Override
	public List<UserRole> getUserRoleByRoleId(Long roleId) {
		return userRoleMapper.getUserRoleByRoleId(roleId);
	}
	
		
	
	
}
