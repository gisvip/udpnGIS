package com.chuanqi.admin.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.Role;
import com.chuanqi.admin.domain.RoleResource;
import com.chuanqi.admin.persistence.RoleMapper;
import com.chuanqi.admin.qp.RoleQp;
import com.chuanqi.admin.service.ResourceService;
import com.chuanqi.admin.service.RoleResourceService;
import com.chuanqi.admin.service.RoleService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	@Resource
	private RoleMapper roleMapper;
	
	@Autowired
    private ResourceService resourceService;
	
	@Autowired
    private RoleResourceService roleResourceService;
	
	
	
	@Override
	public int createRole(Role role) {
		return roleMapper.createRole(role);
	}

	@Override
	public int updateRole(Role role) {
		return roleMapper.updateRole(role);
	}

	@Override
	public void deleteRole(Long roleId) {
		roleMapper.deleteRole(roleId);
	}

	@Override
	public Role findOne(Long roleId) {
		return roleMapper.findOne(roleId);
	}

	@Override
	public List<Role> findAll() {
		return roleMapper.findAll();
	}
	
	
	
	

	@Override
	public Set<String> findRoles(Long... roleIds) {
		Set<String> roles = new HashSet<String>();
        for(Long roleId : roleIds) {
            Role role = findOne(roleId);
            if(role != null) {
                roles.add(role.getRole());
            }
        }
        return roles;
	}

	
	
	@Override
	public Set<String> findPermissions(Long[] roleIds) {
		Set<Long> resourceIds = new HashSet<Long>();
        for(Long roleId : roleIds) {
            Role role = findOne(roleId);
            if(role != null) {            	
                List<RoleResource> roleResources = roleResourceService.getRoleResourcesByRoleId(roleId);
                for(RoleResource roleResource: roleResources){
                	
                	Long resourceId = roleResource.getResourceId();
        			resourceIds.add(resourceId);
                }
            }
        }
        
        return resourceService.findPermissions(resourceIds);
	}
	
	private List<Long> getResourceIds(String resourceIdsStr) {
		List<Long> resourceIds = new ArrayList<Long>();
		if(StringUtils.isNotEmpty(resourceIdsStr)) {
        	String[] resourceIdStrs = resourceIdsStr.split(",");
        	for(String resourceIdStr : resourceIdStrs) {
                if(StringUtils.isEmpty(resourceIdStr)) {
                    continue;
                }
                resourceIds.add(Long.valueOf(resourceIdStr));
            }
        }
		return resourceIds;        
    }

	

	@Override
	public List<Role> selectRolesIn(List<Long> roleIds) {
		return roleMapper.selectRolesIn(roleIds);
	}

	@Override
	public List<Role> getRolesByUserId(Long userId) {
		return roleMapper.getRolesByUserId(userId);
	}

	@Override
	public List<Role> getFirstPageRoles() {
		return roleMapper.getFirstPageRoles();
	}

	@Override
	public Long getAllRolesTotalCount() {
		return roleMapper.getAllRolesTotalCount();
	}

	@Override
	public List<Role> getByQueryBean(RoleQp qp) {
		return roleMapper.getByQueryBean(qp);
	}

	@Override
	public Long getCountByQueryBean(RoleQp qp) {
		return roleMapper.getCountByQueryBean(qp);
	}

	@Override
	public Role getByRolename(String role) {
		return roleMapper.getByRolename(role);
	}

	@Override
	public Role getOneById(Long roleId) {
		return roleMapper.getOneById(roleId);
	}
	
	
	
	

	
    
}
