package com.chuanqi.admin.service.impl;

import javax.annotation.Resource;
import com.chuanqi.admin.domain.Resourse;
import com.chuanqi.admin.persistence.ResourceMapper;
import com.chuanqi.admin.service.ResourceService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service("resourceService")
public class ResourceServiceImpl implements ResourceService {
	
	@Resource
	private ResourceMapper resourceMapper;

	@Override
	public List<Resourse> findAll() {
		return resourceMapper.findAll();
	}
	
	@Override
	public Resourse findOne(Long resourceId) {
		return resourceMapper.findOne(resourceId);
	}	
	
	@Override
	public List<Resourse> getMenusByItemIds(List<Long> itemIds) {
		return resourceMapper.getMenusByItemIds(itemIds);
	}	

	@Override
	public List<Resourse> getItemIdsByMenuId(Map<String,Object> params) {
		return resourceMapper.getItemIdsByMenuId(params);
	}
	
	@Override
	public Set<String> findPermissions(Set<Long> resourceIds) {
		Set<String> permissions = new HashSet<String>();
        for(Long resourceId : resourceIds) {
            Resourse resource = findOne(resourceId);
            if(resource != null && !StringUtils.isEmpty(resource.getPermission())) {
                permissions.add(resource.getPermission());
            }
        }
        return permissions;
	}

	@Override
	public List<Resourse> getResourceIdsByRoleId(Long roleId) {
		return resourceMapper.getResourceIdsByRoleId(roleId);
	}
	    
}
