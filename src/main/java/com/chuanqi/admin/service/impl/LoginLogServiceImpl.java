package com.chuanqi.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.LoginLog;
import com.chuanqi.admin.persistence.LoginLogMapper;
import com.chuanqi.admin.qp.LoginLogQp;
import com.chuanqi.admin.service.LoginLogService;

@Service
public class LoginLogServiceImpl implements LoginLogService {
	@Autowired
	private LoginLogMapper loginLogMapper;	
	
	@Override
	public void insert(LoginLog loginLog) {
		loginLogMapper.insert(loginLog);		
	}

	@Override
	public List<LoginLog> getByQueryBean(LoginLogQp qp) {		
		return loginLogMapper.getByQueryBean(qp);
	}

	@Override
	public Long getCountByQueryBean(LoginLogQp qp) {		
		return loginLogMapper.getCountByQueryBean(qp);
	}

	@Override
	public Long getTotalCount() {
		return loginLogMapper.getTotalCount();
	}

}
