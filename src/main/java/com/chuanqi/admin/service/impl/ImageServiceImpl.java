package com.chuanqi.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.Image;
import com.chuanqi.admin.persistence.ImageMapper;
import com.chuanqi.admin.qp.ImageQp;
import com.chuanqi.admin.service.ImageService;

@Service("imageService")
public class ImageServiceImpl implements ImageService {
	@Autowired
	private ImageMapper imageMapper;
	
	@Override
	public List<Image> getFirstPage() {
		return imageMapper.getFirstPage();
	}

	@Override
	public Long getTotalCount() {
		return imageMapper.getTotalCount();
	}

	

	@Override
	public Long getCountByQueryBean(ImageQp qp) {		
		return imageMapper.getCountByQueryBean(qp);
	}

	@Override
	public List<Image> getByQueryBean(ImageQp qp) {		
		return imageMapper.getByQueryBean(qp);
	}

}
