package com.chuanqi.admin.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chuanqi.admin.domain.Video;
import com.chuanqi.admin.persistence.VideoMapper;
import com.chuanqi.admin.qp.VideoQp;
import com.chuanqi.admin.service.VideoService;

@Service("videoService")
public class VideoServiceImpl implements VideoService {
	@Autowired
	private VideoMapper videoMapper;
	
	@Override
	public List<Video> getFirstPage() {
		return videoMapper.getFirstPage();
	}

	@Override
	public Long getTotalCount() {
		return videoMapper.getTotalCount();
	}	

	@Override
	public Long getCountByQueryBean(VideoQp qp) {		
		return videoMapper.getCountByQueryBean(qp);
	}

	@Override
	public List<Video> getByQueryBean(VideoQp qp) {		
		return videoMapper.getByQueryBean(qp);
	}

}
