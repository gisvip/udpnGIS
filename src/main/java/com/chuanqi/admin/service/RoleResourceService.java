package com.chuanqi.admin.service;

import java.util.List;
import com.chuanqi.admin.domain.RoleResource;

public interface RoleResourceService {
	public List<RoleResource> getRoleResourcesByRoleId(Long roleId);
	
	public void deleteRbyR(Long roleId);
	public void insertRoleResource(RoleResource rr);
	
}
