package com.chuanqi.admin.service;

import java.util.List;

import com.chuanqi.admin.domain.PermittedRecordModel;


public interface PermittedRecordModelService {
	public List<PermittedRecordModel> getPermittedRecordModelsByByUserId(Long userId);
}
