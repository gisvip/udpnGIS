package com.chuanqi.admin.service;

import java.util.List;
import com.chuanqi.admin.domain.PermittedRecordModel;

public interface PermittedItemResourceIdService {
	public List<PermittedRecordModel> getPermittedItemResourceIdByUserId(Long userId);
	
	public List<PermittedRecordModel> getPermittedItemResourceIdByUserIdForTest(Long userId);
}
