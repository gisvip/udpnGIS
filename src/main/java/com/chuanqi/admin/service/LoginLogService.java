package com.chuanqi.admin.service;

import java.util.List;

import com.chuanqi.admin.domain.LoginLog;
import com.chuanqi.admin.qp.LoginLogQp;

public interface LoginLogService {
	public void insert(LoginLog loginLog);	
	public List<LoginLog> getByQueryBean(LoginLogQp qp);	
	public Long getCountByQueryBean(LoginLogQp qp);
	
	public Long getTotalCount();
}
