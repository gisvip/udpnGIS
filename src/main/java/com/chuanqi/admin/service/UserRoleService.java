package com.chuanqi.admin.service;

import java.util.List;

import com.chuanqi.admin.domain.UserRole;

public interface UserRoleService {
	public List<UserRole> getUserRolesByUserId(Long userId);
	public void deleteRbyUR(Long roleId);
	public void insertRbyUR(UserRole rr);
	
	public List<UserRole> getUserRoleByRoleId(Long roleId);
}
