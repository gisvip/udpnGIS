package com.chuanqi.admin.service;

import java.util.List;

import com.chuanqi.admin.domain.OperationLog;
import com.chuanqi.admin.qp.OperationLogQp;

public interface OperationLogService {
	public void insert(OperationLog operationLog);	
	public List<OperationLog> getByQueryBean(OperationLogQp qp);	
	public Long getCountByQueryBean(OperationLogQp qp);
	
	public Long getTotalCount();
}
