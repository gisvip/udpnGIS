package com.chuanqi.admin.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import com.chuanqi.admin.domain.Resourse;

public interface ResourceService {
	public List<Resourse> findAll();
	public Resourse findOne(Long resourceId);
	public List<Resourse> getMenusByItemIds(List<Long> itemIds);
    public List<Resourse> getItemIdsByMenuId(Map<String,Object> params);
    public Set<String> findPermissions(Set<Long> resourceIds);
    public List<Resourse> getResourceIdsByRoleId(Long roleId);
}
